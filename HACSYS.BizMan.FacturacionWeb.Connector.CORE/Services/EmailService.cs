﻿using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;
using NLog;
using System.Net;

namespace HACSYS.BizMan.FacturacionWeb.Connector.CORE.Services
{
    public class EmailService
    {
        private Logger Log = LogManager.GetCurrentClassLogger();

        public EmailService()
        {

        }

        public async Task<bool> SendAsync(int NumEstacion, string ErrorMessage, string sender, string pass)
        {
            //vsmtp.Credentials = new NetworkCredential("svmpetrofact@gmail.com", "e.s.2038");
            // No se usó identity message para poder identificar cualquier otro error que no se relacione a las versiones
            // del .net
            if (NumEstacion == 0 || ErrorMessage == null || ErrorMessage == "")
                return false;

            MailMessage mail = new MailMessage("svmpetrofact@gmail.com", "Error de SignalR en estacion: " + NumEstacion);
            mail.To.Add("soporte@hacsys.com");
            mail.Subject = "Token de registro para Sistema de Facturación";
            mail.Body = ErrorMessage;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sender, pass);
            smtp.Send(mail);

            return true;
        }

    }
}