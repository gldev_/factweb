﻿using HACSYS.BizMan.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.FacturacionWeb.Connector.Core
{
    public interface IVentasService
    {
        Task<Venta> GenerarVentaAsync(string folio, double monto);
        List<ImpuestoAplicado> CalcularImpuestos(Venta venta);
    }
}
