﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.Management.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.FacturacionWeb.Connector.Core
{
    public static class TicketsSampleData
    {
        public static async Task InitializeDatabaseAsync(VentasDbContext context)
        {

            var nl = context.Estados.FirstOrDefault(e => e.Clave.Equals("NL"));
            Direccion dir = new Direccion() { Calle = "Puerto Rico", CodigoPostal = "64000", Colonia = "Vista Hermosa", Estado = nl, Localidad = "Monterrey", Municipio = "Monterrey", NumExterior = "123", NumInterior = "2" };
            context.Direcciones.Add(dir);
            DatosFiscales df = new DatosFiscales() { Correo = "demo@gmail.com", Direccion = dir, RazonSocial = "Demo SA de CV", RFC = "TME840315KT6", Telefono = "812345678" };
            context.DatosFiscales.Add(df);
            var raiz = context.Entidades.FirstOrDefault();
            Entidad demo = new Entidad() { Nombre = "Demo", Jerarquia = "/1/", Padre = raiz };
            context.Entidades.Add(demo);
            Empresa empresa = new Empresa() { Nombre = "Empresa Demo", DatosFiscales = df, Entidad = demo };
            context.Empresas.Add(empresa);
            await context.SaveChangesAsync();

            // Impuestos básicos
            var iva = new Impuesto { Nombre = "IVA 16%", Tasa = 16, NombreSAT = "IVA", Tipo = TipoImpuesto.TRASLADADO, Activo = true };
            var ieps = new Impuesto { Nombre = "iepsMagna", Tasa = 3668, NombreSAT = "IEPS", Tipo = TipoImpuesto.TRASLADADO, Activo = true };
            var iepsPremium = new Impuesto { Nombre = "iepsPremium", Tasa = 4475, NombreSAT = "IEPS", Tipo = TipoImpuesto.TRASLADADO, Activo = true };
            var iepsDiesel = new Impuesto { Nombre = "iepsDiesel", Tasa = 3044, NombreSAT = "IEPS", Tipo = TipoImpuesto.TRASLADADO, Activo = true };
            //context.Impuestos.AddRange(new List<Impuesto> { iva, ieps });
            context.Impuestos.AddRange(new List<Impuesto> { iva, ieps, iepsPremium, iepsDiesel });

            // Gasolinas
            var gasolinas = new Categoria() { Nombre = "Gasolinas" };
            
            var impuestosMagna = new List<Impuesto>() { iva, ieps };
            var impuestosPremium = new List<Impuesto>() { iva, iepsPremium };
            var impuestosDiesel = new List<Impuesto>() { iva, iepsDiesel };
            var magna = new Producto { Concepto = "Magna", Activo = true, Categorias = new List<Categoria>() { gasolinas }, Clave = "32011", Impuestos = impuestosMagna, ValorUnitario = 13.5700, Unidad = "LT", Entidad = demo };
            //var premium = new Producto { Concepto = "Premium", Activo = true, Categorias = new List<Categoria>() { gasolinas }, Clave = "32012", Impuestos = impuestosGasolinas, ValorUnitario = 14.3800, Unidad = "LT", Entidad = demo };
            var premium = new Producto { Concepto = "Premium", Activo = true, Categorias = new List<Categoria>() { gasolinas }, Clave = "32012", Impuestos = impuestosPremium, ValorUnitario = 14.3800, Unidad = "LT", Entidad = demo };
            var diesel = new Producto { Concepto = "Diesel", Activo = true, Categorias = new List<Categoria>() { gasolinas }, Clave = "", Impuestos = impuestosDiesel, ValorUnitario = 14.3800, Unidad = "LT", Entidad = demo };
            context.Productos.AddRange(new List<Producto> { magna, premium, diesel });
            await context.SaveChangesAsync();
        }
    }
}
