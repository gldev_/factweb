﻿using HACSYS.BizMan.FacturacionWeb.Connector.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HACSYS.BizMan.DAL.Repositories;
using HACSYS.BizMan.DAL.Models;

namespace HACSYS.BizMan.FacturacionWeb.Connector.CORE
{
    public class VentasService : IVentasService
    {
        private IVentaRepository VentaRepository;
        public VentasService(IVentaRepository ventaRepository)
        {
            VentaRepository = ventaRepository;
        }

        public List<ImpuestoAplicado> CalcularImpuestos(Venta venta)
        {
            return new List<ImpuestoAplicado>();
        }

        public async Task<Venta> GenerarVentaAsync(string folio, double monto)
        {
            var venta = await VentaRepository.GetAsync(t => t.Folio.Equals(folio) && t.Monto == monto,null, "Detalles, Detalles.ImpuestosAplicados");
            return venta.FirstOrDefault();
        }
    }
}
