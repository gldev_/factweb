﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.FacturacionWeb.Connector.CORE.Services;
using HACSYS.SignalR;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Threading.Tasks;


namespace HACSYS.BizMan.FacturacionWeb.Connector.Core
{
    // TODO: Implementar la lógica para la reconexión
    public class FacturacionClient
    {
        public string ServerURL { get; set; }
        public IHubProxy ServerProxy;
        private IVentasService VentaService;
        private Logger Log = LogManager.GetLogger("FacturacionClient");
        private int EmpresaID;
        private EmailService Email;
        private static string Sender;
        private static string Pass;
        private static DateTime LastCheck;
        private static int MailTimer;

        public FacturacionClient(int empresaID, IHubProxyFactory hubProxyFactory, 
            IVentasService ticketService, string sender, string key,int mailTimer, string serverURL = "")
        {
            EmpresaID = empresaID;
            VentaService = ticketService;
            LastCheck = DateTime.Now;
            MailTimer = mailTimer;
            //var serializer = new JsonSerializer();
            //serializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            //GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);
            Email = new EmailService();
            Sender = sender;
            Pass = key;
            HubProxyOptions options = new HubProxyOptions()
            {
                HubUrl = serverURL,
                HubName = "FacturacionHub",
                OnStarted = OnStarted,
                Reconnected = Reconnected,
                Faulted = Faulted,
                Connected = Connected,
                AutomaticRestart = true,
                RestartTimeout = TimeSpan.FromSeconds(15)
            };
            hubProxyFactory.Create(options);
        }

        // La variable de retorno solo sirve para facilitar los unit tests
        public async Task<Venta> GetVentaEmpresa(string clienteConnectionID, string folio, double monto)
        {
            Log.Info($"GetVentaEmpresa recibido, buscando folio: {folio} con monto: {monto}");
            var venta = await VentaService.GenerarVentaAsync(folio, monto);
            try{
                if (venta != null)
                {
                    Log.Info("Venta encontrado");
                    await ServerProxy.Invoke("GetVentaResult", clienteConnectionID, venta);
                    return venta;
                }
                else
                {
                    Log.Info("Venta no encontrado");
                    await ServerProxy.Invoke("GetVentaResult", clienteConnectionID, JValue.CreateNull());
                    return null;
                }
            }catch(Exception e)
            {
                Log.Error(e, "Error envianro resultado al servidor");
                Log.Info($"El error fue :{e.Message}");
                return null;
            }
        }

        public void OnStarted(IHubProxy proxy)
        {
            Log.Info("Proxy iniciado");
            ServerProxy = proxy;
            ServerProxy.JsonSerializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            //ServerProxy.JsonSerializer.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
            ServerProxy.JsonSerializer.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            ServerProxy.JsonSerializer.NullValueHandling = NullValueHandling.Ignore;

            ServerProxy.On<string, string, double>("GetVentaEmpresa", async (clienteConnectionID, folio, monto) => { await GetVentaEmpresa(clienteConnectionID, folio, monto); });
        }

        public void Connected()
        {
            Log.Info("Cliente Conectado");
            Log.Info("Enviando solicitud de conexión de empresa al servidor");
            var clienteConectado = ServerProxy.Invoke<bool>("ConnectClient", EmpresaID);
        }

        public void Reconnected()
        {
            Log.Info("Cliente reconectado");
            Log.Info("Enviando solicitud de conexión de empresa al servidor");
            var clienteConectado = ServerProxy.Invoke<bool>("ConnectClient", EmpresaID);
        }

        public void Faulted(Exception ex)
        {
            if (LastCheck.AddHours(MailTimer) < DateTime.Now)
                Email.SendAsync(EmpresaID, ex.Message, Sender, Pass);
            else
                LastCheck = DateTime.Now;

            Log.Info($"Error de signalR {ex}");
            //Log.Error(ex, "Error de signalr en FacturacionClient");
            Console.WriteLine(ex);
        }

        public void Disconnected()
        {
            Log.Info("Cliente desconectado");
        }
    }
}
