﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.Management.DAL.Models;
using System.Data.Entity;
using HACSYS.BizMan.DAL.EF6;

namespace HACSYS.BizMan.FacturacionWeb.Connector.Core
{
    public class VentasDbContext : BizManDbContext
    {
        public VentasDbContext(string connectionString) : base(connectionString)
        {
            
        }
    }
}
