﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.Management.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HACSYS.BizMan.Facturacion.Web.ViewModels
{
    public enum ClienteStatusCode { Success = 200, NotFound = 500, DataMissMatch = 400, ClientRegistered = 600 }
    public enum FacturaStatusCode { Success = 200, DataError = 500 }
    public enum VentaStatus { Success = 200, NotFound = 500, DataMissMatch = 400, Error = 600 }
    public class ResponseHandler
    {

        public Venta Venta { get; set; }

        public VentaStatus Status { get; set; }

    }

    public class ResponseRegistration
    {
        public Cliente cliente { get; set; }
        public ClienteStatusCode clienteStatus { get; set; }

    }

    public class ResponseFactura
    {
        public FacturaViewModel factura { get; set; }

        public FacturaStatusCode facturaCode { get; set; }
    }

    public class VentaResponse
    {
        public Venta Venta { get; set; }

        public VentaStatus Status { get; set; }
    }

    public class UserResponse
    {

    }
}