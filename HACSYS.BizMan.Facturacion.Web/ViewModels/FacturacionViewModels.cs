﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.Management.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HACSYS.BizMan.Facturacion.Web.ViewModels
{
    public class FacturaViewModel
    {
        public Cliente Cliente { get; set; }
        public List<Venta> Ventas { get; set; }
        public string Correo { get; set; }
    }

    public class ComprobanteViewModel
    {
        public int ComprobanteID { get; set; }
        public double Total { get; set; }
        public DateTime Fecha { get; set; }
        public string Folio { get; set; }
    }
}