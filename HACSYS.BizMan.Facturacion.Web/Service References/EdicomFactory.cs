﻿using PetroFactWeb.Edicom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace HACSYS.FacturacionWeb.Service_References
{
    public class EdicomFactory
    {
        public static CFDiClient Create()
        {
            //IConfiguration configuration = new Configuration().AddJsonFile("config.json");
            var address = "";
            BasicHttpBinding binding = new BasicHttpBinding();
            EndpointAddress endpoint = new EndpointAddress(address);
            //ChannelFactory<CFDi> channelFactory = new ChannelFactory<CFDi>(binding, endpoint);
            //CFDi client = channelFactory.CreateChannel();
            return new CFDiClient(binding,endpoint);
        }
    }
}
