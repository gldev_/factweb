﻿using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.Management.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Models
{
    public class UserToken
    {
        public int UserTokenID { get; set; }
        public string RFC { get; set; }
        public string Token { get; set; }
        public string Expira { get; set; }
        public string emailTo { get; set; }
        public bool emailSent { get; set; }


        public virtual Cliente Cliente { get; set; }
        public virtual Comprobante Comprobante { get; set; }
    }
}
