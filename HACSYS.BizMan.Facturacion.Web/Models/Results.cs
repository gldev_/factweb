﻿
using System.Collections.Generic;

namespace HACSYS.BizMan.Facturacion.Web.Models
{
    public class AccountResult
    {
        public string Result { get; set; }
        public int StatusCode { get; set; }
        public int clientID { get; set; }
    }

    public class AccountErrorsResult : AccountResult
    {
        public List<string> Errores { get; set; }
    }
}