﻿using HACSYS.BizMan.DAL.EF6.Repositories;
using HACSYS.BizMan.DAL.Repositories;
using HACSYS.FacturacionWeb;
using HACSYS.FacturacionWeb.Identity;
using HACSYS.Management.DAL.Repositories;
using HACSYS.Management.DAL.EF6.Repositories;
using Microsoft.Owin;
using Owin;
using SimpleInjector;
using HACSYS.BizMan.ARES.DAL.Repositories;
using HACSYS.BizMan.Facturacion.Web.Services;
using Microsoft.AspNet.SignalR;
using HACSYS.BizMan.Facturacion.Web.Hubs;
using SimpleInjector.Extensions;
using HACSYS.Management.DAL.EF6;
using HACSYS.BizMan.ARES.DAL.EF6;
using System.Web.Http;
using SimpleInjector.Integration.WebApi;
using Microsoft.AspNet.SignalR.Hubs;
using System.Web.Routing;
using HACSYS.BizMan.DAL.EF6;
using SimpleInjector.Extensions.ExecutionContextScoping;
using HACSYS.BizMan.Facturacion.DAL.Repositories;
using HACSYS.Tools.Security.Certificates;
using System.Security.Cryptography.X509Certificates;
using HACSYS.BizMan.Facturacion.DAL.EF6;
using System.Data.Entity;
using HACSYS.BizMan.Facturacion.Web.Repositories;
using HACSYS.BizMan.Facturacion.DAL.EF6.Repositories;

[assembly: OwinStartupAttribute(typeof(HACSYS.BizMan.Facturacion.Web.Startup))]
namespace HACSYS.BizMan.Facturacion.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            using (var context = new FacturacionDbContext())
            {
                var exist = context.Database.Exists();
                InitialData.InitializeDatabaseAsync(context).Wait();
                if (!exist)
                    SampleData.InitializeDatabaseAsync(context).Wait();
            }

            ConfigureAuth(app);

            var container = new Container();
            //container.RegisterWebApiRequest<DbContext, ManagementDbContext>();
            container.RegisterWebApiRequest<FacturacionDbContext>();
            container.RegisterWebApiRequest<ManagementDbContext>(()=> { return container.GetInstance<FacturacionDbContext>(); });
            container.RegisterWebApiRequest<BizManDbContext>(() => { return container.GetInstance<FacturacionDbContext>(); });
            container.RegisterWebApiRequest<BizManFacturacionDbContext>(() => { return container.GetInstance<FacturacionDbContext>(); });
            //container.RegisterWebApiRequest<BizManDbContext>();
            //container.RegisterWebApiRequest<ManagementDbContext, FacturacionDbContext>();
            //container.RegisterWebApiRequest<BizManDbContext, FacturacionDbContext>();
            //container.RegisterWebApiRequest<BizManFacturacionDbContext, FacturacionDbContext>();
            //container.RegistWebApiRequester<FacturacionDbContext>(FacturacionDbContext.Create);
            //container.RegistWebApiRequester<ManagementDbContext>(FacturacionDbContext.Create);
            ////container.RegiWebApiRequestster<AresDbContext>(FacturacionDbContext.Create);
            //container.RegistWebApiRequester<BizManDbContext>(FacturacionDbContext.Create);
            container.RegisterWebApiRequest<IUsuarioRepository, UsuarioRepository>();
            container.RegisterWebApiRequest<IClienteRepository, ClienteRepository>();
            container.RegisterWebApiRequest<IVentaRepository, VentaRepository>();
            container.RegisterWebApiRequest<IEmpresaRepository, EmpresaRepository>();
            container.RegisterWebApiRequest<IDatosFiscalesRepository, DatosFiscalesRepository>();
            container.RegisterWebApiRequest<IDireccionRepository, DireccionRepository>();
            container.RegisterWebApiRequest<IPaisRepository, PaisRepository>();
            container.RegisterWebApiRequest<IEstacionRepository, EstacionRepository>();
            container.RegisterWebApiRequest<IComprobanteRepository, ComprobanteRepository>();
            container.RegisterWebApiRequest<IFormaDePagoRepository, FormaDePagoRepository>();
            container.RegisterWebApiRequest<IMetodoDePagoRepository, MetodoDePagoRepository>();
            container.RegisterWebApiRequest<IReferenciaDePagoRepository, ReferenciaDePagoRepository>();
            container.RegisterWebApiRequest<ICondicionDePagoRepository, CondicionDePagoRepository>();
            container.RegisterWebApiRequest<IEstadoRepository, EstadoRepository>();
            container.RegisterWebApiRequest<IImpuestoRepository, ImpuestoRepository>();
            container.RegisterWebApiRequest<IProductoRepository, ProductoRepository>();
            //container.RegistWebApiRequester<IImpuestoService, ImpuestosService>();
            //container.RegistWebApiRequester<IProductoService, ProductoService>();
            container.RegisterWebApiRequest<ICertificateStore, CertificateFileStore>();
            container.RegisterWebApiRequest<ICertificateProvider<X509Certificate>, X509CertificateProvider>();
            container.RegisterWebApiRequest<IParser, Parser>();
            container.RegisterWebApiRequest<IDateProvider, DateProvider>();
            container.RegisterWebApiRequest<IEmpresasService, EmpresasService>();
            container.RegisterWebApiRequest<IVentaService, VentaService>();
            container.RegisterWebApiRequest<IClienteService, ClientService>();
            container.RegisterWebApiRequest<IDatosFiscalesService, DatosFiscalesService>();
            container.RegisterWebApiRequest<IFacturacionService, FacturacionService>();
            container.RegisterWebApiRequest<IPDFService, PDFServiceARES>();
            container.RegisterWebApiRequest<IConfigFacturacionRepository, ConfigFacturacionRepository>();
            container.RegisterWebApiRequest<IDatosPagoService, DatosPagoService>();
            container.RegisterWebApiRequest<IEmailService, EmailService>();

            HttpConfiguration config = new HttpConfiguration
            {
                DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container)
            };
            //container.RegisterWebApiControllers(config);

            WebApiConfig.Register(config);
            app.UseWebApi(config);
            app.MapSignalR(new HubConfiguration { EnableDetailedErrors = true });

        }
    }
}
