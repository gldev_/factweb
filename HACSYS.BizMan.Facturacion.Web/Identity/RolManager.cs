﻿using HACSYS.Management.DAL.EF6.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HACSYS.BizMan.Facturacion.Web.Identity
{
    public class RolManager:RoleManager<Rol,int>
    {
        public RolManager(RoleStore<Rol,int,UsuarioRol> store):base(store)
        {

        }
    }
}