﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;

using Microsoft.Owin.Security;
using Microsoft.Owin;
using System.Security.Claims;
using HACSYS.Management.DAL.EF6.Identity;

namespace HACSYS.BizMan.Facturacion.Web.Identity
{
    public class UsuarioSignInManager : SignInManager<Usuario, int>
    {
        public UsuarioSignInManager(UsuarioManager userManager, IAuthenticationManager authenticationManager) :
            base(userManager, authenticationManager)
        { }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(Usuario user)
        {
            return user.GenerateUserIdentityAsync((UsuarioManager)UserManager);
        }

        public static UsuarioSignInManager Create(IdentityFactoryOptions<UsuarioSignInManager> options, IOwinContext context)
        {
            return new UsuarioSignInManager(context.GetUserManager<UsuarioManager>(), context.Authentication);
        }
    }
}
