﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HACSYS.FacturacionWeb.Identity
{
    public class DbContextFactory
    {
        public static FacturacionDbContext GetContext()
        {
            FacturacionDbContext context = null;
            try
            {
                context = new FacturacionDbContext();
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }
            return context;
        }
    }
}
