﻿using HACSYS.BizMan.ARES.DAL.EF6;
using HACSYS.BizMan.ARES.DAL.Models;
using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.Facturacion.DAL.EF6;
using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.BizMan.Facturacion.Web.Models;
using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.Management.DAL.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;


namespace HACSYS.FacturacionWeb.Identity
{
    public class FacturacionDbContext : BizManFacturacionDbContext
    {
        public static FacturacionDbContext Create()
        {
            return new FacturacionDbContext();
        }

      
        public FacturacionDbContext() : base("TestConnection")
        {
           // this.Database.Connection.ConnectionString = HACSYS.Tools.AppSettings.getStringValue("ContextName", "DefaultConnection");
            this.Configuration.ValidateOnSaveEnabled = false;
        }

        public DbSet<Estacion> Estaciones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Estacion>().ToTable("Estacion", "ARES");

            //modelBuilder.Entity<Estacion>().HasRequired(e => e.Empresa).WithMany().HasForeignKey(e => e.EmpresaID);
        }

        //public DbSet<ComprobanteItem> ComprobantesItems { get; set; }
        //public DbSet<TipoComprobante> TipoComprobantes { get; set; }
        //public DbSet<UserToken> UserToken { get; set; }
    }
}