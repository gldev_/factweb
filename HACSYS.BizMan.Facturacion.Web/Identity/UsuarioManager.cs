﻿using HACSYS.FacturacionWeb.Identity;
using HACSYS.Management.DAL.EF6;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.BizMan.Facturacion.Web.Services;

namespace HACSYS.BizMan.Facturacion.Web.Identity
{
    public class UsuarioManager : UserManager<Usuario,int>
    {
        public UsuarioManager(IUserStore<Usuario,int> store)
            : base(store)
        {
        }

        public static UsuarioManager Create(IdentityFactoryOptions<UsuarioManager> options,
            IOwinContext context)
        {
            var manager = new UsuarioManager(new UserStore<Usuario,Rol,int,UsuarioLogin,UsuarioRol,UsuarioClaim>(context.Get<FacturacionDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<Usuario,int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;
            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug in here.
            manager.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider<Usuario,int>
            {
                MessageFormat = "El código de seguridad es: {0}"
            });
            manager.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider<Usuario,int>
            {
                Subject = "Código de Seguridad",
                BodyFormat = "El código de seguridad es {0}"
            });
            manager.EmailService = new EmailService();
            //manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<Usuario,int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}