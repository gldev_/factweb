﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using cfdv32;

namespace HACSYS.BizMan.Facturacion.Web.Resources
{
    public class SAT
    {
        private AsymmetricKeyParameter privateKey;
        private AsymmetricKeyParameter publicKey;

        public string Certificado { get; set; }
        public string NoCertificado { get; set; }
        public string InformacionCertificado { get; set; }
        public DateTime ValidoDesde { get; set; }
        public DateTime ValidoHasta { get; set; }
        public string RazonSocial { get; set; }
        public string RFC { get; set; }
        public string XMLGenerado { get; set; }

        #region Password
        private class Password : Org.BouncyCastle.OpenSsl.IPasswordFinder
        {
            private readonly char[] password;

            public Password(string word)
            {
                this.password = (char[])word.ToCharArray().Clone();
            }

            public char[] GetPassword()
            {
                return (char[])password.Clone();
            }
        }
        #endregion

        public string GetCadenaOriginal(string XML)
        {
            if (XML.Length > 0)
            {
                Stream XSLT = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".Resources.cadenaoriginal_3_2.xslt");

                System.IO.StringWriter strWriter = new StringWriter();
                XmlReader Reader = new XmlTextReader(XSLT);

                System.Xml.Xsl.XslCompiledTransform transformer = new System.Xml.Xsl.XslCompiledTransform();

                transformer.Load(Reader);
                XmlReader xml = XmlReader.Create(new System.IO.StringReader(XML));
                transformer.Transform(xml, null, strWriter);

                return Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(System.Text.RegularExpressions.Regex.Replace(strWriter.ToString(), @"(^\s+)|(\s+$)|(\s+(?=\s.))", "")));

            }
            return "";

        }

        #region CargarCertificado
        public bool CargarCertificado(string ArchivoCertificado)
        {
            if (File.Exists(ArchivoCertificado))
            {
                using (System.IO.Stream sr = System.IO.File.OpenRead(ArchivoCertificado))
                {
                    RFC = "";
                    Org.BouncyCastle.X509.X509CertificateParser cp = new Org.BouncyCastle.X509.X509CertificateParser();
                    Org.BouncyCastle.X509.X509Certificate certificate = cp.ReadCertificate(sr);
                    publicKey = certificate.GetPublicKey();
                    InformacionCertificado = "Emitido por " + certificate.IssuerDN.GetValues()[1].ToString() + "\n" + certificate.IssuerDN.GetValues()[8].ToString() + "\nValido del " + certificate.NotBefore.ToString("dd/MM/yyyy") + " al " + certificate.NotAfter.ToString("dd/MM/yyyy");
                    ValidoDesde = certificate.NotBefore;
                    ValidoHasta = certificate.NotAfter;
                    RazonSocial = certificate.SubjectDN.GetValues()[0].ToString();
                    foreach (string str in certificate.SubjectDN.GetValues())
                    {
                        if (str.IndexOf("/") > 0)
                        {
                            RFC = str.Substring(0, str.IndexOf("/")).Trim();
                            break;
                        }
                    }
                    foreach (string str in certificate.SubjectDN.GetValues())
                    {
                        if (str.Replace(" ", "").Trim().Length >= 12 && str.Replace(" ", "").Trim().Length <= 13)
                        {
                            if (RFC == "") RFC = str.Replace(" ", "").Trim();
                        }
                    }

                    //RFC = certificate.SubjectDN.GetValues()[3].ToString().Substring(0,certificate.SubjectDN.GetValues()[3].ToString().IndexOf("/")).Trim();

                    /*
                                         Count = 6
                    [0]: "HACSYS DE MEXICO SA DE CV"
                    [1]: "HACSYS DE MEXICO SA DE CV"
                    [2]: "HACSYS DE MEXICO SA DE CV"
                    [3]: "HME0302244R2 / HUVH750412LK6"
                    [4]: " / HUVH750412HNLRLC02"
                    [5]: "HACSYS DE MEXICO" 
                                         */

                    /*
                     *   [0]: "A.C. de pruebas"
    [1]: "Servicio de Administración Tributaria"
    [2]: "Administración de Seguridad de la Información"
    [3]: "asisnet@pruebas.sat.gob.mx"
    [4]: "Av. Hidalgo 77, Col. Guerrero"
    [5]: "06300"
    [6]: "MX"
    [7]: "Distrito Federal"
    [8]: "Coyoacán"
    [9]: "SAT970701NN3"
    [10]: "Responsable: Héctor Ornelas Arciga"
                     */
                    byte[] nSerie = certificate.SerialNumber.ToByteArray();
                    NoCertificado = Encoding.ASCII.GetString(nSerie);
                    Certificado = Convert.ToBase64String(certificate.GetEncoded());
                    sr.Close();
                    return true;
                }
            }
            else
            {
                try
                {
                    byte[] bCertificate = Convert.FromBase64String(ArchivoCertificado);

                    RFC = "";
                    Org.BouncyCastle.X509.X509CertificateParser cp = new Org.BouncyCastle.X509.X509CertificateParser();
                    Org.BouncyCastle.X509.X509Certificate certificate = cp.ReadCertificate(bCertificate);

                    publicKey = certificate.GetPublicKey();
                    InformacionCertificado = "Emitido por " + certificate.IssuerDN.GetValues()[1].ToString() + "\n" + certificate.IssuerDN.GetValues()[8].ToString() + "\nValido del " + certificate.NotBefore.ToString("dd/MM/yyyy") + " al " + certificate.NotAfter.ToString("dd/MM/yyyy");
                    ValidoDesde = certificate.NotBefore;
                    ValidoHasta = certificate.NotAfter;
                    RazonSocial = certificate.SubjectDN.GetValues()[0].ToString();
                    foreach (string str in certificate.SubjectDN.GetValues())
                    {
                        if (str.IndexOf("/") > 0)
                        {
                            RFC = str.Substring(0, str.IndexOf("/")).Trim();
                            break;
                        }
                    }
                    foreach (string str in certificate.SubjectDN.GetValues())
                    {
                        if (str.Replace(" ", "").Trim().Length >= 12 && str.Replace(" ", "").Trim().Length <= 13)
                        {
                            if (RFC == "") RFC = str.Replace(" ", "").Trim();
                        }
                    }



                    byte[] nSerie = certificate.SerialNumber.ToByteArray();
                    NoCertificado = Encoding.ASCII.GetString(nSerie);
                    Certificado = Convert.ToBase64String(certificate.GetEncoded());
                    return true;

                }
                catch (Exception ex)
                {
                    System.Diagnostics.EventLog.WriteEntry("PetroFact", "SAT Certificado\n" + ex.Message);
                    return false;
                }
            }
        }
        #endregion

        #region CargarLlavePrivada
        public bool CargarLlavePrivada(string ArchivoLlavePrivada, string password)
        {
            //Leemos el DER
            if (File.Exists(ArchivoLlavePrivada))
            {
                try
                {
                    byte[] signed = File.ReadAllBytes(ArchivoLlavePrivada);
                    //MemoryStream ms = new MemoryStream(UTF8Encoding.UTF8.GetBytes(Convert.ToBase64String(signed)));
                    //byte[] decoded = Org.BouncyCastle.Utilities.Encoders.Base64.Decode(ms.ToArray());
                    Password pass = new Password(password);

                    Asn1Object sigASN = Asn1Object.FromByteArray(signed);
                    EncryptedPrivateKeyInfo pInfo = EncryptedPrivateKeyInfo.GetInstance(sigASN);
                    PrivateKeyInfo decrypted = PrivateKeyInfoFactory.CreatePrivateKeyInfo(pass.GetPassword(), pInfo);
                    privateKey = (RsaPrivateCrtKeyParameters)PrivateKeyFactory.CreateKey(decrypted);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                try
                {
                    byte[] signed = Convert.FromBase64String(ArchivoLlavePrivada);
                    //MemoryStream ms = new MemoryStream(UTF8Encoding.UTF8.GetBytes(Convert.ToBase64String(signed)));
                    //byte[] decoded = Org.BouncyCastle.Utilities.Encoders.Base64.Decode(ms.ToArray());
                    Password pass = new Password(password);

                    Asn1Object sigASN = Asn1Object.FromByteArray(signed);
                    EncryptedPrivateKeyInfo pInfo = EncryptedPrivateKeyInfo.GetInstance(sigASN);
                    PrivateKeyInfo decrypted = PrivateKeyInfoFactory.CreatePrivateKeyInfo(pass.GetPassword(), pInfo);
                    privateKey = (RsaPrivateCrtKeyParameters)PrivateKeyFactory.CreateKey(decrypted);
                    return true;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.EventLog.WriteEntry("PetroFact", "SAT Cargar LlavePrivada\n" + ex.Message);
                    return false;
                }
            }

        }
        #endregion

        #region GenerarSello
        public string GenerarSello(string CadenaOriginal)
        {
            // Get a signature object using the MD5 and RSA combo
            // and sign the plaintext with the private key

            if (publicKey != null && privateKey != null)
            {
                AsymmetricCipherKeyPair key = new AsymmetricCipherKeyPair(publicKey, privateKey);
                //ISigner sig = SignerUtilities.GetSigner("MD5WithRSA");//MD5WITHRSA/ISO9796-2
                ISigner sig = SignerUtilities.GetSigner("SHA-1withRSA");//SHA1WITHRSA/ISO9796-2

                byte[] Mensaje = System.Text.UTF8Encoding.UTF8.GetBytes(CadenaOriginal);
                sig.Init(true, key.Private);
                sig.BlockUpdate(Mensaje, 0, Mensaje.Length);
                byte[] signature = sig.GenerateSignature();
                return Convert.ToBase64String(signature);
                //Convert.ToBase64String(Org.BouncyCastle.Utilities.Encoders.Base64.Encode(signature));
            }
            else
            {
                return "";
            }
        }
        #endregion

        #region GetPFX
        public static byte[] GetPFX(string Certificado, string LlavePrivada, string password)
        {

            try
            {
                byte[] bCertificate = Convert.FromBase64String(Certificado);

                Org.BouncyCastle.X509.X509CertificateParser cp = new Org.BouncyCastle.X509.X509CertificateParser();
                Org.BouncyCastle.X509.X509Certificate certificate = cp.ReadCertificate(bCertificate);

                Org.BouncyCastle.Pkcs.X509CertificateEntry[] cert = new Org.BouncyCastle.Pkcs.X509CertificateEntry[1];
                cert[0] = new X509CertificateEntry(certificate);


                byte[] signed = Convert.FromBase64String(LlavePrivada);
                //MemoryStream ms = new MemoryStream(UTF8Encoding.UTF8.GetBytes(Convert.ToBase64String(signed)));
                //byte[] decoded = Org.BouncyCastle.Utilities.Encoders.Base64.Decode(ms.ToArray());
                Password pass = new Password(password);

                Asn1Object sigASN = Asn1Object.FromByteArray(signed);
                EncryptedPrivateKeyInfo pInfo = EncryptedPrivateKeyInfo.GetInstance(sigASN);
                PrivateKeyInfo decrypted = PrivateKeyInfoFactory.CreatePrivateKeyInfo(pass.GetPassword(), pInfo);
                AsymmetricKeyParameter privateKey = (RsaPrivateCrtKeyParameters)PrivateKeyFactory.CreateKey(decrypted);

                Pkcs12Store store = new Pkcs12StoreBuilder().Build();


                System.Collections.Hashtable bagAttr = new System.Collections.Hashtable();

                bagAttr.Add(PkcsObjectIdentifiers.Pkcs9AtFriendlyName.Id, new DerBmpString("Key"));
                bagAttr.Add(PkcsObjectIdentifiers.Pkcs9AtLocalKeyID.Id, new Org.BouncyCastle.X509.Extension.SubjectKeyIdentifierStructure(certificate.GetPublicKey()));
                //
                // if you haven't set the friendly name and local key id above
                // the name below will be the name of the key
                //
                store.SetKeyEntry("Key", new AsymmetricKeyEntry(privateKey, bagAttr), cert);

                System.Text.UnicodeEncoding encoding = new System.Text.UnicodeEncoding();
                string resultado = "";
                using (MemoryStream strm = new MemoryStream())
                {
                    store.Save(strm, password.ToCharArray(), new SecureRandom());
                    strm.Position = 0;
                    resultado = Convert.ToBase64String(strm.ToArray());
                }

                return Convert.FromBase64String(resultado);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null;
            }


        }
        #endregion

        //#region GeneraXML
        //public string GeneraXML(Comprobante comprobante, string RutaArchivoXML)
        //{
        //    XmlSerializerNamespaces xmlNameSpace = new XmlSerializerNamespaces();
        //    xmlNameSpace.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        //    MemoryStream strmXML = new MemoryStream();
        //    XmlTextWriter xmlTextWriter = new XmlTextWriter(strmXML, Encoding.UTF8);
        //    xmlTextWriter.Formatting = Formatting.Indented;
        //    XmlSerializer xs = new XmlSerializer(typeof(Comprobante));

        //    xs.Serialize(xmlTextWriter, comprobante, xmlNameSpace);
        //    xmlTextWriter.Close();
        //    xmlTextWriter.Flush();

        //    StreamReader reader = new StreamReader(strmXML, Encoding.UTF8, true);
        //    strmXML.Seek(0, SeekOrigin.Begin);
        //    strmXML.Position = 0;
        //    return reader.ReadToEnd();
        //} 
        //#endregion

        #region GeneraXML
        public string GeneraXML(Comprobante comprobante, string RutaArchivoXML)
        {
            XmlSerializerNamespaces xmlNameSpace = new XmlSerializerNamespaces();
            xmlNameSpace.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            MemoryStream strmXML = new MemoryStream();
            XmlTextWriter xmlTextWriter = new XmlTextWriter(RutaArchivoXML + ".xml", Encoding.UTF8);
            xmlTextWriter.Formatting = Formatting.Indented;
            XmlSerializer xs = new XmlSerializer(typeof(Comprobante));

            xs.Serialize(xmlTextWriter, comprobante, xmlNameSpace);
            xmlTextWriter.Close();

            return System.IO.File.ReadAllText(RutaArchivoXML + ".xml", Encoding.UTF8);

        }
        #endregion

        #region GeneraXML v3
        public string GeneraXML(cfdv32.Comprobante comprobante)
        {
            try
            {
                XmlSerializerNamespaces xmlNameSpace = new XmlSerializerNamespaces();
                xmlNameSpace.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                xmlNameSpace.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
                using (System.IO.MemoryStream stream = new MemoryStream())
                {
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(stream, Encoding.UTF8);
                    xmlTextWriter.Formatting = Formatting.Indented;
                    XmlSerializer xs = new XmlSerializer(typeof(cfdv32.Comprobante));

                    xs.Serialize(xmlTextWriter, comprobante, xmlNameSpace);
                    xmlTextWriter.Flush();

                    stream.Position = 0;
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("BizMan", "GeneraXML:" + ex.Message);
                return "";
            }

        }
        #endregion

        #region CadenaOriginal
        public string CadenaOriginal(string RutaArchivoXML)
        {
            if (System.IO.File.Exists(RutaArchivoXML + ".xml"))
            {
                Stream XSLT = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".cadenaoriginal_2_2.xslt");

                System.IO.StringWriter strWriter = new StringWriter();
                XmlReader Reader = new XmlTextReader(XSLT);

                System.Xml.Xsl.XslCompiledTransform transformer = new System.Xml.Xsl.XslCompiledTransform();

                transformer.Load(Reader);
                transformer.Transform(RutaArchivoXML + ".xml", null, strWriter);

                return Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(System.Text.RegularExpressions.Regex.Replace(strWriter.ToString(), @"(^\s+)|(\s+$)|(\s+(?=\s.))", "")));

            }
            return "";

        }
        #endregion

        #region CadenaOriginalv3
        public string CadenaOriginalv3(string RutaArchivoXML)
        {
            if (System.IO.File.Exists(RutaArchivoXML + ".xml"))
            {
                Stream XSLT = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".cadenaoriginal_3_2.xslt");

                System.IO.StringWriter strWriter = new StringWriter();
                XmlReader Reader = new XmlTextReader(XSLT);

                System.Xml.Xsl.XslCompiledTransform transformer = new System.Xml.Xsl.XslCompiledTransform();

                transformer.Load(Reader);
                transformer.Transform(RutaArchivoXML + ".xml", null, strWriter);

                return Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(System.Text.RegularExpressions.Regex.Replace(strWriter.ToString(), @"(^\s+)|(\s+$)|(\s+(?=\s.))", "")));

            }
            return "";

        }
        #endregion

        #region DeserializeCFDi
        public cfdv32.Comprobante DeserializeCFDi(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(typeof(Comprobante));
            object obj = ser.Deserialize(reader);
            // Then you just need to cast obj into whatever type it is eg:
            return (cfdv32.Comprobante)obj;
        }
        #endregion

        #region DeserializeCFD
        public static Comprobante DeserializeCFD(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(typeof(Comprobante));
            object obj = ser.Deserialize(reader);
            // Then you just need to cast obj into whatever type it is eg:
            return (Comprobante)obj;
        }
        #endregion

    }
}