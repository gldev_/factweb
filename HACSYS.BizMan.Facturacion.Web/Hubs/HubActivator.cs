﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HACSYS.BizMan.Facturacion.Web.Hubs
{
    public class SimpleInjectorHubActivator : IHubActivator
    {
        private readonly Container _container;

        public SimpleInjectorHubActivator(Container container)
        {
            _container = container;
        }

        public IHub Create(HubDescriptor descriptor)
        {
            try
            {
                return (IHub)_container.GetInstance(descriptor.HubType);
            }catch(Exception e)
            {

            }
            return null;
        }
    }
}