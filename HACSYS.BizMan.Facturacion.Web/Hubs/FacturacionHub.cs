﻿using HACSYS.BizMan.DAL.EF6;
using HACSYS.BizMan.DAL.EF6.Repositories;
using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.DAL.Repositories;
using HACSYS.BizMan.Facturacion.DAL.EF6.Repositories;
using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.BizMan.Facturacion.Web.Services;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.FacturacionWeb.Identity;
using HACSYS.Management.DAL.Models;
using Microsoft.AspNet.SignalR;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Hubs
{
    public class FacturacionHub : Hub
    {
        private static Dictionary<int, string> Connections = new Dictionary<int, string>();

        public static Dictionary<string, List<Venta>> VentasNoDisponibles = new Dictionary<string, List<Venta>>();

        private IVentaService VentaService;
        private IConfigFacturacionService Config;
        private IProductoRepository RepositorioProductos;
        private Logger Log = LogManager.GetLogger("FacturacionHub");
        private EmailService EmailSender;

        public FacturacionHub()
        {
            var context = new FacturacionDbContext();
            VentaService = new VentaService(new VentaRepository(context), new ImpuestoRepository(context), new ProductoRepository(context), new DatosPagoService(new CondicionDePagoRepository(context), new MetodoDePagoRepository(context), new FormaDePagoRepository(context)));
            Config = new ConfigFacturacionService(new ConfigFacturacionRepository(context));
            RepositorioProductos = new ProductoRepository(context);
            EmailSender = new EmailService();
        }

        public bool AddToBusy(Venta element, string id)
        {
            if (element == null)
                return false;

            lock (VentasNoDisponibles)
            {
                List<Venta> ListValidator;
                VentasNoDisponibles.TryGetValue(id, out ListValidator);
                if (ListValidator == null)
                {
                    ListValidator = new List<Venta>();
                    ListValidator.Add(element);
                    VentasNoDisponibles.Add(id, ListValidator);

                }
                else
                {
                    ListValidator.Add(element);
                }

            }
            return true;
        }

        public bool ConnectClient(int EmpresaID)
        {
            Log.Info($"Empresa {EmpresaID} conectada");
            // Se obtiene el ConnectionID del cliente que se conectará al Hub
            var connectionID = Context.ConnectionId;

            // Llama al método "Add" que se encarga de almacenar en un hash las conexiones locales
            Add(EmpresaID, connectionID);

            return true;
        }

        public async Task<RemoveServerResult> RemoveDespachoOcupado(Venta venta)
        {
            string connectionId = Context.ConnectionId;
            List<Venta> listaventas = new List<Venta>();
            VentasNoDisponibles.TryGetValue(connectionId, out listaventas);
            Venta auxRemove = listaventas.FirstOrDefault(q => q.Folio.Equals(venta.Folio));
            listaventas.Remove(auxRemove);
            VentasNoDisponibles.Remove(connectionId);
            VentasNoDisponibles.Add(connectionId, listaventas);

            return new RemoveServerResult() { response = RemoveResponseCode.SUCCESS, Ventas = listaventas };
            
        }

        public void FacturaClear()
        {
            var id = Context.ConnectionId;
            lock (VentasNoDisponibles)
            {
                VentasNoDisponibles.Remove(id);
            }
            int i = VentasNoDisponibles.Count;
        }

        private void Add(int key, string connectionId)
        {
            try
            {
                // Hacemos un lock a Connections para poder trabajar en él
                lock (Connections)
                {
                    // Creamos el string auxiliar en el que se almacenará el string encontrado (Si se requiere trabajar con él)
                    string connections;
                    if (!Connections.TryGetValue(key, out connections))
                    {
                        // En caso de que el string no sea encontrado se agregará a la tabla de hash y quedará dentro del registro
                        Connections.Add(key, connectionId);
                    }else
                    {
                        Connections[key] = connectionId;
                    }
                }
            }
            catch (Exception ex)
            {
                var tmp = ex;
            }
        }

        public async Task<VentaServerResult> GetVentaServidor(int empresaID, string folio, double monto)
        {
            Log.Trace($"Buscando Folio({folio} con Monto ({monto}) en empresa #{empresaID})");
            // Regresar Datos Invalidos si el folio llega en blanco
            if (string.IsNullOrWhiteSpace(folio))
            {
                return new VentaServerResult { ResultCode = ResponseCode.INVALID_DATA };
            }

            lock (VentasNoDisponibles)
            {
                
                //Buscamos si está en la lista de despachos ocupados (comparando con el folio del ticket y la empresa)
                var ocupado = VentasNoDisponibles.Values.Any(l => l.Any(d => d.Folio.Equals(folio) && d.EmpresaID == empresaID));
                if (ocupado)
                    return new VentaServerResult { ResultCode = ResponseCode.BUSY };
            }

            // Buscar en base de datos local
            var dbCentral = await VentaService.Get(empresaID, folio, monto);

            // Si la venta es encontrada localmente, convertirlo a tipo Venta y regresarlo al cliente
            if (dbCentral.Status == VentaStatus.Success)
            {
                // Regresar Venta_Used si el venta ya fue facturado antes
                if (dbCentral.Venta.Activar)
                {
                    return new VentaServerResult { ResultCode = ResponseCode.VENTA_USED };
                }

                AddToBusy(dbCentral.Venta, Context.ConnectionId);
                return new VentaServerResult { ResultCode = ResponseCode.SUCCESS, Venta = dbCentral.Venta };
            }

            // Si se encontró localmente pero el monto no coincide, informar al cliente que hay datos inválidos
            if (dbCentral.Status == VentaStatus.DataMissMatch)
            {
                return new VentaServerResult { ResultCode = ResponseCode.INVALID_DATA };
            }

            // Buscar en estación si no fue encontrado localmente
            // Obtener el connectionID de la estación a la que hay que ir a buscar
            string empresaConnectionID;
            Connections.TryGetValue(empresaID, out empresaConnectionID);

            // Regresar datos inválidos si la estación no fue encontrada
            if (empresaConnectionID == null)
            {
                //TODO: Envia correo cuando no encuentra estacion
                Log.Info($"Enviando correo por que no se conecta la estación: {empresaID}");
                await EmailSender.SendAsync(empresaID);
                Log.Info($"Email se esta enviando por no conexion a estacion: {empresaID} ");
                return new VentaServerResult { ResultCode = ResponseCode.UNAVAILABLE };
            }

            // Enviar Request a la estación y notificar al cliente que se esta haciendo la busqueda
            var ClienteConnectionID = Context.ConnectionId;
            Clients.Client(empresaConnectionID).GetVentaEmpresa(ClienteConnectionID, folio, monto);
            return new VentaServerResult { ResultCode = ResponseCode.SEARCHING };
        }

        // TODO: Que pasa si un cliente hace doble click, no ha forma de saber que respuesta corresponde a que solicitud
        public void GetVentaResult(string clienteConnectionID, Venta venta)
        {
            var result = new VentaServerResult();
            if (venta != null)
            {
                if (String.IsNullOrWhiteSpace(venta.Folio))
                {
                    result.ResultCode = ResponseCode.INVALID_DATA;
                    result.Message = "Solamente se pueden facturar tickets pagados con efectivo o tarjeta.";
                    Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                    return;
                }

                Log.Trace($"Venta recibida de cliente {clienteConnectionID}, Venta({venta.Folio},{venta.Monto},#{venta.EmpresaID})");
                // Obtener la empresa que envió el resultado
                var empresaConnectionID = Context.ConnectionId;
                var valuePair = Connections.FirstOrDefault(kvp => kvp.Value.Equals(empresaConnectionID));

                // Asignando ids de los productos locales
                var productos = from detalles in venta.Detalles select detalles.Producto;
                foreach(var producto in productos)
                {
                    var productoLocal = RepositorioProductos.Get(p => p.Clave == producto.Clave && p.Concepto == producto.Concepto && p.ValorUnitario == p.ValorUnitario).FirstOrDefault();
                    producto.ProductoID = productoLocal == null ? 0 : productoLocal.ProductoID;
                }

                // Si corresponde al valor default significa que no fue encontrado
                if (valuePair.Equals(default(KeyValuePair<int, string>)))
                {
                    Log.Info($"Empresa con connectionID {Context.ConnectionId} no encontrada en listado");
                    // El request fue enviado por una estación que no esta en nuestra lista de conectadas
                    result.ResultCode = ResponseCode.INVALID_DATA;
                    Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                    return;
                }

                // Obtener el id de la empresa
                Log.Trace("Obteniendo Configuracion");
                ConfigFacturacion empresaConfig = Config.getConfig(venta.EmpresaID);

                if(empresaConfig == null)
                {
                    Log.Warn("EmpresaConfig es nulo");
                }

                // Obtener la lista de ventas del cliente
                List<Venta> ventasGuardadas = null;
                VentasNoDisponibles.TryGetValue(clienteConnectionID, out ventasGuardadas);

                // Checar que los datos de las ventas sean las mismas (MetodoDePago, ReferenciaDePago, CondicionDePago)
                if(ventasGuardadas != null && ventasGuardadas.Count > 0)
                {
                    // Checar si no hemos recibido ya la venta
                    if(ventasGuardadas.Any(v=>v.Folio == venta.Folio && v.EmpresaID == venta.EmpresaID && v.Monto == venta.Monto))
                    {
                        return;
                    }

                    var ventaGuardada = ventasGuardadas.FirstOrDefault();
                    // TODO: Cambiar de Any a FirstOrDefault??
                    if (ventaGuardada.MetodoDePago.Metodo != venta.MetodoDePago.Metodo)
                    {
                        result.ResultCode = ResponseCode.INVALID_DATA;
                        result.Message = $"El metodo de pago del ticket buscado ({venta.MetodoDePago.Metodo}), es diferente a los que ya han sido agregados ({ventaGuardada.MetodoDePago.Metodo}).";
                        Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                        return;
                    }
                    if(ventaGuardada.ReferenciaDePago != venta.ReferenciaDePago)
                    {
                        result.ResultCode = ResponseCode.INVALID_DATA;
                        result.Message = $"La referencia de pago del ticket buscado ({venta.ReferenciaDePago}), es diferente a los que ya han sido agregados ({ventaGuardada.ReferenciaDePago}).";
                        Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                        return;
                    }
                    if(ventaGuardada.FormaDePago.Forma != venta.FormaDePago.Forma) {
                        result.ResultCode = ResponseCode.INVALID_DATA;
                        result.Message = $"La forma de pago del ticket buscado ({venta.FormaDePago.Forma}), es diferente a los que ya han sido agregados ({ventaGuardada.FormaDePago.Forma}).";
                        Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                        return;
                    }
                    if(ventaGuardada.CondicionDePago.Condicion != venta.CondicionDePago.Condicion)
                    {
                        result.ResultCode = ResponseCode.INVALID_DATA;
                        result.Message = $"La condición de pago del ticket buscado ({venta.CondicionDePago.Condicion}), es diferente a los que ya han sido agregados ({ventaGuardada.CondicionDePago.Condicion}).";
                        Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                        return;
                    }

                    if(ventasGuardadas.Any(v=>v.EmpresaID != venta.EmpresaID))
                    {
                        result.ResultCode = ResponseCode.INVALID_DATA;
                        result.Message = "Los Tickets no pueden ser de diferentes Estaciones";
                        Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                        return;
                    }
                }

                Log.Info($"Los días limite son {empresaConfig.DiasLimite}");
                var test = venta.Fecha.AddDays(empresaConfig.DiasLimite);
                Log.Info($"La fecha con días limite agregados es: {test}");
                Log.Info($"La fecha del ticket es {test}");
                if ((venta.Fecha.Month == DateTime.Now.Month) || (empresaConfig.DiasLimite > 0 && (test.Month == DateTime.Now.Month)))
                {

                    AddToBusy(venta, clienteConnectionID);
                    result.ResultCode = ResponseCode.SUCCESS;
                    result.Venta = venta;
                    Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                    return;
                }else
                {
                    result.ResultCode = ResponseCode.DATE_EXPIRED;
                    result.Message = "Ticket Expirado.";
                    Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
                    return;
                }
            }
            Log.Trace("Venta no encontrada");
            result.ResultCode = ResponseCode.NOT_FOUND;
            // Enviar respuesta al cliente
            Clients.Client(clienteConnectionID).OnVentaSearchResult(result);
        }
#pragma warning disable 1998
        public override async Task OnDisconnected(bool stopCalled)
        {
            if (VentasNoDisponibles.ContainsKey(Context.ConnectionId))
            {
                VentasNoDisponibles.Remove(Context.ConnectionId);
            }
        }
#pragma warning restore 1998

        public class VentaServerResult
        {
            public string Message { get; internal set; }
            public ResponseCode ResultCode { get; set; }
            public Venta Venta { get; set; }
        }

        public class RegistrationServerResult
        {
            public RegistrationResponseCode ResultCode { get; set; }
            public Cliente cliente { get; set; }
        }

        public class RemoveServerResult
        {
            public ICollection<Venta> Ventas;
            public RemoveResponseCode response;
        }
    }

    public enum ResponseCode { BUSY, INVALID_DATA, SEARCHING, SUCCESS, NOT_FOUND, VENTA_USED, DATE_EXPIRED, UNAVAILABLE}
    public enum RegistrationResponseCode { SUCCESS, ALREADY_REGISTERED, INVALID_DATA }
    public enum RemoveResponseCode { SUCCESS = 200, ERROR = 500 }
}
