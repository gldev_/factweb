﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.Facturacion.Web.Identity;
using HACSYS.FacturacionWeb.Identity;
using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.Management.DAL.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HACSYS.BizMan.Facturacion.Web
{
    public class InitialData
    {
        public static async Task InitializeDatabaseAsync(FacturacionDbContext context)
        {
            try
            {
                var sqlServerDatabase = context.Database;// as SqlServerDatabase;
                if (sqlServerDatabase != null)
                {
                    if (sqlServerDatabase.CreateIfNotExists())
                    {
                        await InsertInitialData(context);
                        //await CreateAdminUser(context);
                    }
                }
            }
            catch (Exception e)
            {
                string a = e.Message;
            }
        }

            const string adminRole = "Admin";
            const string defaultAdminUserName = "admin@hacsys.com";
            const string defaultAdminPassword = "Hacsys 1234";
            private static async Task CreateAdminUser(FacturacionDbContext context)
            {
                var userStore = new UserStore<Usuario, Rol, int, UsuarioLogin, UsuarioRol, UsuarioClaim>(context);
                var userManager = new UsuarioManager(userStore);

                var rolStore = new RoleStore<Rol, int, UsuarioRol>(context);
                var rolManager = new RolManager(rolStore);

                if (!await rolManager.RoleExistsAsync(adminRole))
                {
                    await rolManager.CreateAsync(new Rol() { Name = adminRole });
                }
                var user = await userManager.FindByNameAsync(defaultAdminUserName);
                if (user == null)
                {
                    var db = context as FacturacionDbContext;
                    var dir = db.Direcciones.FirstOrDefault() ?? new Direccion() {  };
                    var datosFiscales = new DatosFiscales() { RazonSocial = "Eduardo Gonzalez", RFC = "GOGE910221HCL", Direccion = dir };
                    var cliente = new Cliente() { DatosFiscales = datosFiscales, Entidad = db.Entidades.FirstOrDefault() };
                    db.Clientes.Add(cliente);
                    user = new Usuario { UserName = defaultAdminUserName, Cliente = cliente };
                    var result = await userManager.CreateAsync(user, defaultAdminPassword);
                    await userManager.AddToRoleAsync(user.Id, adminRole);
                    //await userManager.AddClaimAsync(user, new Claim("ManageStore", "Allowed"));
                }
            }
        private static async Task InsertInitialData(FacturacionDbContext context)
        {
            var mex = GetPaisConEstados();
            context.Paises.Add(mex);
            Entidad raiz = new Entidad() { Nombre = "HACSYS", Jerarquia = "/" };
            context.Entidades.Add(raiz);

            // Impuestos básicos
            var iva = new Impuesto { Nombre = "IVA 16%", Tasa = 16, NombreSAT = "IVA", Tipo = TipoImpuesto.TRASLADADO, Activo = true };
            var ieps = new Impuesto { Nombre = "IEPS", Tasa = 43, NombreSAT = "IEPS", Tipo = TipoImpuesto.TRASLADADO, Activo = true, Oculto = true };
            context.Impuestos.AddRange(new List<Impuesto> { iva, ieps });

            // Formas de Pago
            var unaExhibicion = new FormaDePago() { Forma = "Pago en una sola Exhibición" };
            var parcialidades = new FormaDePago() { Forma = "Parcialidades" };
            var parcialidadesSinConcepto = new FormaDePago() { Forma = "Parcialidades sin concepto" };
            context.FormasDePago.AddRange(new List<FormaDePago> { unaExhibicion, parcialidades, parcialidadesSinConcepto });

            // Metodos de pago
            var cheque = new MetodoDePago() { Metodo = "Cheque" };
            var efectivo = new MetodoDePago() { Metodo = "Efectivo" };
            var transferencia = new MetodoDePago() { Metodo = "Transferencia Bancaria" };
            var tarjeta = new MetodoDePago() { Metodo = "Tarjeta" };
            context.MetodosDePago.AddRange(new List<MetodoDePago> { cheque, efectivo, transferencia, tarjeta });

            // Condiciones de pago
            var noAplica = new CondicionDePago() { Condicion = "No Aplica" };
            var credito = new CondicionDePago() { Condicion = "Crédito" };
            context.CondicionesDePago.AddRange(new List<CondicionDePago> { noAplica, credito });

            await context.SaveChangesAsync();
        }

        private static Pais GetPaisConEstados()
        {
            var estados = new List<Estado>();
            Pais mx = new Pais() { Clave = "MX", Nombre = "México", Estados = estados };
            Estado Aguascalientes = new Estado() { Clave = "AGS", Nombre = "Aguascalientes", Pais = mx };
            Estado BajaCalifornia = new Estado() { Clave = "BC", Nombre = "Baja California", Pais = mx };
            Estado BajaCaliforniaSur = new Estado() { Clave = "BCS", Nombre = "Baja California Sur", Pais = mx };
            Estado Campeche = new Estado() { Clave = "CAMP", Nombre = "Campeche", Pais = mx };
            Estado Chihuahua = new Estado() { Clave = "CHIH", Nombre = "Chihuahua", Pais = mx };
            Estado Chiapas = new Estado() { Clave = "CHIS", Nombre = "Chiapas", Pais = mx };
            Estado Coahuila = new Estado() { Clave = "COAH", Nombre = "Coahuila", Pais = mx };
            Estado Colima = new Estado() { Clave = "COL", Nombre = "Colima", Pais = mx };
            Estado DistritoFederal = new Estado() { Clave = "DF", Nombre = "Distrito Federal", Pais = mx };
            Estado Durango = new Estado() { Clave = "DGO", Nombre = "Durango", Pais = mx };
            Estado Guerrero = new Estado() { Clave = "GRO", Nombre = "Guerrero", Pais = mx };
            Estado Hidalgo = new Estado() { Clave = "HGO", Nombre = "Hidalgo", Pais = mx };
            Estado Jalisco = new Estado() { Clave = "JAL", Nombre = "Jalisco", Pais = mx };
            Estado EstadodeMexico = new Estado() { Clave = "MEX", Nombre = "Estado de México", Pais = mx };
            Estado Michoacán = new Estado() { Clave = "MICH", Nombre = "Michoacán", Pais = mx };
            Estado Morelos = new Estado() { Clave = "MOR", Nombre = "Morelos", Pais = mx };
            Estado NuevoLeon = new Estado() { Clave = "NL", Nombre = "Nuevo León", Pais = mx };
            Estado Nayarit = new Estado() { Clave = "NAY", Nombre = "Nayarit", Pais = mx };
            Estado Oaxaca = new Estado() { Clave = "OAX", Nombre = "Oaxaca", Pais = mx };
            Estado Puebla = new Estado() { Clave = "PUE", Nombre = "Puebla", Pais = mx };
            Estado QuintanaRoo = new Estado() { Clave = "QROO", Nombre = "Quintana Roo", Pais = mx };
            Estado Querétaro = new Estado() { Clave = "QRO", Nombre = "Querétaro", Pais = mx };
            Estado Sinaloa = new Estado() { Clave = "SIN", Nombre = "Sinaloa", Pais = mx };
            Estado Veracruz = new Estado() { Clave = "VER", Nombre = "Veracruz", Pais = mx };
            Estado Yucatan = new Estado() { Clave = "YUC", Nombre = "Yucatán", Pais = mx };
            Estado Zacatecas = new Estado() { Clave = "ZAC", Nombre = "Zacatecas", Pais = mx };
            Estado SanLuisPotosi = new Estado() { Clave = "SLP", Nombre = "San Luis Potosí", Pais = mx };
            Estado Sonora = new Estado() { Clave = "SON", Nombre = "Sonora", Pais = mx };
            Estado Tabasco = new Estado() { Clave = "TAB", Nombre = "Tabasco", Pais = mx };
            Estado Tamaulipas = new Estado() { Clave = "TAMPS", Nombre = "Tamaulipas", Pais = mx };
            Estado Tlaxcala = new Estado() { Clave = "TLAX", Nombre = "Tlaxcala", Pais = mx };
            estados.Add(Aguascalientes);
            estados.Add(BajaCalifornia);
            estados.Add(BajaCaliforniaSur);
            estados.Add(Campeche);
            estados.Add(Chihuahua);
            estados.Add(Chiapas);
            estados.Add(Coahuila);
            estados.Add(Colima);
            estados.Add(DistritoFederal);
            estados.Add(Durango);
            estados.Add(Guerrero);
            estados.Add(Hidalgo);
            estados.Add(Jalisco);
            estados.Add(EstadodeMexico);
            estados.Add(Michoacán);
            estados.Add(Morelos);
            estados.Add(NuevoLeon);
            estados.Add(Nayarit);
            estados.Add(Oaxaca);
            estados.Add(Puebla);
            estados.Add(QuintanaRoo);
            estados.Add(Querétaro);
            estados.Add(Sinaloa);
            estados.Add(Veracruz);
            estados.Add(Yucatan);
            estados.Add(Zacatecas);
            estados.Add(SanLuisPotosi);
            estados.Add(Sonora);
            estados.Add(Tabasco);
            estados.Add(Tamaulipas);
            estados.Add(Tlaxcala);

            return mx;
        }
    }
}