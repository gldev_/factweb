﻿"use strict";
var gulp = require('gulp');
var bower = require('gulp-bower');
var del = require('del');
var mainBowerFiles = require('main-bower-files');

var project = require('./project.json');
var webroot = project.webroot;
var lib = webroot + '/lib';

gulp.task('default', ['bower:install','wiredep'], function () {
    return;
});

gulp.task('bower:install-dev', function () {
    return bower({
        directory: './Libraries'
    });
});

gulp.task('build',function()
{
    return gulp.src(mainBowerFiles({
        paths: {
            bowerDirectory: './Libraries'
        }
    })).pipe(gulp.dest(lib));
})

gulp.task('build:clean', function (done) {
    del(lib, done);
});

gulp.task('wiredep', function () {

    var wiredep = require('wiredep').stream;
    var options = {
        bowerJson: require('./bower.json'),
        directory: lib,
        ignorePath: './wwwroot',
    };

    return gulp
        .src('./Views/index.html')
        .pipe(wiredep(options))
        .pipe(gulp.dest(webroot));
});