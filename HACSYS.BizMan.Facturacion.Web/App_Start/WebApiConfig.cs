﻿using HACSYS.BizMan.ARES.DAL.EF6;
using HACSYS.BizMan.ARES.DAL.EF6.Repositories;
using HACSYS.BizMan.ARES.DAL.Repositories;
using HACSYS.BizMan.Facturacion.Web.Hubs;
using HACSYS.BizMan.Facturacion.Web.Services;
using HACSYS.FacturacionWeb.Identity;
using HACSYS.Management.DAL.EF6;
using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.Management.DAL.EF6.Repositories;
using HACSYS.Management.DAL.Repositories;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HACSYS.BizMan.Facturacion.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Newtonsoft.Json.Formatting.Indented,
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };
        }
    }
}
