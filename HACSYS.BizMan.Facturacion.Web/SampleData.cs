﻿
using HACSYS.BizMan.ARES.DAL.Models;
using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.BizMan.Facturacion.Web.Identity;
using HACSYS.FacturacionWeb.Identity;
using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.Management.DAL.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HACSYS.FacturacionWeb
{
    public static class SampleData
    {
        public static async Task InitializeDatabaseAsync(FacturacionDbContext context)
        {

            var nl = context.Estados.FirstOrDefault(e => e.Clave.Equals("NL"));
            Direccion dir = new Direccion() { Calle = "Puerto Rico", CodigoPostal = "64000", Colonia = "Vista Hermosa", Estado = nl, Localidad = "Monterrey", Municipio = "Monterrey", NumExterior = "123", NumInterior = "2" };
            context.Direcciones.Add(dir);
            DatosFiscales df = new DatosFiscales() { Correo = "demo@gmail.com", Direccion = dir, RazonSocial = "Demo SA de CV", RFC = "TME840315KT6", Telefono = "812345678" };
            context.DatosFiscales.Add(df);
            var raiz = context.Entidades.FirstOrDefault();
            Entidad demo = new Entidad() { Nombre = "Demo", Jerarquia = "/1/", Padre = raiz };
            context.Entidades.Add(demo);
            Empresa empresa = new Empresa() { Nombre = "Empresa Demo", DatosFiscales = df, Entidad = demo };
            context.Empresas.Add(empresa);
            Estacion estacion = new Estacion { ClavePemex = "1234567890000", Empresa = empresa };
            context.Estaciones.Add(estacion);
            ConfigFacturacion config = new ConfigFacturacion { Empresa = empresa, RutaLlavePrivada = @"CertificadosTest\CSD01_AAA010101AAA.key", RutaCertificado = @"CertificadosTest\CSD01_AAA010101AAA.cer", Password = "12345678a", Serie = "A", FolioInicial = 1 };
            context.ConfiguracionesFacturacion.Add(config);
            await context.SaveChangesAsync();


        }
    }
}