﻿using HACSYS.BizMan.Facturacion.Web.Models;
using HACSYS.Management.DAL.EF6;
using HACSYS.Management.DAL.EF6.Repositories;
using HACSYS.Management.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Repositories
{
    public class UserTokenRepository : GenericAsyncRepository<UserToken>, IUserTokenRepository, IGenericAsyncRepository<UserToken>, IGenericRepository<UserToken>
    {
        //public UserTokenRepository(ManagementDbContext context);
    }
}
