﻿using HACSYS.BizMan.Facturacion.Web.Models;
using HACSYS.Management.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Repositories
{
    public interface IUserTokenRepository : IGenericRepository<UserToken>
    {
    }
}
