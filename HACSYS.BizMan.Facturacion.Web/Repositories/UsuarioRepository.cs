﻿using HACSYS.Management.DAL.EF6;
using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.Management.DAL.EF6.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HACSYS.BizMan.Facturacion.Web.Repositories
{
    public class UsuarioRepository : GenericAsyncRepository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(ManagementDbContext context):base(context)
        {

        }
    }
}