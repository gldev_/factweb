﻿using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.Management.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Repositories
{
    public interface IUsuarioRepository : IGenericAsyncRepository<Usuario>
    {
    }
}
