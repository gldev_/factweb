﻿using HACSYS.BizMan.ARES.DAL.Models;
using HACSYS.BizMan.ARES.DAL.Repositories;
using HACSYS.FacturacionWeb.Identity;
using HACSYS.Management.DAL.EF6.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HACSYS.BizMan.Facturacion.Web.Repositories
{
    public class EstacionRepository : GenericAsyncRepository<Estacion>, IEstacionRepository
    {
        public EstacionRepository(FacturacionDbContext context):base(context)
        {

        }
    }
}