﻿using HACSYS.BizMan.Facturacion.Web.Models;
using HACSYS.BizMan.Facturacion.Web.Services;
using HACSYS.Management.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System;

namespace HACSYS.FacturacionWeb.Controllers
{
    [RoutePrefix("api/Cliente")]
    public class ClienteController : ApiController
    {
        IClienteService ClienteService;
        IDatosFiscalesService FiscalesService;
        //public ClienteController(IClienteService clienteService)
        //{
        //    ClienteService = clienteService;
        //}

        public ClienteController(IClienteService clienteService, IDatosFiscalesService fiscalesService)
        {
            ClienteService = clienteService;
            FiscalesService = fiscalesService;
        }
    

        [HttpPost]
        [Route("GetClientDetails")]
        public async Task<Cliente> GetClientDetails([FromBody]int id)
        {
            return await ClienteService.GetClienteFromUserID(id);
        }

        [HttpPost]
        [Route("AddMail")]
        public async Task<Boolean> AddMailClient([FromBody]MailViewModel data)
        {

            if (data == null)
                return false;

            await ClienteService.addMail(data.ClientID, data.Mail,data.UMail);
            return true;
        }

        //[HttpPost]
        //public async Task<Cliente> getClientDetails(int id)
        //{
        //    return await IRCR.GetClientDetails(id);
        //}

        [HttpPost]
        [Route("updateDireccion")]
        public async Task<Direccion> updateDireccion(NuevaDireccion nueva)
        {
            //nueva.direccion.Estado.Pais = nueva.pais;
            //nueva.direccion.Estado.Direcciones = null;
            //return ClienteService.UpdateClientDetails(nueva.direccion, nueva.userID);
            //Direccion entidad = ClienteService.UpdateClientDetails(nueva.direccion, nueva.userID);
            int clienteID = ClienteService.GetClienteFromUserID(nueva.userID).Result.ClienteID;
            await FiscalesService.changeDatos(nueva.direccion, clienteID);
            return nueva.direccion;
        }

        [HttpPost]
        public async Task<IEnumerable<Direccion>> getClienteDirecciones(int id)
        {
                return await ClienteService.GetDirecciones(id);
        }

        [HttpGet]
        public async Task<IEnumerable<Direccion>> getClienteDireccionesGet(int id)
        {
                return await ClienteService.GetDirecciones(id);
        }

        [HttpPost]
        public Cliente changeDireccion(Contenido data)
        {
            
            return ClienteService.changeDireccion(data.ClienteID, data.direccionNueva , data.direccion);
        }

        [HttpPost]
        public async Task<UserToken> getRFC(tokenViewModel tokenViewModel)
        {
            string Token = tokenViewModel.token;
            return await ClienteService.UToken(Token);
        }

        [HttpGet]
        public async Task<UserToken> GEtRFC(string token)
        {
            return await ClienteService.UToken(token);
        }

        [HttpGet]
        [Authorize]
        [Route("DatosFiscales")]
        public async Task<DatosFiscales> DatosFiscales()
        {
            var cliente = await ClienteService.GetClienteFromUserID(User.Identity.GetUserId<int>());
            if (cliente == null)
                return null;

            return cliente.DatosFiscales;
        }


    }

    public class Contenido
    {
        public int ClienteID { get; set; }

        public Direccion direccionNueva { get; set; }

        public Direccion direccion { get; set; }
    }

    public class NuevaDireccion
    {
        public Direccion direccion { get; set; }
        public Pais pais { get; set; }
        public int userID { get; set; }   
    }

    public class tokenViewModel
    {
        public string token { get; set; }
    }

    public class MailViewModel
    {
        public int ClientID { get; set; }
        public string Mail { get; set; }
        public string UMail { get; set; }
    }
}
