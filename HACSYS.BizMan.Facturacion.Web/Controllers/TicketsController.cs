﻿using System.Threading.Tasks;
using HACSYS.BizMan.Facturacion.Web.Services;
using HACSYS.BizMan.Facturacion.Web.Results;
using System.Web.Http;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.BizMan.DAL.Models;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HACSYS.BizMan.Facturacion.Web.Controllers
{
    //[Route("api/[controller]/[action]")]
    [RoutePrefix("api/Ticket")]
    public class TicketsController : ApiController
    {
        private IVentaService TicketService;

        public TicketsController(IVentaService ticketService)
        {
            TicketService = ticketService;
        }

        
        [Authorize]
        [HttpGet]
        public async Task<VentaResponse> Get(int estacion, string folio, double cantidad)
        {
            return await TicketService.Get(estacion, folio, cantidad);
        }

       
        [HttpPost]
        [Route("GuardaReferencia")]
        public async Task<VentaResponse> GuardaReferencia(Venta venta)
        {
            return await TicketService.ActualizaReferencia(venta);
        }

    }
}
