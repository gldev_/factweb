﻿using HACSYS.BizMan.Facturacion.Web.Identity;
using HACSYS.BizMan.Facturacion.Web.Models;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.Management.DAL.Models;
using HACSYS.Management.DAL.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NLog;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace HACSYS.BizMan.Facturacion.Web.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private UsuarioManager _userManager;
        private Logger Log = LogManager.GetCurrentClassLogger();
        private IEstadoRepository RepositorioEstado;
        private int EntidadID;
        public AccountController()
        {
            EntidadID = Int32.Parse(WebConfigurationManager.AppSettings["EntidadID"]);
        }

        //private AccountController(IEstadoRepository repositorioEstado)
        //{
        //    RepositorioEstado = repositorioEstado;
        //}

        public UsuarioManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<UsuarioManager>();
        }
            private set
            {
                _userManager = value;
            }
        }
       
        private UsuarioSignInManager _signInManager;

        public UsuarioSignInManager SignInManager
        {
            get 
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<UsuarioSignInManager>();
            }
            private set { _signInManager = value; }
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [Route("Login")]
        public async Task<AccountResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return new AccountResult() { Result = "Bad Request", StatusCode = 400 };
            }

            // This doen't count login failures towards lockout only two factor authentication
            // To enable password failures to trigger lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: true);

            Usuario a = UserManager.Find(model.Email, model.Password);
            
            switch (result)
            {
                case SignInStatus.Success:
                    int idc = a.Cliente.ClienteID;
                    return new AccountResult { Result = "Success", StatusCode = 200, clientID = idc };
                case SignInStatus.LockedOut:
                    return new AccountResult { Result = "LockOut", StatusCode = 200 };
                case SignInStatus.RequiresVerification:
                    return null;//RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    return new AccountResult { Result = "Invalid Credentials", StatusCode = 401 };
            }
        }

        [HttpGet]
        [Route("UserDetails")]
        //[Authorize]
        public async Task<UserDetails> UserDetails()
        {
            try
            {
                var usuario = await GetCurrentUserAsync();
                var lista = await UserManager.GetRolesAsync(usuario.Id);
                var result = new UserDetails
                {
                    UserName = usuario.UserName,
                    Roles = lista.ToList<string>(),
                    userID = usuario.Id
                };
                return result;
            }
            catch (Exception ex)
            {
                var result = new UserDetails
                {
                    UserName = null,
                    Roles = null
                };

                return result;
            }
        }

        private Task<Usuario> GetCurrentUserAsync()
        {
            return UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
        }

        [HttpPost]
        [Route("Register/{id:int?}")]
        public async Task<AccountResult> Register([FromBody] RegisterViewModel vm, int? id = null )
        {
            Log.Trace("Registrando Usuario");
            if (!ModelState.IsValid || string.IsNullOrWhiteSpace(vm.Account.Email) || string.IsNullOrWhiteSpace(vm.Account.Password))
            {
                return new AccountResult { Result = "Bad Request", StatusCode = 400 };
                //return new JsonResult(new AccountResult { Result = "Bad Request", StatusCode = 400 });
            }
            //var cliente = new Cliente() { Direcciones =  vm.Direccion , RazonSocial = vm.Cliente.RazonSocial, RFC = vm.Cliente.RFC, EmailNotificaciones = vm.Account.Email };
            // TODO: Obtener los datos del estado desde la BD

            vm.DatosFiscales.Direccion.EstadoID = vm.DatosFiscales.Direccion.Estado.EstadoID;
            vm.DatosFiscales.Direccion.Estado = null;
            var cliente = new Cliente() { DatosFiscales = vm.DatosFiscales, EntidadID = EntidadID};
            if (id.HasValue)
                cliente.ClienteID = id.Value;
            var usuario = new Usuario { UserName = vm.Account.Email, Email = vm.Account.Email, Cliente = cliente };
            Log.Trace("Agregando usuario a la base de datos");
            try {
                var result = await UserManager.CreateAsync(usuario, vm.Account.Password);
                if (result.Succeeded)
                {
                    Log.Trace("Enviando correo de confirmación");
                    var code = await UserManager.GenerateEmailConfirmationTokenAsync(usuario.Id);
                    var callbackUrl = Url.Link("DefaultApi", new { Controller = "Account", Action = "ConfirmEmail", userId = usuario.Id, code = code });
                    await UserManager.SendEmailAsync(usuario.Id, "Confirmación de cuenta", callbackUrl);
                    Log.Trace("Registro exitoso");
                    return new AccountResult { Result = "Success", StatusCode = 200 };
                }
                else
                {
                    return new AccountErrorsResult { Result = "Registration Errors, see error details", StatusCode = 400, Errores = result.Errors.ToList<string>() };
                    //return new JsonResult(new AccountErrorsResult { Result = "Registration Errors, see error details", StatusCode = 400, Errores = result.Errors.Select(e => e.Description).ToList<string>() });
                }
            }
            catch (Exception e)
            {
                return new AccountErrorsResult { Result = "Errores de servidor", StatusCode = 500, Errores = new System.Collections.Generic.List<string> { e.ToString() } };
            }
        }


        [AllowAnonymous]
        [HttpGet]
        public async Task<HttpResponseMessage> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return null;
            }

            var result = await UserManager.ConfirmEmailAsync(Int32.Parse(userId), code);
            if(result.Succeeded)
            {
                var response = Request.CreateResponse(HttpStatusCode.Moved);
                response.Headers.Location = new Uri(Request.RequestUri, RequestContext.VirtualPathRoot+"#/Confirmacion");
                return response;
            }
            return null;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<AccountResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return new AccountResult { StatusCode = 500 };
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = $"http://{HttpContext.Current.Request.Url.Host}/#/ResetPassword/{user.Id}/{code}";
                //var callbackUrl = Url.Link("DefaultApi", new { Controller = "Account", Action = "ResetPassword", userId = user.Id, code = code });
                await UserManager.SendEmailAsync(user.Id, "Regenerar Contraseña", callbackUrl);
                return new AccountResult { StatusCode = 200 };
            }
            return new AccountResult { StatusCode = 500 };
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [Route("ResetPassword")]
        public async Task<AccountResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return new AccountResult { StatusCode = 500 };
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return new AccountResult { StatusCode = 500 };
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return new AccountResult { StatusCode = 200 };
            }
            AddErrors(result);
            return new AccountErrorsResult { StatusCode = 500, Errores = result.Errors.ToList()};
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<AccountResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return new AccountResult { StatusCode = 500 };
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user == null || !(await UserManager.CheckPasswordAsync(user,model.OldPassword)))
            {
                // Don't reveal that the user does not exist
                return new AccountResult { StatusCode = 500 };
            }
            var result = await UserManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return new AccountResult { StatusCode = 200 };
            }
            AddErrors(result);
            return new AccountErrorsResult { StatusCode = 500, Errores = result.Errors.ToList() };
        }

        //
        // POST: /Account/LogOff
        [HttpGet]
        [Route("LogOff")]
        public void LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
            {
            get
                {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        #endregion
    }
}
