﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HACSYS.Management.DAL.Repositories;
using HACSYS.Management.DAL.Models;
using System.Web.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HACSYS.BizMan.Facturacion.Web.Controllers
{
    [Route("api/Empresas")]
    public class EmpresasController : ApiController
    {
        IEmpresaRepository RepositorioEmpresas;
        public EmpresasController(IEmpresaRepository repositorio)
        {
            RepositorioEmpresas = repositorio;
        }

        // GET: api/empresas
        [HttpGet]
        public async Task<IEnumerable<Empresa>> Get()
        {
            var empresas = await RepositorioEmpresas.GetAsync();
            return empresas.Select(e => new Empresa() { Nombre = e.Nombre, EmpresaID = e.EmpresaID });
        }

        // GET: api/empresas/5
        [HttpGet]
        public async Task<Empresa> Get(int id)
        {            
            var res = await RepositorioEmpresas.GetAsync(e => e.EmpresaID == id);
            return res.FirstOrDefault();
        }
    }
}
