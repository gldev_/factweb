﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HACSYS.Management.DAL.Models;
using HACSYS.Management.DAL.Repositories;
using HACSYS.BizMan.Facturacion.Web.Identity;
using System.Web.Http;
using System;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HACSYS.BizMan.Facturacion.Web.Controllers
{
    [RoutePrefix("api/Direcciones")]
    public class DireccionesController : ApiController
    {
        private IPaisRepository RepositorioPaises;
        public DireccionesController(IPaisRepository repositorioPaises)
        {
            RepositorioPaises = repositorioPaises;
        }
        // GET: api/Direcciones/Paises
        [HttpGet]
        [Route("Paises")]
        public async Task<IEnumerable<Pais>> GetPaises()
        {
            try
            {
                var paises = await RepositorioPaises.GetAsync();
                return paises;
            }catch(Exception e)
            {
                return null;
            }
        }
    }
}
