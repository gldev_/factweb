﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.Facturacion.DAL.Repositories;
using HACSYS.BizMan.Facturacion.Web.Hubs;
using HACSYS.BizMan.Facturacion.Web.Repositories;
using HACSYS.BizMan.Facturacion.Web.Results;
using HACSYS.BizMan.Facturacion.Web.Services;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.Management.DAL.EF6.Identity;
using HACSYS.Management.DAL.Repositories;
using Microsoft.AspNet.Identity;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;

namespace HACSYS.BizMan.Facturacion.Web.Controllers
{
    [RoutePrefix("api/Factura")]
    public class FacturaController : ApiController
    {

        //ITokenService ITSR;
        IDatosFiscalesRepository DatosFiscales;
        IFacturacionService FacturacionService;
        IUsuarioRepository RepositorioUsuarios;
        IComprobanteRepository RepositorioComprobantes;
        IEmailService EmailService;
        IClienteService ClienteService;

        private int EntidadID;
        private Logger Log = LogManager.GetCurrentClassLogger();

        public FacturaController(IDatosFiscalesRepository datosFiscales, IFacturacionService facturacionService, IUsuarioRepository repositorioUsuarios, IComprobanteRepository repositorioComprobantes, IEmailService emailService, IClienteService clienteservice)
        {
            DatosFiscales = datosFiscales;
            FacturacionService = facturacionService;
            RepositorioUsuarios = repositorioUsuarios;
            RepositorioComprobantes = repositorioComprobantes;
            EmailService = emailService;
            ClienteService = clienteservice;
            EntidadID = Convert.ToInt32(WebConfigurationManager.AppSettings["EntidadID"]);
        }


        [HttpPost]
        [Route("Facturar")]
        public async Task<FacturarResult> Facturar(FacturaViewModel factura)
        {

            if (factura == null)
            {
                return new FacturarResult { ResultCode = FacturarResultCode.INVALID_DATA };
            }
            else
            {
                factura.Cliente.EntidadID = EntidadID;
                var result = await FacturacionService.Facturar(factura.Cliente, factura.Ventas);
                
                if (result != null)
                {

                    await Email(result.FacturaID, factura.Correo);
                    return result;
                }

                else return new FacturarResult { ResultCode = FacturarResultCode.SERVER_ERROR };
            }
        }

        [HttpPost]
        [Route("FacturarCliente")]
        [Authorize]
        public async Task<FacturarResult> FacturarCliente(List<Venta> ventas)
        {
            if (ventas == null || ventas.Count == 0)
            {
                return new FacturarResult { ResultCode = FacturarResultCode.INVALID_DATA };
            }
            else
            {
                var usuario = await GetCurrentUserAsync();
                var result = await FacturacionService.Facturar(usuario.Cliente, ventas);

                if (result != null)
                {
                    await Email(result.FacturaID, usuario.Email);
                    return result;
                }
                else return null;
            }
        }

        [Route("GetPDF/{id:int}")]
        public async Task<HttpResponseMessage> GetPDF(int id, bool inline = false)
        {
            HttpResponseMessage result = null;
            Stream PDFStream = null;
            try
            {
                Log.Info($"Obteniendo el pdf de la factura {id}");
                PDFStream = await FacturacionService.GetPDFFactura(id);
            } catch (Exception e)
            {
                Log.Info($"Error, {e}");
                Log.Error(e, $"El error fue {e.Message}");
            }

            if (PDFStream == null)
            {
                result = Request.CreateResponse(HttpStatusCode.Gone);
            }
            else
            {// serve the file to the client
                var comprobante = RepositorioComprobantes.GetByID(id);
                if(comprobante == null)
                {
                    return Request.CreateResponse(HttpStatusCode.Gone);
                }
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(PDFStream);
                if (inline)
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = $"{comprobante.DatosFiscalesEmisor.RFC}_{comprobante.DatosFiscalesCliente.RFC}_{comprobante.Serie}{comprobante.Folio}_{comprobante.FechaCreada.Value.ToShortDateString()}.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    result.Content.Headers.ContentDisposition.FileName = $"{comprobante.DatosFiscalesEmisor.RFC}_{comprobante.DatosFiscalesCliente.RFC}_{comprobante.Serie}{comprobante.Folio}_{comprobante.FechaCreada.Value.ToShortDateString()}.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }

                result.Content.Headers.ContentLength = PDFStream.Length;
            }

            return result;

        }

        [Authorize]
        [HttpGet]
        [Route("{start:DateTime?}/{end:DateTime?}")]
        public async Task<List<ComprobanteViewModel>> Get(DateTime? start=null, DateTime? end=null)
        {
            var usuario = await GetCurrentUserAsync();
            if (start == null)
                start = DateTime.Now.AddDays(-31);
            if (end == null)
                end = DateTime.Now;
            end = end.Value.AddHours(23).AddMinutes(59);
            var comprobantes = RepositorioComprobantes.Get(c => c.Cliente.ClienteID == usuario.Cliente.ClienteID && c.FechaCreada >= start.Value && c.FechaCreada <= end.Value);
            return comprobantes.Select(c=>new ComprobanteViewModel { ComprobanteID = c.ComprobanteID, Fecha = c.FechaCreada.Value, Total = c.Total, Folio = c.Folio }).ToList();
        }

        [Route("GetXML/{id:int}")]
        public async Task<HttpResponseMessage> GetXML(int id)
        {
            HttpResponseMessage result = null;
            Stream XMLstream = null;
            try
            {
                XMLstream = await FacturacionService.GetXMLFactura(id);
            }
            catch (Exception e)
            {

            }

            if (XMLstream == null)
            {
                result = Request.CreateResponse(HttpStatusCode.Gone);
            }
            else
            {
                var comprobante = RepositorioComprobantes.GetByID(id);
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(XMLstream);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = $"{comprobante.DatosFiscalesEmisor.RFC}_{comprobante.DatosFiscalesCliente.RFC}_{comprobante.Serie}{comprobante.Folio}_{comprobante.FechaCreada.Value.ToShortDateString()}.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentLength = XMLstream.Length;
            }

            return result;
        }

        [HttpGet]
        [Authorize]
        [Route("Email/{id:int}/{correo?}")]
        public async Task Email(int id, string correo = null)
        {
            var user = await GetCurrentUserAsync();
            if (correo == null)
            {
                correo = user.Email;
            }

            try
            {
                Log.Info($"Obteniendo el comprobante {id}");
                var comprobante = RepositorioComprobantes.GetByID(id);
                Log.Info($"Comprobante {comprobante} listo");
                Stream PDF = await FacturacionService.GetPDFFactura(id);
                var pdfName = $"{comprobante.DatosFiscalesEmisor.RFC}_{comprobante.DatosFiscalesCliente.RFC}_{comprobante.Serie}{comprobante.Folio}_{comprobante.FechaCreada.Value.ToShortDateString()}.pdf";

                Stream XML = await FacturacionService.GetXMLFactura(id);
                var xmlName = $"{comprobante.DatosFiscalesEmisor.RFC}_{comprobante.DatosFiscalesCliente.RFC}_{comprobante.Serie}{comprobante.Folio}_{comprobante.FechaCreada.Value.ToShortDateString()}.xml";
                //await EmailService.SendAsync(correo, PDF, XML, pdfName, xmlName);

                string correos = await ClienteService.getCorreo(user.Cliente.ClienteID);
                await EmailService.SendAsync(correo, PDF, XML, correos, pdfName, xmlName);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error al enviar pdf y xml por correo");
            }
        }
        private Task<Usuario> GetCurrentUserAsync()
        {
            return Task.FromResult<Usuario>(RepositorioUsuarios.GetByID(User.Identity.GetUserId<int>()));
        }
    }
}
