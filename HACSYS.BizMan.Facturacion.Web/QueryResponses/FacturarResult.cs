﻿using HACSYS.BizMan.Facturacion.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Results
{
    public enum FacturarResultCode
    {
        SUCCESS, ERROR, INVALID_DATA, SERVER_ERROR, SAT_ERROR, PAC_ERROR
    };
    public class FacturarResult
    {
        public FacturarResultCode ResultCode { get; set; }
        public int FacturaID { get; set; }
        public Comprobante Factura { get; set; }
        public string Message { get; set; }
    }
}
