﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IParser
    {
        string GeneraXML(cfdv32.Comprobante comprobante);
        string CadenaOriginalv3(string RutaArchivoXML);
        string GenerarSello(string CadenaOriginal, X509Certificate2 certificate);
        string GetCadenaOriginal(string XML);
    }
}
