﻿using HACSYS.Management.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IEmpresasService
    {
        Task<Empresa> GetCurrent();
    }
}