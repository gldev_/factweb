﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IDateProvider
    {
        DateTime GetDateWithDelta();
        DateTime GetDateWithDelta(TimeSpan delta);
    }
}
