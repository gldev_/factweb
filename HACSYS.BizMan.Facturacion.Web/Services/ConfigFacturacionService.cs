﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.BizMan.Facturacion.DAL.Repositories;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    class ConfigFacturacionService : IConfigFacturacionService
    {

        IConfigFacturacionRepository Config;

        public ConfigFacturacionService(IConfigFacturacionRepository config) {
            Config = config;
        }

        public ConfigFacturacion getConfig(int id)
        {
            return Config.Get(c => c.EmpresaID == id).FirstOrDefault();
        }
    }
}
