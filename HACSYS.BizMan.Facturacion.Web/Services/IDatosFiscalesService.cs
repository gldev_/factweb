﻿using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.BizMan.Facturacion.Web.Models;
using HACSYS.BizMan.Facturacion.Web.Results;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.Management.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IDatosFiscalesService
    {

        Task<DatosFiscales> changeDatos(Direccion direccion, int id);
        Task<DatosFiscales> AgregarCorreo(string correo, int id);
    }
}
