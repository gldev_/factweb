﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.Facturacion.Web.Results;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IVentaService
    {
        Task<VentaResponse> Get(int estacion, string folio, double monto);
        Task<VentaResponse> GetbyID(int ID);
        void Save(List<Venta> items, int numEstacion);
        void Remove(List<Venta> items);
        Task<VentaResponse> Deactivate(int id);
        Task<VentaResponse> Activate(int id);
        List<Venta> ConvertToLocalValues(List<Venta> items);
        Task<VentaResponse> ActualizaReferencia(Venta item);

    }
}
