﻿using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.BizMan.Facturacion.Web.Models;
using HACSYS.BizMan.Facturacion.Web.Results;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.Management.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IClienteService
    {
        void Insert(Cliente cliente);
        Task<ResponseRegistration> Get(string rfc, string razonSocial);

        Task<Cliente> GetCliente(int id);
        Task<Cliente> GetClienteFromUserID(int id);
        Task<IEnumerable<Direccion>> GetDirecciones(int id);
        Cliente changeDireccion(int uid, Direccion ndireccion, Direccion vdireccion);
        Cliente changeDireccion(Cliente cliente);

        Direccion getDireccion(int direccionID);

        Cliente getByEmail(string email);
        //Cliente UpdateClientDetails(Direccion entity, int id);
        Direccion UpdateClientDetails(Direccion entity, int id);
        Task<UserToken> UToken(string token);
        Task<String> getCorreo(int id);
        Task addMail(int id, string mail, string umail);

    }
}
