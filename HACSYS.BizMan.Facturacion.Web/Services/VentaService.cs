﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HACSYS.BizMan.Facturacion.Web.Results;
using HACSYS.BizMan.DAL.Repositories;
using HACSYS.BizMan.DAL.Models;
using HACSYS.Management.DAL.Repositories;

using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.BizMan.ARES.DAL.Repositories;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class VentaService : IVentaService
    {
        private IVentaRepository RepositorioVentas;
        private IImpuestoRepository RepositorioImpuestos;
        private IProductoRepository RepositorioProductos;
        private IDatosPagoService ServicioDatosPago;

        public VentaService(IVentaRepository repositorioVentas, IImpuestoRepository repositorioImpuestos, IProductoRepository repositorioProductos, IDatosPagoService servicioDatosPago)
        {
            RepositorioVentas = repositorioVentas;
            RepositorioProductos = repositorioProductos;
            RepositorioImpuestos = repositorioImpuestos;
            ServicioDatosPago = servicioDatosPago;
        }
        public async Task<VentaResponse> Get(int estacion, string folio, double monto)
        {
            // Buscar localmente la venta para poder realizar el Comprobante
            var resultado = await RepositorioVentas.GetAsync(q => q.Folio.Equals(folio));
            //resultado = resultado.Where(v => v.Sucursal.SucursalID == estacion).ToList();

            // Aquí verificamos que el resultado no sea nulo, en general si no lo encuentra por default regresaría nulo
            // pero para este caso el proceso como quiera sigue después del if por que regresa un objeto vacío antes de 
            // terminar el runtime de la función
            if (resultado != null)
            {
                // Aquí verificamos que aun que el objeto venga con algo, necesite tener datos para poder trabajar con él
                if (resultado.Count() == 0)
                {
                    // Si no tiene información de despacho regresa el despacho nulo y el código señalando que no fueron
                    // Encontrados resutlados con el query
                    return new VentaResponse() { Status = VentaStatus.NotFound, Venta = null };
                }
                else
                {
                    // En caso de que se haya encontrado PERO no coincidan las cantidades de monto en despacho y
                    if (resultado.FirstOrDefault().Monto != monto) { return new VentaResponse() { Status = VentaStatus.DataMissMatch, Venta = resultado.FirstOrDefault() }; }
                    else
                    {
                        // Si todos los datos están bien y no hay ningun tipo de error le regresamos un Success y el despacho encontrado
                        return new VentaResponse() { Status = VentaStatus.Success, Venta = resultado.FirstOrDefault() };
                    }
                }
            }

            return null;
        }
        
        public List<Venta> ConvertToLocalValues(List<Venta> items)
        {
            var ventas = new List<Venta>();
            foreach (Venta item in items)
            {
                item.MetodoDePago = ServicioDatosPago.GetMetodoDePago(item.MetodoDePago.Metodo);
                item.FormaDePago = ServicioDatosPago.GetFormaDePago(item.FormaDePago.Forma);
                item.CondicionDePago = ServicioDatosPago.GetCondicionDePago(item.CondicionDePago.Condicion);

                foreach (var detalle in item.Detalles)
                {
                    detalle.Producto = RepositorioProductos.Get(p => p.Concepto == detalle.Producto.Concepto && p.Clave == detalle.Producto.Clave && p.ValorUnitario == detalle.Producto.ValorUnitario).FirstOrDefault() ?? detalle.Producto;
                    for (int index = 0; index < detalle.Producto.Impuestos.Count; index++)
                    {
                        var impuesto = detalle.Producto.Impuestos.ElementAt(index);
                        var tmp = RepositorioImpuestos.Get(i => i.Tasa == impuesto.Tasa && i.NombreSAT == impuesto.NombreSAT && i.Nombre == impuesto.Nombre && i.Tipo == impuesto.Tipo).FirstOrDefault();
                        if (tmp != null)
                        {
                            detalle.Producto.Impuestos.Remove(impuesto);
                            detalle.Producto.Impuestos.Add(tmp);
                        }
                    }

                    for (int index = 0; index < detalle.ImpuestosAplicados.Count; index++)
                    {
                        var aplicado = detalle.ImpuestosAplicados.ElementAt(index);
                        var impuesto = RepositorioImpuestos.Get(i => i.Tasa == aplicado.Impuesto.Tasa && i.NombreSAT == aplicado.Impuesto.NombreSAT && i.Nombre == aplicado.Impuesto.Nombre && i.Tipo == aplicado.Impuesto.Tipo).FirstOrDefault();
                        if(impuesto != null)
                        {
                            aplicado.Impuesto = impuesto;
                        }
                    }
                }

                ventas.Add(item);
            }

            return ventas;
        }

        public void Save(List<Venta> items, int numEstacion)
        {
            foreach(Venta item in items)
            {
                RepositorioVentas.Insert(item);
            }
            RepositorioVentas.Save();
        }

        public void Remove(List<Venta> items)
        {
            foreach(Venta item in items)
            {
                RepositorioVentas.Delete(item);
            }
            RepositorioVentas.Save();
        }

        public async Task<VentaResponse> GetbyID(int id)
        {
            //var resultado = await RepositorioTickets.AsyncGet(q => q.TicketID == id);
            Venta resultado = RepositorioVentas.Get(q=> q.VentaID == id).FirstOrDefault();

            return new VentaResponse() { Status = VentaStatus.Success, Venta = resultado };
        }

        //public async Task<TicketResponse> Deactivate(int id)
        //{
        //    // Obtenemos el depsacho mediante su ID
        //    var resultado = await RepositorioTickets.AsyncGet(q => q.TicketID == id);
        //    if (resultado != null)
        //    {
        //        // Validamos que el despacho tiene información con la cual se puede trabajar
        //        if (resultado.Count() != 0)
        //        {
        //            // Desactivamos el despacho y de esa manera queda "invisible" para cualquier otro proceso
        //            resultado.FirstOrDefault().Activar = false;
        //            RepositorioTickets.Update(resultado.FirstOrDefault());

        //            // Regresamos el despacho que se desactivó, así se puede presentar la información
        //            return new TicketResponse() { Status = VentaStatus.Success, Ticket = resultado.FirstOrDefault() };

        //        }
        //        else
        //        {
        //            // Si no se encuentra se regresa el mensaje de no encontrado y el despacho vacío
        //            return new TicketResponse() { Status = VentaStatus.NotFound, Ticket = null };
        //        }
        //    }

        //    return null;
        //}

        public async Task<VentaResponse> Deactivate(int id)
        {
            // Obtenemos el depsacho mediante su ID
            //var resultado = await RepositorioTickets.AsyncGet(q => q.TicketID == id);
            Venta resultado = RepositorioVentas.Get(q => q.VentaID == id).FirstOrDefault();
            if (resultado != null)
            {
                // Validamos que el despacho tiene información con la cual se puede trabajar

                    // Desactivamos el despacho y de esa manera queda "invisible" para cualquier otro proceso
                    resultado.Activar = false;
                    RepositorioVentas.Update(resultado);

                    // Regresamos el despacho que se desactivó, así se puede presentar la información
                    return new VentaResponse() { Status = VentaStatus.Success, Venta = resultado };

            }
            else
            {
                // Si no se encuentra se regresa el mensaje de no encontrado y el despacho vacío
                return new VentaResponse() { Status = VentaStatus.NotFound, Venta = null };
            }

            return null;
        }

        public async Task<VentaResponse> Activate(int id)
        {
            // Desactivamos el despacho y de esa manera queda "invisible" para cualquier otro proceso
            //var resultado = await RepositorioTickets.AsyncGet(q => q.TicketID == id);
            Venta resultado = RepositorioVentas.Get(q => q.VentaID == id).FirstOrDefault();
            if (resultado != null)
            {
                    // Desactivamos el despacho y de esa manera queda "invisible" para cualquier otro proceso
                    resultado.Activar = true;
                    RepositorioVentas.Update(resultado);

                    // Regresamos el despacho que se activó, así se puede presentar la información
                    return new VentaResponse() { Status = VentaStatus.Success, Venta = resultado };
            }
            else
            {
                // Si no se encuentra se regresa el mensaje de no encontrado y el despacho vacío
                return new VentaResponse() { Status = VentaStatus.NotFound, Venta = null };
            }

            return null;
        }

        public async Task<VentaResponse> ActualizaReferencia(Venta item)
        {
            try {
                RepositorioVentas.Update(item);
            }catch(Exception ex)
            {
                string e = ex.ToString();
            }
            Venta venta = RepositorioVentas.GetByID(item.VentaID);

            if(venta != null)
            return new VentaResponse() { Venta = venta, Status = VentaStatus.Success };
            else
            return new VentaResponse() { Venta = venta, Status = VentaStatus.Error };
        }
    }
}
