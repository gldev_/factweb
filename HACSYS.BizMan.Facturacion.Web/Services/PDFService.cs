﻿using HACSYS.BizMan.ARES.DAL.Repositories;
using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.Management.DAL.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NLog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class PDFServiceARES : IPDFService
    {
        private IEstacionRepository RepositorioEstaciones;
        private Logger Log = LogManager.GetCurrentClassLogger();
        public PDFServiceARES(IEstacionRepository repositorioEstaciones)
        {
            RepositorioEstaciones = repositorioEstaciones;
        }
        public Stream GetPDF(Comprobante comprobante)
        {
            Log.Info($"Generando PDF del Comprobante ");
            Log.Info($"Generando PDF del Comprobante {comprobante}");
            try
            {
                
                Log.Info($"Generando PDF del Comprobante #{comprobante.ComprobanteID}");
                Log.Info($"Generando PDF del Comprobante #{comprobante.Empresa}");
                Log.Info($"Generando PDF del Comprobante #{comprobante.Empresa.DatosFiscales.RFC}");
                Log.Info($"Generando PDF del Comprobante #{comprobante.Cliente.DatosFiscales.RFC}");
                Log.Info($"Generando PDF del Comprobante #{comprobante.Folio}");
                Log.Info($"Generando PDF del Comprobante #{comprobante.FechaCreada}");
                Log.Info($"Revisando el status");
                Log.Info($"Generando PDF del Comprobante #{comprobante.Status.Status}");
                Log.Info($"Generando bandas...");
                clsBandasFacturaOXXO bandas = new clsBandasFacturaOXXO(comprobante.Empresa, 
                    comprobante.LeyendaDocumentoImpreso, 
                    comprobante.Empresa.DatosFiscales.RFC + "_" +
                    comprobante.Cliente.DatosFiscales.RFC + "_" + 
                    ((comprobante.Serie == "-") ? "" : comprobante.Serie) + 
                    comprobante.Folio + "_" + 
                    comprobante.FechaCreada.Value.Day.ToString("0#") + comprobante.FechaCreada.Value.Month.ToString("0#") + comprobante.FechaCreada.Value.Year.ToString().Substring(2, 2), false);
                Log.Info($"bandas... {bandas}");

                if (comprobante.Status.Status == "Cancelada") bandas.Cancelado(); //else bandas.Demo();


                iTextSharp.text.pdf.PdfPTable parTabla = bandas.TablaDetalle();

                string strFormaDePago = "";
                Log.Info($"Revisando la forma de pago {comprobante.FormaDePago}");
                Log.Info($"Revisando la forma de pago {comprobante.FormaDePago.Forma}");
                Log.Info($"Revisando la forma de pago {comprobante.Parcialidad}");
                if (comprobante.FormaDePago.Forma == "Parcialidades") strFormaDePago = comprobante.FormaDePago.Forma + " " + comprobante.Parcialidad; else strFormaDePago = comprobante.FormaDePago.Forma;
                Log.Info($"Obtenemos la estacion {comprobante.EmpresaID}");
                Log.Info($"Repositorio {RepositorioEstaciones}");
                var estacion = RepositorioEstaciones.Get(e => e.EmpresaID == comprobante.EmpresaID).FirstOrDefault();
                Log.Info($"Ahora el numero de esa estacion {estacion}");
                Log.Info($"Ahora el numero de esa estacion {estacion.NumEstacion}");
                string numEstacion = $"E{estacion.NumEstacion.ToString("D5")}";
                string lugarexpedicion = "@";

                Log.Info("Generando Encabezado");
                bandas.Encabezado(comprobante.Cliente, comprobante.DatosFiscalesCliente.Direccion, comprobante, false, numEstacion, lugarexpedicion);
               
               
                Log.Info("Agregando Items");
                //var items = from item in comprobante.Items
                //            group item by new { item.Concepto, item.ValorUnitario, item.Unidad }
                //            into grp
                //            select new ComprobanteItem { Concepto = grp.Key.Concepto, Unidad = grp.Key.Unidad, ValorUnitario = grp.Key.ValorUnitario, Importe = grp.Sum(c=>c.Importe), Cantidad = grp.Sum(c=>c.Cantidad) };
                
                var items = from item in comprobante.Items
                            group item by new { item.Concepto, item.ValorUnitario, item.Unidad, item.ImpuestosAplicados, item.Producto }
                            into grp
                            select new ComprobanteItem { Concepto = grp.Key.Concepto, Unidad = grp.Key.Unidad, ValorUnitario = grp.Key.ValorUnitario, Importe = grp.Sum(c => c.Importe), Cantidad = grp.Sum(c => c.Cantidad), ImpuestosAplicados = grp.Key.ImpuestosAplicados, Producto = grp.Key.Producto };

                ComprobanteItem itemAux = new ComprobanteItem();
                List<string> val = new List<string>();
                
                foreach (var item in items)
                {
                    var iva = item.ImpuestosAplicados.FirstOrDefault(impuesto => impuesto.Impuesto.NombreSAT == "IVA");
                    var ieps = item.ImpuestosAplicados.FirstOrDefault(impuesto => impuesto.Impuesto.NombreSAT == "IEPS");
                    item.ValorUnitario = ((item.ValorUnitario - (ieps.Impuesto.Tasa / 10000)) / ((iva.Impuesto.Tasa + 100) / 100)) + (ieps.Impuesto.Tasa / 10000);
                    //item.Importe = item.ValorUnitario * item.Cantidad;
                    double cantidad = items.Where(q => q.Concepto.Equals(item.Concepto)).Sum(q => q.Cantidad);

                    item.Importe = item.ValorUnitario * cantidad;
                    item.Cantidad = cantidad;

                    if (!val.Contains(item.Concepto))
                    {
                        if (comprobante.FormaDePago.Forma == "Parcialidades Sin Concepto") bandas.DetalleParcialidad(item, parTabla); else bandas.Detalle(item, item.Unidad, parTabla);
                        val.Add(item.Concepto);
                    }
                    
                        
                    
                }
                


                //foreach (var item in items)
                //{
                //    var iva = item.ImpuestosAplicados.FirstOrDefault(impuesto => impuesto.Impuesto.NombreSAT == "IVA");
                //    var ieps = item.ImpuestosAplicados.FirstOrDefault(impuesto => impuesto.Impuesto.NombreSAT == "IEPS");
                //    item.ValorUnitario = ((item.ValorUnitario - (ieps.Impuesto.Tasa / 10000)) / ((iva.Impuesto.Tasa + 100) / 100)) + (ieps.Impuesto.Tasa / 10000);
                //    item.Importe = item.ValorUnitario * item.Cantidad;


                //    if (comprobante.FormaDePago.Forma == "Parcialidades Sin Concepto") bandas.DetalleParcialidad(item, parTabla); else bandas.Detalle(item, item.Unidad, parTabla);
                //}

                //var itemsAgrupados = from item in comprobante.Items
                //                     group item by new { item.Concepto, item.ValorUnitario, item.Unidad, item.ImpuestosAplicados } into grupo
                //                     select new ComprobanteItem()
                //                     {
                //                         Cantidad = grupo.Sum(i => i.Cantidad),
                //                         Concepto = grupo.Key.Concepto,
                //                         ValorUnitario = grupo.Key.ValorUnitario,
                //                         Unidad = grupo.Key.Unidad,
                //                         Importe = grupo.Sum(i => i.Importe),
                //                         ImpuestosAplicados = grupo.Key.ImpuestosAplicados
                //                     };



                int totalDetalle = 5;

                for (int i = 0; i < totalDetalle - items.Count(); i++)
                {
                    bandas.DetalleVacio(parTabla);
                }
                var clavePemex = $"{estacion.ClavePemex} ({numEstacion})";

                Log.Info("Agrgando detalle del comprobante");
                bandas.DetalleOperacion(comprobante.MetodoDePago.Metodo, comprobante.ReferenciaDePago, clavePemex, comprobante.Notas, Convert.ToDecimal(comprobante.Total), parTabla);
                bandas.Totales(comprobante, strFormaDePago, parTabla);
                bandas.imprimeTabla(parTabla);

                var folios = String.Join(",", comprobante.Ventas.Select(v => v.Folio).ToList());
                if (folios != "") bandas.Tabla("Folios de comprobantes de venta", folios);

                Log.Info("Agregando tabla de timbres");
                bandas.TablaTimbre(comprobante.DatosFiscalesEmisor.RFC, comprobante.DatosFiscalesCliente.RFC, comprobante.Total.ToString("#########0.000000"), comprobante);


                System.IO.MemoryStream m = new System.IO.MemoryStream();
                m = bandas.Close();

                //if (Page.Request.QueryString["Correo"] != null) Helper.EnviaFactura(configuracion, factura, cliente, m, emisor.RFC + "_" + cliente.RFC + "_" + ((factura.Serie == "-") ? "" : factura.Serie) + factura.Folio, ".");
                return m;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error creando PDF");
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return null;
        }
    }

    #region clsBandasFacturaOXXO
    public class clsBandasFacturaOXXO
    {
        private clsReportePDF reporte;
        string formatoMoneda = "$ ###,###,##0.00";
        string formatoDecimal = "###,###,##0.00";
        string formatoMoneda6 = "$ ###,###,##0.00####";
        string formatoDecimal6 = "###,###,##0.00####";
        string formatoMonedaPrecio = "$ ###,###,##0.000000";
        private Logger Log = LogManager.GetCurrentClassLogger();


        #region Constructor

        private string FormatRFC(string Simple)
        {
            Simple = Simple.Trim();

            if (Simple.Length <= 0) return "";
            if (Simple.Length == 13) return Simple.Substring(0, 4) + "-" + Simple.Substring(4, 6) + "-" + Simple.Substring(10, 3);
            if (Simple.Length == 12) return Simple.Substring(0, 3) + "-" + Simple.Substring(3, 6) + "-" + Simple.Substring(9, 3);
            if (Simple.Length < 12)
            {
                return Simple.Substring(0, Simple.Length - 6) + "-" + Simple.Substring(Simple.Length - 6, 6);
            }
            return "";
        }

        private string FormatDireccion(Direccion direccion)
        {
            return $"{direccion.Calle} {direccion.NumInterior} {direccion.Colonia} C.P. {direccion.CodigoPostal} {direccion.Municipio},{direccion.Estado.Nombre}";
        }

        public clsBandasFacturaOXXO(Empresa emisor, string leyendaoficial, string Archivo, bool flagWatermark)
        {
            reporte = new clsReportePDF();
            reporte.metadataAuthor = "HACSYS DE MEXICO S.A. DE C.V.";
            reporte.metadataCreator = "PetroFACT";
            reporte.metadataHeader("Expires", "0");
            reporte.metadataSubject = leyendaoficial;
            reporte.metadataTitulo = "CFDi " + Archivo;

            reporte.setEventos(new EventosSimple(leyendaoficial));
            if (flagWatermark) addWatermark(emisor.Logo);

            reporte.Open();
        }
        #endregion Constructor

        #region Destructor
        ~clsBandasFacturaOXXO()
        {

        }
        #endregion Destructor

        #region GetHypotenuseAngleInDegreesFrom
        public static double GetHypotenuseAngleInDegreesFrom(double opposite, double adjacent)
        {
            //http://www.regentsprep.org/Regents/Math/rtritrig/LtrigA.htm
            // Tan <angle> = opposite/adjacent
            // Math.Atan2: http://msdn.microsoft.com/en-us/library/system.math.atan2(VS.80).aspx 

            double radians = Math.Atan2(opposite, adjacent); // Get Radians for Atan2
            double angle = radians * (180 / Math.PI); // Change back to degrees
            return angle;
        }
        #endregion

        #region Close
        public System.IO.MemoryStream Close()
        {
            return reporte.Close();
        }
        #endregion Close

        #region Create
        public string Create()
        {
            return reporte.Create();
        }
        #endregion

        #region GetArchivo
        public string GetArchivo()
        {
            return reporte.GetArchivo();
        }
        #endregion

        #region addWatermark
        private void addWatermark(byte[] byteLogo)
        {
            try
            {
                reporte.DocumentAddwatermark(iTextSharp.text.Image.GetInstance(byteLogo));
            }
            catch (Exception) { }
        }
        #endregion addWatermark

        #region Cancelado
        public void Cancelado()
        {
            string strEstampado = "CANCELADA";


            // If you ask for the page size with the method getPageSize(), you always get a
            // Rectangle object without rotation (rot. 0 degrees)—in other words, the paper size
            // without orientation. That’s fine if that’s what you’re expecting; but if you reuse
            // the page, you need to know its orientation. You can ask for it separately with
            // getPageRotation(), or you can use getPageSizeWithRotation(). - (Manning Java iText Book)
            //   
            //
            iTextSharp.text.Rectangle pageSize = reporte.GetPageSize();

            //
            // Gets the content ABOVE the PDF, Another option is GetUnderContent(...)  
            // which will place the text below the PDF content. 
            //
            PdfContentByte pdfPageContents = reporte.getContentByte();
            pdfPageContents.BeginText(); // Start working with text.

            //
            // Create a font to work with 
            //
            BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, System.Text.Encoding.ASCII.EncodingName, false);
            pdfPageContents.SetFontAndSize(baseFont, 120); // 40 point font
            pdfPageContents.SetRGBColorFill(255, 0, 0); // Sets the color of the font, RED in this instance


            //
            // Angle of the text. This will give us the angle so we can angle the text diagonally 
            // from the bottom left corner to the top right corner through the use of simple trigonometry. 
            //
            float textAngle = (float)GetHypotenuseAngleInDegreesFrom(pageSize.Height, pageSize.Width);

            //
            // Note: The x,y of the Pdf Matrix is from bottom left corner. 
            // This command tells iTextSharp to write the text at a certain location with a certain angle.
            // Again, this will angle the text from bottom left corner to top right corner and it will 
            // place the text in the middle of the page. 
            //
            pdfPageContents.ShowTextAligned(PdfContentByte.ALIGN_CENTER, strEstampado,
                                            pageSize.Width / 2 + 10,
                                            pageSize.Height / 2 + 10,
                                            textAngle);

            pdfPageContents.EndText(); // Done working with text

            //Regresamos el color 
            pdfPageContents.SetRGBColorFill(0, 0, 0);


        }
        #endregion

        #region Demo
        public void Demo()
        {
            string strEstampado = "DEMOSTRACION";


            // If you ask for the page size with the method getPageSize(), you always get a
            // Rectangle object without rotation (rot. 0 degrees)—in other words, the paper size
            // without orientation. That’s fine if that’s what you’re expecting; but if you reuse
            // the page, you need to know its orientation. You can ask for it separately with
            // getPageRotation(), or you can use getPageSizeWithRotation(). - (Manning Java iText Book)
            //   
            //
            iTextSharp.text.Rectangle pageSize = reporte.GetPageSize();

            //
            // Gets the content ABOVE the PDF, Another option is GetUnderContent(...)  
            // which will place the text below the PDF content. 
            //
            PdfContentByte pdfPageContents = reporte.getContentByte();
            pdfPageContents.BeginText(); // Start working with text.

            //
            // Create a font to work with 
            //
            BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, System.Text.Encoding.ASCII.EncodingName, false);
            pdfPageContents.SetFontAndSize(baseFont, 100); // 40 point font
            pdfPageContents.SetRGBColorFill(255, 0, 0); // Sets the color of the font, RED in this instance


            //
            // Angle of the text. This will give us the angle so we can angle the text diagonally 
            // from the bottom left corner to the top right corner through the use of simple trigonometry. 
            //
            float textAngle = (float)GetHypotenuseAngleInDegreesFrom(pageSize.Height, pageSize.Width);

            //
            // Note: The x,y of the Pdf Matrix is from bottom left corner. 
            // This command tells iTextSharp to write the text at a certain location with a certain angle.
            // Again, this will angle the text from bottom left corner to top right corner and it will 
            // place the text in the middle of the page. 
            //
            pdfPageContents.ShowTextAligned(PdfContentByte.ALIGN_CENTER, strEstampado,
                                            pageSize.Width / 2 + 10,
                                            pageSize.Height / 2 + 10,
                                            textAngle);

            pdfPageContents.EndText(); // Done working with text

            //Regresamos el color 
            pdfPageContents.SetRGBColorFill(0, 0, 0);


        }
        #endregion


        #region Encabezado
        public void Encabezado(Cliente cliente, Direccion direccion, Comprobante factura, bool frontera, string estaciones, string lugarexpedicion)
        {
            //#####################################################################################################################
            //   Encabezado de Factura                                                                                                            
            //#####################################################################################################################

            iTextSharp.text.pdf.PdfPTable header = new iTextSharp.text.pdf.PdfPTable(3);
            header.KeepTogether = true;
            header.DefaultCell.BorderWidth = 1f;
            header.DefaultCell.Padding = 4;
            header.SetWidths(new float[] { 20f, 60f, 20f });
            header.WidthPercentage = 100;
            header.DefaultCell.Border = iTextSharp.text.Rectangle.BOX;
            header.DefaultCell.BorderColor = new BaseColor(Color.Black);
            header.HorizontalAlignment = Element.ALIGN_CENTER;
            header.SpacingAfter = 9.5f;
            PdfPCell celdaHDR;

            iTextSharp.text.Image imgLogo = null;
            if(factura.Empresa.Logo == null)
            {
                Log.Trace("Logo nulo");
            }
            if (factura.Empresa.Logo.Length > 0)
            {
                byte[] img = factura.Empresa.Logo;
                //imgLogo = iTextSharp.text.Image.GetInstance(factura.Empresa.Logo);
                imgLogo = iTextSharp.text.Image.GetInstance(img);

                double maxHeight = 86;
                double maxWidth = 104;


                if (imgLogo.Width > maxWidth || imgLogo.Height > maxHeight)
                {
                    float intPercent = 99;
                    while (imgLogo.ScaledHeight > maxHeight || imgLogo.ScaledWidth > maxWidth)
                    {

                        imgLogo.ScalePercent((float)intPercent--);
                    }
                }
            }

            var emisor = factura.DatosFiscalesEmisor;
            celdaHDR = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 15, iTextSharp.text.Font.BOLD)));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celdaHDR.Colspan = 3;
            header.AddCell(celdaHDR);

            Log.Trace("Agregando Logo");
            celdaHDR = new PdfPCell(imgLogo);
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celdaHDR.Rowspan = 6;
            header.AddCell(celdaHDR);

            Log.Trace("Agregando Razon Social");
            celdaHDR = new PdfPCell(new Phrase(emisor.RazonSocial, FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD)));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            header.AddCell(celdaHDR);

            celdaHDR = new PdfPCell(new Phrase("Factura", FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL, new BaseColor(Color.White))));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.BOX;
            celdaHDR.BackgroundColor = new BaseColor(Color.Black);
            celdaHDR.BorderWidth = header.DefaultCell.BorderWidth;
            header.AddCell(celdaHDR);

            Log.Trace("Agregando Direccion Emisor");
            celdaHDR = new PdfPCell(new Phrase(FormatDireccion(emisor.Direccion), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celdaHDR.Rowspan = 2;
            header.AddCell(celdaHDR);

            Log.Trace("Agregando Serie");
            celdaHDR = new PdfPCell(new Phrase(((factura.Serie != "-") ? factura.Serie : "") + factura.Folio, FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(Color.Red))));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.BOX;
            celdaHDR.PaddingBottom = 4;
            celdaHDR.BorderWidth = header.DefaultCell.BorderWidth;
            header.AddCell(celdaHDR);

            //celdaHDR = new PdfPCell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            //celdaHDR.HorizontalAlignment = Element.ALIGN_LEFT;
            //celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            //celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            //celdaHDR.PaddingLeft = 8;
            //header.AddCell(celdaHDR);

            celdaHDR = new PdfPCell(new Phrase("No. Certificado", FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL, new BaseColor(Color.White))));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.BOX;
            celdaHDR.BackgroundColor = new BaseColor(Color.Black);
            celdaHDR.BorderWidth = header.DefaultCell.BorderWidth;
            header.AddCell(celdaHDR);

            Log.Trace("Agregando RFC");
            celdaHDR = new PdfPCell(new Phrase("R.F.C. " + FormatRFC(emisor.RFC), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            header.AddCell(celdaHDR);

            Log.Trace("Agregando Certificado");
            celdaHDR = new PdfPCell(new Phrase(factura.Certificado, FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL)));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.BOX;
            celdaHDR.PaddingBottom = 4;
            celdaHDR.BorderWidth = header.DefaultCell.BorderWidth;
            header.AddCell(celdaHDR);
            if (lugarexpedicion == "@")
            {
                if (frontera)
                {



                    if (estaciones.Contains("8466") || estaciones.Contains("7214") || estaciones.Contains("8775"))
                    {
                        celdaHDR = new PdfPCell(new Phrase("Expedido en Benito Juarez, Quintana Roo", FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL)));
                    }
                    else
                    {
                        celdaHDR = new PdfPCell(new Phrase("Expedido en Nuevo Laredo, Tamaulipas", FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL)));
                    }

                    celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
                    celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
                    celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    header.AddCell(celdaHDR);
                }
                else
                {
                    celdaHDR = new PdfPCell(new Phrase("Expedido en " + emisor.Direccion.Municipio + ", " + (( string.IsNullOrWhiteSpace(emisor.Direccion.Localidad)) ? "" : emisor.Direccion.Localidad + ", ") + emisor.Direccion.Estado.Nombre, FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL)));
                    celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
                    celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
                    celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    header.AddCell(celdaHDR);
                }
            }
            else
            {
                celdaHDR = new PdfPCell(new Phrase("Expedido en " + lugarexpedicion, FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL)));
                celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
                celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
                celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
                header.AddCell(celdaHDR);
            }

            celdaHDR = new PdfPCell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            header.AddCell(celdaHDR);

            Log.Trace("Agregando Regimen Fiscal");
            celdaHDR = new PdfPCell(new Phrase("Régimen Fiscal: " + emisor.RegimenFiscal, FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL)));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            header.AddCell(celdaHDR);

            celdaHDR = new PdfPCell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celdaHDR.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaHDR.VerticalAlignment = Element.ALIGN_MIDDLE;
            celdaHDR.Border = iTextSharp.text.Rectangle.NO_BORDER;
            header.AddCell(celdaHDR);

            reporte.DocumentAdd(header);

            iTextSharp.text.pdf.PdfPTable datos = new iTextSharp.text.pdf.PdfPTable(4);
            datos.KeepTogether = true;
            datos.DefaultCell.BorderWidth = 1f;
            datos.DefaultCell.Padding = 4;
            datos.SetWidths(new float[] { 15f, 50f, 15f, 20f });
            datos.WidthPercentage = 100;
            datos.DefaultCell.Border = iTextSharp.text.Rectangle.BOX;
            datos.DefaultCell.BorderColor = new BaseColor(Color.Black);
            datos.HorizontalAlignment = Element.ALIGN_CENTER;
            datos.SpacingAfter = 9.5f;

            PdfPCell celda;

            iTextSharp.text.Font elfont = FontFactory.GetFont(FontFactory.HELVETICA, 8f);
            iTextSharp.text.Font elfontH = FontFactory.GetFont(FontFactory.HELVETICA, 8f, iTextSharp.text.Font.BOLD);


            celda = new PdfPCell(new Phrase("NOMBRE", elfontH));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.TOP_BORDER + iTextSharp.text.Rectangle.LEFT_BORDER;
            celda.PaddingLeft = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            Log.Trace("Agregando Datos Fiscales cliente");
            celda = new PdfPCell(new Phrase(factura.DatosFiscalesCliente.RazonSocial, elfont));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.TOP_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.Colspan = 3;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);


            celda = new PdfPCell(new Phrase("DIRECCION", elfontH));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
            celda.PaddingLeft = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            Log.Trace("Agregando Dirección Cliente");
            celda = new PdfPCell(new Phrase($"{direccion.Calle}, {direccion.NumExterior} {(direccion.NumInterior != null ? $"- {direccion.NumInterior}" : string.Empty)}", elfont));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase("COLONIA", elfontH));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.PaddingLeft = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase(direccion.Colonia, elfont));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase("CIUDAD", elfontH));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
            celda.PaddingLeft = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase(direccion.Municipio + (direccion.Localidad ?? ""), elfont));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase("C.P.", elfontH));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.PaddingLeft = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase(direccion.CodigoPostal, elfont));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase("ESTADO", elfontH));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
            celda.PaddingLeft = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase(direccion.Estado.Nombre, elfont));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase("R.F.C.", elfontH));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.PaddingBottom = 8;
            celda.PaddingLeft = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase(FormatRFC(cliente.DatosFiscales.RFC), elfont));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.PaddingBottom = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase("FECHA", elfontH));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.PaddingBottom = 8;
            celda.PaddingLeft = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.FechaCreada.Value.ToString("dd/MM/yyyy HH:mm:ss"), elfont));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.PaddingBottom = 8;
            celda.BorderWidth = datos.DefaultCell.BorderWidth;
            datos.AddCell(celda);

            reporte.DocumentAdd(datos);

        }
        #endregion Encabezado

        #region TablaDetalle
        public iTextSharp.text.pdf.PdfPTable TablaDetalle()
        {
            iTextSharp.text.pdf.PdfPTable parTabla = new iTextSharp.text.pdf.PdfPTable(5);
            parTabla.DefaultCell.BorderWidth = 1f;
            parTabla.KeepTogether = true;
            parTabla.DefaultCell.Padding = 2;
            parTabla.SpacingAfter = 0;
            parTabla.WidthPercentage = 100;
            parTabla.SetWidths(new float[] { 14f, 10f, 48f, 14f, 14f });
            parTabla.DefaultCell.Border = iTextSharp.text.Rectangle.BOX;

            PdfPCell celda;

            celda = new PdfPCell(new Phrase("CANTIDAD", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, new BaseColor(Color.White))));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(Color.Black);
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("UNIDAD", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, new BaseColor(Color.White))));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(Color.Black);
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("DESCRIPCION", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, new BaseColor(Color.White))));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(Color.Black);
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("PRECIO", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, new BaseColor(Color.White))));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(Color.Black);
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("IMPORTE", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, new BaseColor(Color.White))));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(Color.Black);
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            return parTabla;
        }
        #endregion #region TablaDetalle

        #region Detalle
        public void Detalle(ComprobanteItem detalle, string Unidad, iTextSharp.text.pdf.PdfPTable parTabla)
        {
            PdfPCell celda;

            celda = new PdfPCell(new Phrase(detalle.Cantidad.ToString(formatoDecimal6), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(Unidad, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(detalle.Concepto, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(detalle.ValorUnitario.ToString(formatoMonedaPrecio), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(detalle.Importe.ToString(formatoMoneda), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);


        }
        #endregion

        #region DetalleParcialidad
        public void DetalleParcialidad(ComprobanteItem detalle, iTextSharp.text.pdf.PdfPTable parTabla)
        {
            PdfPCell celda;
            celda = new PdfPCell(new Phrase(detalle.Concepto, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(detalle.Importe.ToString(formatoMoneda), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            parTabla.AddCell(celda);


        }
        #endregion

        #region DetalleVacio
        public void DetalleVacio(iTextSharp.text.pdf.PdfPTable parTabla)
        {
            PdfPCell celda;
            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);


        }
        #endregion

        #region DetalleOperacion
        public void DetalleOperacion(string MetodoDePago, string ReferenciaPago, string ClavePemex, string Notas, decimal Total, iTextSharp.text.pdf.PdfPTable parTabla)
        {
            PdfPCell celda;

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 5;
            parTabla.AddCell(celda);


            celda = new PdfPCell(new Phrase("Tipo de Operación: " + MetodoDePago + ((ReferenciaPago.Trim().Length > 1) ? ", terminación: " + ReferenciaPago : ""), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.PaddingLeft = 8;
            celda.Colspan = 5;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 5;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Clave PEMEX de las estaciones de servicio del grupo: " + ClavePemex, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.PaddingLeft = 8;
            celda.Colspan = 5;
            parTabla.AddCell(celda);

            //celda = new PdfPCell(new Phrase("Nota(s): " + Notas, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.PaddingLeft = 8;
            celda.Colspan = 5;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Cantidad con Letra: " + enletras(Total.ToString(formatoDecimal)), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.PaddingLeft = 8;
            celda.PaddingBottom = 8;
            celda.Colspan = 5;
            parTabla.AddCell(celda);


        }
        #endregion

        #region Totales
        public void Totales(Comprobante factura, string strFormaDePago, iTextSharp.text.pdf.PdfPTable parTabla)
        {

            PdfPCell celda;

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Subtotal", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.SubTotal.ToString(formatoMoneda), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            parTabla.AddCell(celda);

            Dictionary<double, double> importeIVA = new Dictionary<double, double>();
            foreach (var item in factura.Items)
            {
                var iva = item.ImpuestosAplicados.FirstOrDefault(impuesto => impuesto.Impuesto.NombreSAT == "IVA");
                var ieps = item.ImpuestosAplicados.FirstOrDefault(impuesto => impuesto.Impuesto.NombreSAT == "IEPS");
                if (!importeIVA.ContainsKey(iva.Impuesto.Tasa)) importeIVA.Add(iva.Impuesto.Tasa, 0);
                if (item.ImpuestoIncluido)
                {
                    importeIVA[iva.Impuesto.Tasa] += ((item.ValorUnitario - ieps.Impuesto.Tasa/10000) * item.Cantidad) * ((iva.Impuesto.Tasa) / 100);
                }
                else
                {
                    //TODO: Al remover el ieps el precio unitario sigue conservando el IVA, el IVA se aplica ya por litro
                    double totalIVA = (item.ValorUnitario - ieps.Impuesto.Tasa / 10000) - ((item.ValorUnitario - ieps.Impuesto.Tasa / 10000) / ((iva.Impuesto.Tasa + 100) / 100));
                    double Unitario = (item.ValorUnitario - ((ieps.Impuesto.Tasa / 10000) + totalIVA) ) * item.Cantidad;
                    //importeIVA[iva.Impuesto.Tasa] += ((item.ValorUnitario - ieps.Impuesto.Tasa/10000) * item.Cantidad) /** (iva.Impuesto.Tasa) / 100*/;
                    importeIVA[iva.Impuesto.Tasa] += ((item.ValorUnitario - ieps.Impuesto.Tasa / 10000) * item.Cantidad) - Unitario ;
                }
            }


            int i = 0;
            foreach (var item in importeIVA)
            {
                if (i == 1)
                {
                    celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
                    celda.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celda.VerticalAlignment = Element.ALIGN_MIDDLE;
                    celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
                    celda.Colspan = 3;
                    parTabla.AddCell(celda);
                }

                celda = new PdfPCell(new Phrase("IVA (" + item.Key.ToString() + "%)", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
                celda.HorizontalAlignment = Element.ALIGN_RIGHT;
                celda.VerticalAlignment = Element.ALIGN_MIDDLE;
                celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
                celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
                parTabla.AddCell(celda);

                celda = new PdfPCell(new Phrase(item.Value.ToString(formatoMoneda), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
                celda.HorizontalAlignment = Element.ALIGN_RIGHT;
                celda.VerticalAlignment = Element.ALIGN_MIDDLE;
                celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
                celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
                parTabla.AddCell(celda);
                i++;
            }


            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Total", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.Total.ToString(formatoMoneda), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(strFormaDePago, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 2;
            parTabla.AddCell(celda);


        }
        #endregion

        #region TablaTimbre
        public void TablaTimbre(string RFCEmisor, string RFCReceptor, string Total, Comprobante factura)
        {
            iTextSharp.text.pdf.PdfPTable parTabla = new iTextSharp.text.pdf.PdfPTable(4);
            parTabla.DefaultCell.BorderWidth = 0.5f;
            parTabla.KeepTogether = true;
            parTabla.DefaultCell.Padding = 2;
            parTabla.SpacingBefore = 10;
            parTabla.WidthPercentage = 100;
            parTabla.SetWidths(new float[] { 20f, 40f, 20f, 20f });
            parTabla.DefaultCell.Border = iTextSharp.text.Rectangle.BOX;

            PdfPCell celda;

            celda = new PdfPCell(new Phrase("Información del Timbre Fiscal Digital", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, new BaseColor(Color.White))));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(Color.Black);
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 4;
            parTabla.AddCell(celda);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("?re=");
            sb.Append(RFCEmisor);
            sb.Append("&rr=");
            sb.Append(RFCReceptor);
            sb.Append("&tt=");
            sb.Append(Total);

            System.Collections.Generic.Dictionary<iTextSharp.text.pdf.qrcode.EncodeHintType, object> dic = new System.Collections.Generic.Dictionary<iTextSharp.text.pdf.qrcode.EncodeHintType, object>();
            dic.Add(iTextSharp.text.pdf.qrcode.EncodeHintType.ERROR_CORRECTION, iTextSharp.text.pdf.qrcode.ErrorCorrectionLevel.H);
            BarcodeQRCode qr = new BarcodeQRCode(sb.ToString(), 256, 256, dic);

            iTextSharp.text.Image image = qr.GetImage();

            image.ScaleAbsolute(104, 104);

            celda = new PdfPCell(image);
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Rowspan = 8;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Folio Fiscal", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth + 0.5f;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Certificado Digital SAT", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth + 0.5f;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Fecha de Certificación", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth + 0.5f;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.FolioSAT, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.Mensaje, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.FechaCreada.Value.ToString("dd/MM/yyyy HH:mm:ss"), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Cadena Original del Timbre", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.PaddingLeft = 5;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.CadenaOriginal, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            celda.PaddingBottom = 5;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Sello Digital del Emisor", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.PaddingLeft = 5;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.SelloDigital, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            celda.PaddingBottom = 5;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase("Sello Digital del SAT", FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.PaddingLeft = 5;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(factura.SelloDigitalPAC, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.Border = iTextSharp.text.Rectangle.BOX;
            celda.BorderWidth = parTabla.DefaultCell.BorderWidth;
            celda.Colspan = 3;
            celda.PaddingBottom = 5;
            parTabla.AddCell(celda);

            reporte.DocumentAdd(parTabla);
        }
        #endregion #region TablaTimbre

        #region imprimeTabla
        public void imprimeTabla(iTextSharp.text.pdf.PdfPTable parTabla)
        {
            reporte.DocumentAdd(parTabla);
        }
        #endregion imprimeTabla

        #region Tabla
        public void Tabla(string Titulo, string Valor)
        {
            iTextSharp.text.pdf.PdfPTable parTabla = new iTextSharp.text.pdf.PdfPTable(1);
            parTabla.DefaultCell.Padding = 0.2f;
            parTabla.SpacingAfter = 1;
            parTabla.WidthPercentage = 100;
            parTabla.DefaultCell.BorderWidth = 0.5f;
            parTabla.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            PdfPCell celda;
            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 2, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;

            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(Titulo, FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(Color.White))));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.TOP_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(Color.Black);
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(Valor, FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            parTabla.AddCell(celda);

            reporte.DocumentAdd(parTabla);
        }
        #endregion

        #region TablaBig
        public void TablaBig(string Titulo, string Valor)
        {
            iTextSharp.text.pdf.PdfPTable parTabla = new iTextSharp.text.pdf.PdfPTable(1);
            parTabla.DefaultCell.Padding = 0.2f;
            parTabla.SpacingAfter = 1;
            parTabla.WidthPercentage = 100;
            parTabla.DefaultCell.BorderWidth = 0.5f;
            parTabla.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            PdfPCell celda;
            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 2, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(Titulo, FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.TOP_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(0xDE, 0xDE, 0xDE, 255);
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(Valor, FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL)));
            celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            parTabla.AddCell(celda);

            reporte.DocumentAdd(parTabla);
        }
        #endregion

        #region TablaSmall
        public void TablaSmall(string Titulo, string Valor, int Pos)
        {
            iTextSharp.text.pdf.PdfPTable parTabla = new iTextSharp.text.pdf.PdfPTable(1);
            parTabla.DefaultCell.Padding = 0.2f;
            parTabla.SpacingAfter = 1;
            //parTabla.WidthPercentage = 87;
            parTabla.DefaultCell.BorderWidth = 0.5f;
            parTabla.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            parTabla.HorizontalAlignment = Element.ALIGN_RIGHT;
            parTabla.SetTotalWidth(new float[] { 508f });

            PdfPCell celda;
            celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 2, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.NO_BORDER;
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(Titulo, FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.TOP_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            celda.BackgroundColor = new iTextSharp.text.BaseColor(0xDE, 0xDE, 0xDE, 255);
            parTabla.AddCell(celda);

            celda = new PdfPCell(new Phrase(Valor, FontFactory.GetFont(FontFactory.HELVETICA, 4.7f, iTextSharp.text.Font.BOLD)));
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.Border = iTextSharp.text.Rectangle.LEFT_BORDER + iTextSharp.text.Rectangle.BOTTOM_BORDER + iTextSharp.text.Rectangle.RIGHT_BORDER;
            parTabla.AddCell(celda);


            PdfContentByte cb = reporte.getContentByte();
            parTabla.WriteSelectedRows(0, -1, 92, Pos, cb);
            //reporte.DocumentAdd(parTabla);



        }
        #endregion

        #region QRCode
        public void QRCode(string RFCEmisor, string RFCReceptor, string Total, string UUID)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("?re=");
            sb.Append(RFCEmisor);
            sb.Append("&rr=");
            sb.Append(RFCReceptor);
            sb.Append("&tt=");
            sb.Append(Total);
            sb.Append("&id=");
            sb.Append(UUID);

            PdfContentByte cb = reporte.getContentByte();



            System.Collections.Generic.Dictionary<iTextSharp.text.pdf.qrcode.EncodeHintType, object> dic = new System.Collections.Generic.Dictionary<iTextSharp.text.pdf.qrcode.EncodeHintType, object>();
            dic.Add(iTextSharp.text.pdf.qrcode.EncodeHintType.ERROR_CORRECTION, iTextSharp.text.pdf.qrcode.ErrorCorrectionLevel.H);
            BarcodeQRCode qr = new BarcodeQRCode(sb.ToString(), 256, 256, dic);

            iTextSharp.text.Image image = qr.GetImage();
            //image.ScalePercent(22);
            //cb.AddImage(image, image.ScaledWidth,0, 0, image.ScaledHeight, 540, 732);

            image.ScaleAbsolute(104, 104);
            cb.AddImage(image, image.ScaledWidth, 0, 0, image.ScaledHeight, 502f, 622.9f);


            //cb.AddImage(image, image.ScaledWidth, 0, 0, image.ScaledHeight, 544, 26);


        }

        public static string enletras(string num)
        {

            string res, dec = " PESOS";

            Int64 entero;

            int decimales;

            double nro;

            try
            {

                nro = Convert.ToDouble(num);

            }

            catch
            {

                return "";

            }

            entero = Convert.ToInt64(Math.Truncate(nro));

            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));

            //if (decimales > 0)
            //{

            dec = " PESOS " + decimales.ToString("0#") + "/100";

            //}

            res = enletras(Convert.ToDouble(entero)) + dec + " M.N.";

            return res;

        }

        private static string enletras(double value)
        {

            string Num2Text = "";

            value = Math.Truncate(value);

            if (value == 0) Num2Text = "CERO";

            else if (value == 1) Num2Text = "UN";

            else if (value == 2) Num2Text = "DOS";

            else if (value == 3) Num2Text = "TRES";

            else if (value == 4) Num2Text = "CUATRO";

            else if (value == 5) Num2Text = "CINCO";

            else if (value == 6) Num2Text = "SEIS";

            else if (value == 7) Num2Text = "SIETE";

            else if (value == 8) Num2Text = "OCHO";

            else if (value == 9) Num2Text = "NUEVE";

            else if (value == 10) Num2Text = "DIEZ";

            else if (value == 11) Num2Text = "ONCE";

            else if (value == 12) Num2Text = "DOCE";

            else if (value == 13) Num2Text = "TRECE";

            else if (value == 14) Num2Text = "CATORCE";

            else if (value == 15) Num2Text = "QUINCE";

            else if (value < 20) Num2Text = "DIECI" + enletras(value - 10);

            else if (value == 20) Num2Text = "VEINTE";

            else if (value < 30) Num2Text = "VEINTI" + enletras(value - 20);

            else if (value == 30) Num2Text = "TREINTA";

            else if (value == 40) Num2Text = "CUARENTA";

            else if (value == 50) Num2Text = "CINCUENTA";

            else if (value == 60) Num2Text = "SESENTA";

            else if (value == 70) Num2Text = "SETENTA";

            else if (value == 80) Num2Text = "OCHENTA";

            else if (value == 90) Num2Text = "NOVENTA";

            else if (value < 100) Num2Text = enletras(Math.Truncate(value / 10) * 10) + " Y " + enletras(value % 10);

            else if (value == 100) Num2Text = "CIEN";

            else if (value < 200) Num2Text = "CIENTO " + enletras(value - 100);

            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = enletras(Math.Truncate(value / 100)) + "CIENTOS";

            else if (value == 500) Num2Text = "QUINIENTOS";

            else if (value == 700) Num2Text = "SETECIENTOS";

            else if (value == 900) Num2Text = "NOVECIENTOS";

            else if (value < 1000) Num2Text = enletras(Math.Truncate(value / 100) * 100) + " " + enletras(value % 100);

            else if (value == 1000) Num2Text = "MIL";

            else if (value < 2000) Num2Text = "MIL " + enletras(value % 1000);

            else if (value < 1000000)
            {

                Num2Text = enletras(Math.Truncate(value / 1000)) + " MIL";

                if ((value % 1000) > 0) Num2Text = Num2Text + " " + enletras(value % 1000);

            }

            else if (value == 1000000) Num2Text = "UN MILLON";

            else if (value < 2000000) Num2Text = "UN MILLON " + enletras(value % 1000000);

            else if (value < 1000000000000)
            {

                Num2Text = enletras(Math.Truncate(value / 1000000)) + " MILLONES ";

                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + enletras(value - Math.Truncate(value / 1000000) * 1000000);

            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";

            else if (value < 2000000000000) Num2Text = "UN BILLON " + enletras(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            else
            {

                Num2Text = enletras(Math.Truncate(value / 1000000000000)) + " BILLONES";

                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + enletras(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            }

            return Num2Text;

        }
        #endregion

    }



    public class clsReportePDF
    {
        private MemoryStream strmArchivo;
        private Document document = null;
        private PdfWriter writer;
        private string strArchivo;

        private string P_strmetadataTitulo;
        private string P_strmetadataSubject;
        private string P_strmetadataCreator;
        private string P_strmetadataAuthor;

        #region Constructor
        public clsReportePDF()
        {
            //document = new Document(PageSize.LETTER, 20, 20, 70, 20);
            document = new Document(PageSize.LETTER, 20, 20, 20, 20);
            try
            {
                strmArchivo = new MemoryStream();

                writer = PdfWriter.GetInstance(document, strmArchivo);
                writer.CloseStream = false;
            }
            catch (System.IO.IOException ioe)
            {
                System.Console.WriteLine(ioe.Message);

            }
        }
        #endregion Constructor

        #region metadataTitulo
        public string metadataTitulo
        {
            get
            {
                return P_strmetadataTitulo;
            }
            set
            {
                if (value != "")
                {
                    P_strmetadataTitulo = value;
                    document.AddTitle(P_strmetadataTitulo);
                }
            }
        }
        #endregion metadataTitulo

        #region metadataSubject
        public string metadataSubject
        {
            get
            {
                return P_strmetadataSubject;
            }
            set
            {
                if (value != "")
                {
                    P_strmetadataSubject = value;
                    document.AddSubject(P_strmetadataSubject);
                }
            }
        }
        #endregion metadataSubject

        #region metadataCreator
        public string metadataCreator
        {
            get
            {
                return P_strmetadataCreator;
            }
            set
            {
                if (value != "")
                {
                    P_strmetadataCreator = value;
                    document.AddCreator(P_strmetadataCreator);
                }
            }
        }
        #endregion metadataCreator

        #region metadataAuthor
        public string metadataAuthor
        {
            get
            {
                return P_strmetadataAuthor;
            }
            set
            {
                if (value != "")
                {
                    P_strmetadataAuthor = value;
                    document.AddAuthor(P_strmetadataAuthor);
                }
            }
        }
        #endregion metadataAuthor

        #region metadataHeader
        public void metadataHeader(string name, string content)
        {
            document.AddHeader(name, content);
        }
        #endregion metadataHeader

        #region setEventos
        public void setEventos(iTextSharp.text.pdf.PdfPageEventHelper eventos)
        {
            try
            {
                writer.PageEvent = eventos;
            }
            catch { }
        }
        #endregion setEventos

        #region Open
        public void Open()
        {
            document.Open();
        }
        #endregion Open

        #region ResetPageCount
        public void ResetPageCount()
        {
            // we reset the page numbering
            document.ResetPageCount();
        }
        #endregion ResetPageCount

        #region NewPage
        public void NewPage()
        {
            // we trigger a page break
            document.NewPage();
        }
        #endregion NewPage

        #region DocumentAdd
        public void DocumentAdd(iTextSharp.text.IElement element)
        {
            try
            {
                document.Add(element);
            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
            }
            catch (Exception E)
            {
                Console.Error.WriteLine(E.Message);
            }
        }
        #endregion DocumentAdd

        #region DocumentAddwatermark
        public void DocumentAddwatermark(iTextSharp.text.Image img)
        {
            //Imagen de Fondo
            img.Alignment = iTextSharp.text.Image.MIDDLE_ALIGN;
            //Watermark watermark = new Watermark(img, 42, 160);
            //document.Add(watermark);
        }
        #endregion DocumentAddwatermark

        #region getContentByte
        public iTextSharp.text.pdf.PdfContentByte getContentByte()
        {
            return writer.DirectContent;

        }
        #endregion getContentByte

        public iTextSharp.text.Rectangle GetPageSize()
        {
            return document.PageSize;
        }

        #region Close
        public MemoryStream Close()
        {
            document.Close();
            //document = null;
            //writer = null;
            strmArchivo.Seek(0, SeekOrigin.Begin);
            strmArchivo.Position = 0;

            return strmArchivo;
        }
        #endregion Close

        #region Create
        public string Create()
        {
            document.Close();
            document = null;
            writer = null;
            return strArchivo;
        }
        #endregion

        #region GetArchivo()
        public string GetArchivo()
        {
            return strArchivo;
        }
        #endregion

        #region pageWidth
        public float pageWidth
        {
            get
            {
                return document.PageSize.Width;
            }
        }
        #endregion pageWidth

        #region pageHeight
        public float pageHeight
        {
            get
            {
                return document.PageSize.Height;
            }
        }
        #endregion pageHeight

    }

    public class EventosSimple : iTextSharp.text.pdf.PdfPageEventHelper
    {
        // objeto contentbyte del writer
        PdfContentByte cb;

        // Contenido del total de paginas se agregara al template
        PdfTemplate template;

        // Font que se utilizara para el footer
        BaseFont bf = null;
        string LeyendaOficial = "";
        #region Constructor
        public EventosSimple(string leyendaoficial)
        {
            LeyendaOficial = leyendaoficial;
        }

        public EventosSimple() { }
        #endregion Constructor

        #region onOpenDocument
        /// sobrecarga del metodo onOpenDocument
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                //template = cb.createTemplate(50, 50);
                template = cb.CreateTemplate(document.PageSize.Width, 50);

            }
            catch (DocumentException de)
            {
                System.Console.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                System.Console.WriteLine(ioe.Message);
            }
        }
        #endregion onOpenDocument

        #region onEndPage
        // sobrecarga del metodo onEndPage
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            int pageN = writer.PageNumber;
            string text = "Hoja " + pageN + " / ";
            float len = bf.GetWidthPoint(text, 8);
            float x = ((document.Left + document.Right) / 2) - (len / 2);



            cb.BeginText();
            cb.SetFontAndSize(bf, 8);
            cb.SetTextMatrix(x, 45);
            cb.ShowText(text);
            cb.EndText();
            cb.AddTemplate(template, x + len, 45);

            cb.BeginText();
            cb.SetFontAndSize(bf, 8);
            cb.SetTextMatrix(200, 35);
            cb.ShowText(LeyendaOficial);
            cb.EndText();

        }
        #endregion onEndPage

        #region onCloseDocument
        // sobrecarga del metodo onCloseDocument 
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            template.BeginText();
            template.SetFontAndSize(bf, 8);
            int intNumPagina = (writer.PageNumber - 1);
            template.ShowText(intNumPagina.ToString());
            template.EndText();
        }
        #endregion onCloseDocument


    }
    #endregion 
}