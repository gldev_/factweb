﻿using System.IO;
using HACSYS.BizMan.Facturacion.DAL.Models;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IPDFService
    {
        Stream GetPDF(Comprobante comprobante);
    }
}