﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;
using System.Web.Hosting;
using NLog.Fluent;
using NLog;
using System.Web.Configuration;
using HACSYS.Management.DAL.Repositories;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class EmailService : IIdentityMessageService, IEmailService
    {
        private Logger Log = LogManager.GetCurrentClassLogger();
        private string FromEmail;
        private static string Group = "GNO";
        public EmailService()
        {
            FromEmail = WebConfigurationManager.AppSettings["Sender"];
            Group = WebConfigurationManager.AppSettings["Grupo"];
                
        }
        public async Task SendAsync(IdentityMessage message)
        {
            var from = new MailAddress(FromEmail, "Facturacion");
            var to = new MailAddress(message.Destination);
            string emailBody = String.Empty;
            switch(message.Subject)
            {
                case "Regenerar Contraseña":
                    emailBody = LoadTemplate("Contrasena.htm");
                    break;
                case "Confirmación de cuenta":
                    emailBody = LoadTemplate("Confirmacion.htm");
                    break;
            }
            var uri = new Uri(message.Body);
            emailBody = emailBody.Replace("{{baseURL}}", $"http://{uri.Host}/");
            emailBody = emailBody.Replace("{{link}}", message.Body);
            var correo = new MailMessage(from, to) { Subject = message.Subject, Body = emailBody, IsBodyHtml = true };
            correo.SubjectEncoding = System.Text.Encoding.UTF8;
            correo.BodyEncoding = System.Text.Encoding.UTF8;
            using (var client = new SmtpClient())
            {
                await client.SendMailAsync(correo);
            }
        }

        public async Task SendAsync(string email,Stream pdf, Stream xml, string pdfName = null, string xmlName = null)
        {

            Attachment pdfAttachment = new Attachment(pdf,pdfName ?? "FACTURA_PDF.pdf");
            ContentDisposition disposition;
            if (pdf != null)
            {
                Log.Info($"Asignamos los datos al ContentDisposition asignado al attachment");
                disposition = pdfAttachment.ContentDisposition;
                disposition.CreationDate = File.GetCreationTime("FACTURA_PDF.pdf");
                disposition.ModificationDate = File.GetLastWriteTime("FACTURA_PDF.pdf");
                disposition.ReadDate = File.GetLastAccessTime("FACTURA_PDF.pdf");
                disposition.FileName = "FACTURA_PDF.pdf";
                disposition.Size = pdf.Length;
                disposition.DispositionType = DispositionTypeNames.Attachment;
            }

            Attachment xmlAttachment = new Attachment(xml,xmlName ?? "FACTURA_XML.xml");
            ContentDisposition dispositionXML;
            if (xml != null)
            {
                Log.Info($"Asignamos los datos al ContentDisposition asignado al attachment");
                dispositionXML = xmlAttachment.ContentDisposition;
                dispositionXML.CreationDate = File.GetCreationTime("FACTURA_XML.xml");
                dispositionXML.ModificationDate = File.GetLastWriteTime("FACTURA_XML.xml");
                dispositionXML.ReadDate = File.GetLastAccessTime("FACTURA_XML.xml");
                dispositionXML.FileName = "FACTURA_XML.xml";
                dispositionXML.Size = pdf.Length;
                dispositionXML.DispositionType = DispositionTypeNames.Attachment;
            }

            Log.Info($"Se configura el cuerpo del correo");
            var from = new MailAddress(FromEmail, "Facturacion");
            var to = new MailAddress(email);
            var url = $"http://{HttpContext.Current.Request.Url.Host}/";
            var emailBody = LoadTemplate("Facturacion.htm");
            emailBody = emailBody.Replace("{{baseURL}}",url);
            var correo = new MailMessage(from, to) { Subject = "Facturación Electrónica", Body = emailBody, IsBodyHtml = true };
            correo.SubjectEncoding = System.Text.Encoding.UTF8;
            correo.BodyEncoding = System.Text.Encoding.UTF8;
            correo.Attachments.Add(pdfAttachment);
            correo.Attachments.Add(xmlAttachment);
            try
            {
                Log.Info($"Se envia el correo con un disposable context");
                using (var client = new SmtpClient())
                {
                    await client.SendMailAsync(correo);
                }
            }catch(Exception e)
            {
                Log.Error(e, "Error al enviar correo con comprobante");
            }
        }

        private string LoadTemplate(string fileName)
        {
            String emailBody = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplates/"+fileName)))
            {
                emailBody = reader.ReadToEnd();
            }
            return emailBody;
        }

        public async Task SendAsync(int estacion, string gpo = "GND")
        {
            gpo = Group;

            Log.Info($"Se configura el cuerpo del correo para soporte");
            var from = new MailAddress(FromEmail, "Facturacion");
            var to = new MailAddress("soporte@hacsys.com");
            var emailBody = "Estacion " + estacion + " desconectada";
            var correo = new MailMessage(from, to) { Subject = $"BizMan ARES FactCtes ErrCon - {gpo}: [E{estacion}]", Body = emailBody, IsBodyHtml = true };
            correo.SubjectEncoding = System.Text.Encoding.UTF8;
            correo.BodyEncoding = System.Text.Encoding.UTF8;
            try
            {
                Log.Info($"Se envia el correo con un disposable context a soporte");
                using (var client = new SmtpClient())
                {
                    await client.SendMailAsync(correo);
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "Error al enviar correo con el mensaje a soporte");
            }
        }


        public async Task SendAsync(string email, Stream pdf, Stream xml, string correos, string pdfName = null, string xmlName = null)
        {

            if (correos == null || correos == "")
            {
                Attachment pdfAttachment = new Attachment(pdf, pdfName ?? "FACTURA_PDF.pdf");
                ContentDisposition disposition;
                if (pdf != null)
                {
                    Log.Info($"Asignamos los datos al ContentDisposition asignado al attachment");
                    disposition = pdfAttachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime("FACTURA_PDF.pdf");
                    disposition.ModificationDate = File.GetLastWriteTime("FACTURA_PDF.pdf");
                    disposition.ReadDate = File.GetLastAccessTime("FACTURA_PDF.pdf");
                    disposition.FileName = "FACTURA_PDF.pdf";
                    disposition.Size = pdf.Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                }

                Attachment xmlAttachment = new Attachment(xml, xmlName ?? "FACTURA_XML.xml");
                ContentDisposition dispositionXML;
                if (xml != null)
                {
                    Log.Info($"Asignamos los datos al ContentDisposition asignado al attachment");
                    dispositionXML = xmlAttachment.ContentDisposition;
                    dispositionXML.CreationDate = File.GetCreationTime("FACTURA_XML.xml");
                    dispositionXML.ModificationDate = File.GetLastWriteTime("FACTURA_XML.xml");
                    dispositionXML.ReadDate = File.GetLastAccessTime("FACTURA_XML.xml");
                    dispositionXML.FileName = "FACTURA_XML.xml";
                    dispositionXML.Size = pdf.Length;
                    dispositionXML.DispositionType = DispositionTypeNames.Attachment;
                }

                Log.Info($"Se configura el cuerpo del correo");
                var from = new MailAddress(FromEmail, "Facturacion");
                var to = new MailAddress(email);
                var url = $"http://{HttpContext.Current.Request.Url.Host}/";
                var emailBody = LoadTemplate("Facturacion.htm");
                emailBody = emailBody.Replace("{{baseURL}}", url);
                var correo = new MailMessage(from, to) { Subject = "Facturación Electrónica", Body = emailBody, IsBodyHtml = true };
                correo.SubjectEncoding = System.Text.Encoding.UTF8;
                correo.BodyEncoding = System.Text.Encoding.UTF8;
                correo.Attachments.Add(pdfAttachment);
                correo.Attachments.Add(xmlAttachment);
                try
                {
                    Log.Info($"Se envia el correo con un disposable context");
                    using (var client = new SmtpClient())
                    {
                        await client.SendMailAsync(correo);
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e, "Error al enviar correo con comprobante");
                }
            }
            else
            {

                Array listaEmail = correos.Split(',');

                Attachment pdfAttachment = new Attachment(pdf, pdfName ?? "FACTURA_PDF.pdf");
                ContentDisposition disposition;
                if (pdf != null)
                {
                    Log.Info($"Asignamos los datos al ContentDisposition asignado al attachment");
                    disposition = pdfAttachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime("FACTURA_PDF.pdf");
                    disposition.ModificationDate = File.GetLastWriteTime("FACTURA_PDF.pdf");
                    disposition.ReadDate = File.GetLastAccessTime("FACTURA_PDF.pdf");
                    disposition.FileName = "FACTURA_PDF.pdf";
                    disposition.Size = pdf.Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                }

                Attachment xmlAttachment = new Attachment(xml, xmlName ?? "FACTURA_XML.xml");
                ContentDisposition dispositionXML;
                if (xml != null)
                {
                    Log.Info($"Asignamos los datos al ContentDisposition asignado al attachment");
                    dispositionXML = xmlAttachment.ContentDisposition;
                    dispositionXML.CreationDate = File.GetCreationTime("FACTURA_XML.xml");
                    dispositionXML.ModificationDate = File.GetLastWriteTime("FACTURA_XML.xml");
                    dispositionXML.ReadDate = File.GetLastAccessTime("FACTURA_XML.xml");
                    dispositionXML.FileName = "FACTURA_XML.xml";
                    dispositionXML.Size = pdf.Length;
                    dispositionXML.DispositionType = DispositionTypeNames.Attachment;
                }

                Log.Info($"Se configura el cuerpo del correo");
                var from = new MailAddress(FromEmail, "Facturacion");
                var to = new MailAddress(email);

                var url = $"http://{HttpContext.Current.Request.Url.Host}/";
                var emailBody = LoadTemplate("Facturacion.htm");
                emailBody = emailBody.Replace("{{baseURL}}", url);
                var correo = new MailMessage(from, to) { Subject = "Facturación Electrónica", Body = emailBody, IsBodyHtml = true };

                //TODO: Verificar que los agregue

                foreach (string value in listaEmail)
                {
                    if (value != null || value != "")
                        correo.To.Add(value);
                }

                correo.SubjectEncoding = System.Text.Encoding.UTF8;
                correo.BodyEncoding = System.Text.Encoding.UTF8;
                correo.Attachments.Add(pdfAttachment);
                correo.Attachments.Add(xmlAttachment);
                try
                {
                    Log.Info($"Se envia el correo con un disposable context");
                    using (var client = new SmtpClient())
                    {
                        await client.SendMailAsync(correo);
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e, "Error al enviar correo con comprobante");
                }

            }
        }
    }
}