﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.DAL.Repositories;
using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.BizMan.Facturacion.DAL.Repositories;
using HACSYS.BizMan.Facturacion.Web.Resources;
using HACSYS.BizMan.Facturacion.Web.Results;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.Management.DAL.Models;
using HACSYS.Management.DAL.Repositories;
using HACSYS.Tools.Security.Certificates;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Xml;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class FacturacionService : IFacturacionService
    {
        private IVentaService VentaService;
        private int emisorEmpresaID;
        private IComprobanteRepository RepositorioComprobantes;
        private IClienteRepository RepositorioClientes;
        //private IEmpresasService EmpresasService;
        private IEmpresaRepository RepositorioEmpresas;
        //private IImpuestoService ImpuestoService;
        //private IProductoService ProductoService;
        private ICertificateProvider<X509Certificate> CertificateProvider;
        private IParser Parser;
        private IDateProvider DateProvider;
        private IPDFService PDFService;
        private IConfigFacturacionRepository ConfigRepository;

        private ICondicionDePagoRepository CondicionDePagoRepository;
        private IMetodoDePagoRepository MetodoDePagoRepository;
        private IFormaDePagoRepository FormaDePagoRepository;

        private ConfigFacturacion Config;
        private Logger Log = LogManager.GetLogger("FacturacionService");
        private EmailService ServicioEmail;
        private bool Test = false;
        string LogHistory;

        //TODO: Revisar si podemos cambiar las strings a enums cuando esté la base de datos
        public FacturacionService(
            IFormaDePagoRepository repositorioFormaDePagorepository,
            IMetodoDePagoRepository repositorioMetodoDePagorepository,
            ICondicionDePagoRepository repositorioCondicionDepago,
            IComprobanteRepository repositorioComprobantes,
            IEmpresaRepository repositorioEmpresas, 
            IVentaService ventaService,
            IClienteRepository repositorioClientes,
            //IImpuestoService impuestoService,
            //IProductoService productoService,
            ICertificateProvider<X509Certificate> certificateProdiver,
            IParser parser,
            IDateProvider dateProvider,
            IPDFService pdfService,
            IConfigFacturacionRepository configRepository)
        {

            FormaDePagoRepository = repositorioFormaDePagorepository;
            MetodoDePagoRepository = repositorioMetodoDePagorepository;
            CondicionDePagoRepository = repositorioCondicionDepago;

            RepositorioComprobantes = repositorioComprobantes;
            RepositorioEmpresas = repositorioEmpresas;
            VentaService = ventaService;
            RepositorioClientes = repositorioClientes;
            //ImpuestoService = impuestoService;
            //ProductoService = productoService;
            CertificateProvider = certificateProdiver;
            Parser = parser;
            DateProvider = dateProvider;
            PDFService = pdfService;
            ConfigRepository = configRepository;
            ServicioEmail = new EmailService();
            try {
                Test = Convert.ToBoolean(WebConfigurationManager.AppSettings["Test"]);
            }
            catch (Exception e) { }

        }
        //public FacturacionService(IComprobanteRepository repositorioComprobantes,
        //    IVentaService ticketService,
        //    IImpuestoService impuestoService,
        //    IProductoService productoService)
        //{
        //    RepositorioComprobantes = repositorioComprobantes;
        //    VentaService = ticketService;
        //    //ImpuestoService = impuestoService;
        //    //ProductoService = productoService;
        //    CertificateProvider = new X509CertificateProvider(new CertificateFileStore());
        //    Parser = new Parser(CertificateProvider);
        //    DateProvider = new DateProvider();
        //}
        public async Task<bool> VentaFacturada(Venta venta)
        {
            if(venta == null)
            {
                return false;
            }

            var factura = await RepositorioComprobantes.GetAsync(f => f.Ventas.Contains(venta));
            if(factura == null || factura.FirstOrDefault() == null)
            {
                return false;
            }

            return factura.FirstOrDefault().Status.Status != "Cancelada";
        }

        public Task<bool> VentaFacturada(int estacionID, string folio, double monto)
        {
            // TODO: Implementar
            return Task.FromResult<bool>(false);
            //var venta = await VentaService.Get(estacionID, folio, monto);
            //if(venta.Venta == null)
            //{
            //    return false;
            //}

            //return await VentaFacturado(venta.Venta);
        }

        private List<Venta> ValidateDate(List<Venta> ventas, ConfigFacturacion config)
        {

            int DiasDeTolerancia = config.DiasLimite;
            DateTime fechaActual = DateTime.Now;
            List<Venta> ventasAprobadas = new List<Venta>();
            //config = ConfigRepository.Get(c => c.EmpresaID == ventas.).FirstOrDefault();

            foreach (Venta venta in ventas)
            {
                if ((venta.Fecha.Month == DateTime.Now.Month) || (DiasDeTolerancia > 0 && new DateTime(DateTime.Now.Year, venta.Fecha.Month+1, DateTime.Now.Day + DiasDeTolerancia) < DateTime.Now))
                {
                    ventasAprobadas.Add(venta);
                }
            }

            return ventasAprobadas;
        }

        public async Task<FacturarResult> Facturar(int clienteID, List<Venta> ventas)
        {
            var cliente = RepositorioClientes.GetByID(clienteID);
            if (cliente == null)
                return new FacturarResult { ResultCode = FacturarResultCode.INVALID_DATA, Message = "El cliente no puede ser nulo" };
            return await Facturar(cliente, ventas);
        }

        public async Task<FacturarResult> Facturar(Cliente cliente, List<Venta> ventas)
        {
            
            Log.Info($"Creando factura a cliente {cliente.DatosFiscales.RazonSocial}, con {ventas.Count} ventas");
            LogHistory += $"Creando factura a cliente {cliente.DatosFiscales.RazonSocial}, con {ventas.Count} ventas \n";

            var empresaID = ventas.FirstOrDefault().EmpresaID;
            Config = ConfigRepository.Get(c=>c.EmpresaID==empresaID).FirstOrDefault();
            Log.Trace($"Buscando Empresa #{empresaID}");
            Empresa empresa = null;
            try
            {
                empresa = RepositorioEmpresas.GetByID(empresaID);
            }catch(Exception e)
            {
                Log.Error(e, "Empresa no encontrada");
            }
            if(empresa == null)
            {
                Log.Info($"Empresa #{empresaID} no encontrada");
                LogHistory += $"Empresa #{empresaID} no encontrada";
                return new FacturarResult() { ResultCode = FacturarResultCode.SERVER_ERROR, Message = "No se pudo encontrar dicha empresa" };
            }
                return await Facturar(empresa, cliente, ventas);
        }

        // TODO: UnitTest
        // TODO: Agregar la lógica para convertir los items en datos de facturación
        public async Task<FacturarResult> Facturar(Empresa emisor, Cliente cliente, List<Venta> ventas)
        {
            Log.Trace("Inciiando facturacion");
            LogHistory += "Iniciando Facturacion \n"; 
            if (emisor == null)
            {
                Log.Trace("Error, no hay datos del emisor");
                throw new ArgumentNullException("emisor");

            }

            if (cliente == null) {
                Log.Trace("Error, no hay datos del cliente");
                throw new ArgumentNullException("cliente");
            }

            if (ventas == null)
            {
                Log.Trace("Error, no hay datos del ticket");
                throw new ArgumentNullException("tickets");
            }
            var comprobante = new Comprobante();
            // comprobante.DireccionCliente = cliente.Direccion;
            //if(RepositorioComprobantes.Get().Count == 0)
            //{
            //    comprobante.ComprobanteID = Config.FolioInicial;
            //}
            Log.Info($"Asignando los datos de: {cliente}");
            LogHistory += $"Asignando los datos de: {cliente} \n";
            comprobante.Cliente = cliente;
            Log.Info($"Asignando los datos de: {emisor}");
            LogHistory += $"Asignando los datos de: {emisor} \n";
            comprobante.Empresa = emisor;
            comprobante.DatosFiscalesCliente = cliente.DatosFiscales;
            Log.Info($"Asignando los datos del emisor: {emisor.EmpresaID}");
            LogHistory += $"Asignando los datos del emisor: {emisor.EmpresaID} \n";
            comprobante.DatosFiscalesEmisor = emisor.DatosFiscales;
            comprobante.DireccionCliente = cliente.DatosFiscales.Direccion;
            Log.Info($"Asignando la dirección de el emisor: {emisor.EmpresaID}");
            LogHistory += $"Asignando la dirección de el emisor: {emisor.EmpresaID} \n";
            comprobante.DireccionEmisor = emisor.DatosFiscales.Direccion;
            Log.Info($"Generando el comprobante");
            LogHistory += $"Generando el comprobante";
            comprobante.Tipo = new TipoComprobante() { Tipo = TipoComprobanteEnum.FACTURA };
            Log.Info($"Asignando la fecha de creación: {DateTime.Now}");
            LogHistory += $"Asignando la fecha de creación: {DateTime.Now}";
            comprobante.FechaCreada = DateTime.Now;
            //TODO: Validar que no existan en la base de datos
            Log.Info($"Asignando la forma de pago de las ventas: {ventas.Count}");
            LogHistory += $"Asignando la forma de pago de las ventas: {ventas.Count}";
            FormaDePago formaDepago = FormaDePagoRepository.GetByID(ventas.FirstOrDefault().FormaDePago.FormaDePagoID);
            if (formaDepago != null)
            {
                comprobante.FormaDePago = formaDepago;
            }
            else
                comprobante.FormaDePago = ventas.FirstOrDefault().FormaDePago;

            Log.Info($"Asignando el método de pago: {ventas.Count}");
            MetodoDePago metodoDePago = MetodoDePagoRepository.GetByID(ventas.FirstOrDefault().MetodoDePago.MetodoDePagoID);
            if (metodoDePago != null)
                comprobante.MetodoDePago = metodoDePago;
            else
            comprobante.MetodoDePago = ventas.FirstOrDefault().MetodoDePago;
            Log.Info($"Asignando la condición de pago: {ventas.Count}");
            CondicionDePago condicionDePago = CondicionDePagoRepository.GetByID(ventas.FirstOrDefault().CondicionDePago.CondicionDePagoID);
            if (condicionDePago != null)
                comprobante.CondicionDePago = condicionDePago;
            else
                comprobante.CondicionDePago = ventas.FirstOrDefault().CondicionDePago;
            Log.Info($"Asignando la referencia de pago: {ventas.Count}");
            var refPago = ventas.FirstOrDefault().ReferenciaDePago;
            comprobante.ReferenciaDePago = refPago != null ? refPago.Referencia : "";
            comprobante.Serie = Config.Serie;
            comprobante.Notas = "";
            comprobante.LeyendaDocumentoImpreso = "";
            comprobante.Status = new StatusComprobante { Status = "En Proceso" };
            comprobante.Ventas = VentaService.ConvertToLocalValues(ventas);

            Log.Info($"Salvando las ventas: {ventas.Count}");
            // TODO: Revisar si se tiene que cambiar de estacion a entidad
            // TODO: CATCH DE LAS VENTAS 1/9/15
            emisorEmpresaID = emisor.EmpresaID;
            //VentaService.Save(comprobante.Ventas.ToList(), emisor.EmpresaID);


            Log.Info($"Calculando los impuestos");
            // Revisar que tanto producto, como impuestos también esten en la base de datos central;
            // Obtener listado de impuestos para verificar que también existan en el servidor
            var impuestos = from t in comprobante.Ventas
                            from detalle in t.Detalles
                            from i in detalle.ImpuestosAplicados
                            select i.Impuesto;
            //await ImpuestoService.Merge(impuestos);

            Log.Info($"Calculando los impuestos");
            // Obtener listado de productos para verificar que también existan en el servidor
            var productos = from t in comprobante.Ventas
                            from venta in t.Detalles
                            select venta.Producto;
            //await ProductoService.Merge(productos);

            Log.Info($"Obteniendo lso detalles de la venta");
            // Obtener los detalles de las ventas
            var detalles = comprobante.Ventas.SelectMany((v) => v.Detalles);
            List < ComprobanteItem > itemsComprobante = new List<ComprobanteItem>();

            foreach (var detalle in detalles)
            {
                var item = new ComprobanteItem();
                item.Cantidad = detalle.Cantidad;
                item.Concepto = detalle.Concepto;
                item.ValorUnitario = detalle.ValorUnitario;
                item.Unidad = detalle.Unidad;
                item.Importe = detalle.Total;
                item.ImpuestosAplicados = detalle.ImpuestosAplicados;
                itemsComprobante.Add(item);
            }
            comprobante.Items = itemsComprobante;

            comprobante.Total = detalles.Sum(d => d.Total);
            comprobante.SubTotal = detalles.Sum(d => d.Subtotal);
            try
            {
                Log.Info($"Enciando el comprobante a facturar al sat {comprobante.ComprobanteID}");
                return await FacturarSAT(comprobante);
            }
            catch (Exception e)
            {
                Log.Error(e,"Error precargando la Excepcion");
                RepositorioComprobantes.Delete(comprobante);
                VentaService.Remove(comprobante.Ventas.ToList());
                return new FacturarResult { ResultCode = FacturarResultCode.SERVER_ERROR, Message = "Error cargando datos a Factura" };
            }

        }

        public async Task<FacturarResult> FacturarSAT(Comprobante comprobante)
        {
            Log.Info($"Preguardamos el comprobante");
            //Pre-guardar comprobante
            //TODO: PRE GUARDAR COMPROBANTE
            //RepositorioComprobantes.Insert(comprobante);
            //await RepositorioComprobantes.SaveAsync();

            // Obtener certificados
            Log.Info($"Obteniendo el certificado");
#if DEBUG
            var certificatePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, Config.RutaCertificado);
            var certificateKeyPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, Config.RutaLlavePrivada);
            var certificatePassword = Config.Password;
#else
            var certificatePath = Config.RutaCertificado;
            var certificateKeyPath = Config.RutaLlavePrivada;
            var certificatePassword = Config.Password;
#endif
            Log.Info($"Cargando el certificado con el siguiente path {certificatePath}");
            var sat = new SAT();
            sat.CargarCertificado(certificatePath);
            sat.CargarLlavePrivada(certificateKeyPath,certificatePassword);
            cfdv32.Comprobante comprobanteSAT = new cfdv32.Comprobante();

            Log.Info($"Agregando el No. de certificado del sat {sat.NoCertificado}");
            comprobanteSAT.noCertificado = sat.NoCertificado;
            comprobanteSAT.certificado = sat.Certificado;
            comprobante.Certificado = sat.NoCertificado;

            Log.Info($"Agregando los datos fiscales del emisor {comprobante.Empresa.DatosFiscales.RFC}");
            // Agregar datos fiscales del Emisor
            cfdv32.ComprobanteEmisor cEmisor = new cfdv32.ComprobanteEmisor();
            cEmisor.rfc = comprobante.Empresa.DatosFiscales.RFC;
            cEmisor.nombre = comprobante.Empresa.DatosFiscales.RazonSocial;
            cEmisor.RegimenFiscal = new cfdv32.ComprobanteEmisorRegimenFiscal[1];
            cfdv32.ComprobanteEmisorRegimenFiscal regimen = new cfdv32.ComprobanteEmisorRegimenFiscal();
            regimen.Regimen = comprobante.Empresa.DatosFiscales.RegimenFiscal ?? "Persona fisica con actividad empresarial";
            cEmisor.RegimenFiscal[0] = regimen;
            comprobanteSAT.Emisor = cEmisor;

            Log.Info($"Agregamos los datos fiscales del receptor {comprobante.DatosFiscalesCliente.RFC}");
            // Agregar datos fiscales del Receptor
            cfdv32.ComprobanteReceptor cReceptor = new cfdv32.ComprobanteReceptor();
            cReceptor.rfc = comprobante.DatosFiscalesCliente.RFC;
            cReceptor.nombre = comprobante.DatosFiscalesCliente.RazonSocial;
            cfdv32.t_Ubicacion direccion = new cfdv32.t_Ubicacion();
            direccion.calle = comprobante.DireccionCliente.Calle;
            direccion.noExterior = string.IsNullOrWhiteSpace(comprobante.DireccionCliente.NumExterior) ? "NA" : comprobante.DireccionCliente.NumExterior;
            if (!string.IsNullOrWhiteSpace(comprobante.DireccionCliente.NumInterior))
                direccion.noInterior = comprobante.DireccionCliente.NumInterior;
            direccion.colonia = comprobante.DireccionCliente.Colonia;
            direccion.codigoPostal = comprobante.DireccionCliente.CodigoPostal;
            direccion.municipio = comprobante.DireccionCliente.Municipio;
            if (!string.IsNullOrWhiteSpace(comprobante.DireccionCliente.Localidad))
                direccion.localidad = comprobante.DireccionCliente.Localidad;
            direccion.estado = comprobante.DireccionCliente.Estado.Nombre;
            direccion.pais = "Mexico";//comprobante.DireccionCliente.Estado.Pais.Nombre;
            if (!string.IsNullOrWhiteSpace(comprobante.DireccionCliente.Referencia))
                direccion.referencia = comprobante.DireccionCliente.Referencia;
            cReceptor.Domicilio = direccion;
            comprobanteSAT.Receptor = cReceptor;

            comprobanteSAT.metodoDePago = comprobante.MetodoDePago.Metodo;

            switch (comprobante.Tipo.Tipo)
            {
                case TipoComprobanteEnum.NOTA_DE_CREDITO:
                    comprobanteSAT.tipoDeComprobante = cfdv32.ComprobanteTipoDeComprobante.egreso;
                    break;
                case TipoComprobanteEnum.FACTURA:
                case TipoComprobanteEnum.HONORARIOS:
                default:
                    comprobanteSAT.tipoDeComprobante = cfdv32.ComprobanteTipoDeComprobante.ingreso;
                    break;
            }
            Log.Info($"Agregamos la fecha del SAT");
            // Agregar fecha SAT
            comprobanteSAT.LugarExpedicion = String.IsNullOrWhiteSpace(comprobante.DireccionCliente.Localidad) ?
                $"{comprobante.DireccionCliente.Municipio}, {comprobante.DireccionCliente.Estado.Nombre}" : // Sin localidad
                $"{comprobante.DireccionCliente.Municipio}, {comprobante.DireccionCliente.Localidad}, {comprobante.DireccionCliente.Estado.Nombre}"; // Con localidad

            Log.Info($"Asignamos la serie {comprobante.Serie} y el folio {comprobante.ComprobanteID.ToString("")}");
            // TODO: Agregar el folio y la serie correcta;
            comprobanteSAT.serie = comprobante.Serie;
            comprobanteSAT.folio = comprobante.ComprobanteID.ToString("");

            // Obtiene la fecha actual más la cantidad definida en el config (DateConfig -> Delta(HH:mm:ss))
            //TODO: Validación fecha
            comprobanteSAT.fecha = DateProvider.GetDateWithDelta();
            comprobanteSAT.formaDePago = comprobante.FormaDePago.Forma;
            comprobanteSAT.condicionesDePago = comprobante.CondicionDePago.Condicion;
            if(!string.IsNullOrWhiteSpace(comprobante.ReferenciaDePago))
                comprobanteSAT.NumCtaPago = comprobante.ReferenciaDePago;
            comprobanteSAT.subTotal = Convert.ToDecimal(comprobante.SubTotal.ToString("############0.000000"));
            comprobanteSAT.total = Convert.ToDecimal(comprobante.Total.ToString("############0.000000"));

            Log.Info($"Agrupa los conceptos");
            // Agrupar y Agregar conceptos
            var conceptos = new List<cfdv32.ComprobanteConcepto>();
            
            try {
              var  itemsAgrupados = from item in comprobante.Items
                                 group item by new { item.Concepto, item.ValorUnitario, item.Unidad } into grupo
                                 select new ComprobanteItem()
                                 {
                                     Cantidad = grupo.Sum(i => i.Cantidad),
                                     Concepto = grupo.Key.Concepto,
                                     ValorUnitario = grupo.Key.ValorUnitario,
                                     Unidad = grupo.Key.Unidad,
                                     Importe = grupo.Sum(i => i.Importe),
                                 };


                foreach (var item in itemsAgrupados)
                {
                    cfdv32.ComprobanteConcepto concepto = new cfdv32.ComprobanteConcepto();
                    concepto.cantidad = Convert.ToDecimal(item.Cantidad);
                    concepto.descripcion = item.Concepto;
                    concepto.importe = Convert.ToDecimal(item.Importe);
                    concepto.valorUnitario = Convert.ToDecimal(item.ValorUnitario);
                    concepto.unidad = item.Unidad;
                    conceptos.Add(concepto);
                }

            }
            catch(Exception ex)
            {
                Log.Error(ex, $"El error fue {ex.Message}");
            }

            comprobanteSAT.Conceptos = conceptos.ToArray();

            Log.Info($"Agrega los impuestos y las retenciones");
            // Agregar Impuestos
            cfdv32.ComprobanteImpuestos impuestos = new cfdv32.ComprobanteImpuestos();
            
            // Retenciones
            var impuestosRetenidos = from item in comprobante.Items
                                     from impuestoAplicado in item.ImpuestosAplicados
                                     where impuestoAplicado.Impuesto.Tipo == TipoImpuesto.RETENIDO
                                     group impuestoAplicado by new { Impuesto = impuestoAplicado.Impuesto.NombreSAT, Tasa = impuestoAplicado.Impuesto.Tasa }
                                        into grupo
                                     select new
                                     {
                                         Importe = grupo.Sum(g => g.Valor),
                                         Impuesto = grupo.Key.Impuesto,
                                         Tasa = grupo.Key.Tasa
                                     };

            if (impuestosRetenidos != null && impuestosRetenidos.Any())
            {
                var totalRetenido = impuestosRetenidos.Sum(i => i.Importe); 
                impuestos.totalImpuestosRetenidos = Convert.ToDecimal(Convert.ToDecimal(totalRetenido).ToString("############0.000000")); ;
                impuestos.totalImpuestosRetenidosSpecified = true;
                var retenciones = new List<cfdv32.ComprobanteImpuestosRetencion>();
                foreach (var retenido in impuestosRetenidos)
                {
                    var retencion = new cfdv32.ComprobanteImpuestosRetencion();
                    switch (retenido.Impuesto)
                    {
                        case "IVA":
                            retencion.impuesto = cfdv32.ComprobanteImpuestosRetencionImpuesto.IVA;
                            break;
                        case "ISR":
                            retencion.impuesto = cfdv32.ComprobanteImpuestosRetencionImpuesto.ISR;
                            break;
                    }
                    retencion.importe = Convert.ToDecimal(Convert.ToDecimal(retenido.Importe).ToString("############0.000000"));
                    retenciones.Add(retencion);
                }

                impuestos.Retenciones = retenciones.ToArray();
            }
            else{
                impuestos.totalImpuestosRetenidos = 0;
                impuestos.totalImpuestosRetenidosSpecified = false;
            }

            // Traslados
            var impuestosTrasladados = from item in comprobante.Items
                                       from impuestoAplicado in item.ImpuestosAplicados
                                     where impuestoAplicado.Impuesto.Tipo == TipoImpuesto.TRASLADADO && !impuestoAplicado.Impuesto.Oculto
                                     group impuestoAplicado by new { Impuesto = impuestoAplicado.Impuesto.NombreSAT, Tasa = impuestoAplicado.Impuesto.Tasa }
                                        into grupo
                                       select new
                                       {
                                           Importe = grupo.Sum(g => g.Valor),
                                           Impuesto = grupo.Key.Impuesto,
                                           Tasa = grupo.Key.Tasa
                                       };

            if (impuestosTrasladados.Any())
            {
                var totalTrasladado = impuestosTrasladados.Sum(i => i.Importe);
                impuestos.totalImpuestosTrasladados = Convert.ToDecimal(Convert.ToDecimal(totalTrasladado).ToString("############0.000000")); ;
                impuestos.totalImpuestosTrasladadosSpecified = true;
                var trasladados = new List<cfdv32.ComprobanteImpuestosTraslado>();
                foreach (var traslados in impuestosTrasladados)
                {
                    var trasladoSAT = new cfdv32.ComprobanteImpuestosTraslado();
                    switch (traslados.Impuesto)
                    {
                        case "IVA":
                            trasladoSAT.impuesto = cfdv32.ComprobanteImpuestosTrasladoImpuesto.IVA;
                            break;
                        case "IEPS":
                            trasladoSAT.impuesto = cfdv32.ComprobanteImpuestosTrasladoImpuesto.IEPS;
                            break;
                    }

                    trasladoSAT.importe = Convert.ToDecimal(Convert.ToDecimal(traslados.Importe).ToString("############0.000000"));
                    trasladoSAT.tasa = Convert.ToDecimal(Convert.ToDecimal(traslados.Tasa).ToString("############0.000000"));
                    trasladados.Add(trasladoSAT);
                }

                impuestos.Traslados = trasladados.ToArray();
            }
            else
            {
                impuestos.totalImpuestosTrasladados = 0;
                impuestos.totalImpuestosTrasladadosSpecified = false;
            }

            comprobanteSAT.Impuestos = impuestos;
            comprobanteSAT.version = "3.2";

            Log.Info($"Genera el XML Pre-CadenaOriginal");
            // Generar XML Pre-CadenaOriginal
            var xml = sat.GeneraXML(comprobanteSAT);

            Log.Info($"Calcula la cadena original");
            // Calcular Cadena Original
            var cadena = sat.GetCadenaOriginal(xml);

            Log.Info($"Agrega el Sello digital");
            // Agregar Sello
            var sello = sat.GenerarSello(cadena);
            comprobanteSAT.sello = sello;

            Log.Info($"Genera el XML con sello (sin timbre)");
            // Generar XML con sello (sin timbre)
            var xmlSellado = Parser.GeneraXML(comprobanteSAT);

            Log.Info($"Envia la factura al proveedor");
            // Enviar factura a proveedor
            var edicom = new Edicom.CFDiClient();
            var bytes = Encoding.UTF8.GetBytes(xmlSellado);
            byte[] respuestaEdicom;
            //Comprimir
            using (System.IO.MemoryStream outputMS = new System.IO.MemoryStream())
            {
                using (ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipOutput = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(outputMS))
                {
                    ICSharpCode.SharpZipLib.Zip.ZipEntry ze = new ICSharpCode.SharpZipLib.Zip.ZipEntry("Comprobante.xml");
                    zipOutput.PutNextEntry(ze);
                    zipOutput.Write(bytes, 0, bytes.Length);
                    zipOutput.Finish();
                    var finalBytes = outputMS.ToArray();
                    try
                    {
                        if(Test)
                    respuestaEdicom = edicom.getTimbreCfdiTest("HME0302244R2", "pxxiillkj", bytes);
                        else 
                            respuestaEdicom = edicom.getTimbreCfdi("HME0302244R2", "pxxiillkj", bytes);
                    }
                    catch (System.Web.Services.Protocols.SoapException soapEx)
                    {
                        Log.Error(soapEx, "Error al timbrar comprobante");
                        VentaService.Remove(comprobante.Ventas.ToList());
                        //RepositorioComprobantes.Delete(comprobante);
                        
                        return new FacturarResult { ResultCode = FacturarResultCode.PAC_ERROR, Message = soapEx.Message };
                    }
                    catch (Exception e)
                    {
                        Log.Error(e, "Error al timbrar comprobante");
                        RepositorioComprobantes.Delete(comprobante);
                        VentaService.Remove(comprobante.Ventas.ToList());
                        return new FacturarResult { ResultCode = FacturarResultCode.PAC_ERROR, Message = $"Error al timbrar comprobante" };
                    }
                }

                outputMS.Close();
            }

            //var bytesTimbre = edicom.getTimbreCfdiTest("HME0302244R2", "pxxiillkj", bytes);



            string timbre; //= Encoding.UTF8.GetString(bytesTimbre);
            //Descomprimir
            using (System.IO.MemoryStream inputMS = new System.IO.MemoryStream(respuestaEdicom))
            {
                using (ICSharpCode.SharpZipLib.Zip.ZipInputStream zipInput = new ICSharpCode.SharpZipLib.Zip.ZipInputStream(inputMS))
                {
                    ICSharpCode.SharpZipLib.Zip.ZipEntry ze = zipInput.GetNextEntry();
                    byte[] buffer = new byte[4082];
                    using (System.IO.MemoryStream msFinal = new System.IO.MemoryStream())
                    {
                        zipInput.CopyTo(msFinal);
                        timbre = Encoding.UTF8.GetString(msFinal.ToArray());
                        msFinal.Close();
                    }

                }

                inputMS.Close();
            }

            Log.Info($"Agrega el timbre al XML");
            // Agregar timbre a xml
            XmlDocument doc = new XmlDocument();
            timbre = timbre.Replace("xsi:schemaLocation=\"http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd\"",
                                    "schemaLocation=\"http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd\"");
            doc.LoadXml(timbre);
            comprobanteSAT.Complemento = new cfdv32.ComprobanteComplemento();
            comprobanteSAT.Complemento.Any = new XmlElement[1] { doc.DocumentElement };

            Log.Info($"Genera el XML final");
            // Generar XML final
            var xmlFinal = Parser.GeneraXML(comprobanteSAT);
            xmlFinal = xmlFinal.Replace("schemaLocation=", " xsi:schemaLocation=");
            var xmlComprobante = new XMLComprobante() { XML = xmlFinal };
            var version = doc.ChildNodes[0].Attributes["version"].Value;
            var uuid = doc.ChildNodes[0].Attributes["UUID"].Value.ToUpper();
            var fechaTimbrado = DateTime.Parse(doc.ChildNodes[0].Attributes["FechaTimbrado"].Value).ToString("yyyy-MM-ddTHH:mm:ss");
            var selloCFD = doc.ChildNodes[0].Attributes["selloCFD"].Value;
            var noCertificadoSat = doc.ChildNodes[0].Attributes["noCertificadoSAT"].Value;
            var cadenaOriginal = $"||{version}|{uuid}|{fechaTimbrado}|{selloCFD}|{noCertificadoSat}||";

            Log.Info($"Guarda en la base de datos");
            // Guardar en BD
            comprobante.Folio = comprobanteSAT.folio;
            comprobante.FechaTimbrado = DateTime.Parse(doc.ChildNodes[0].Attributes["FechaTimbrado"].Value);
            comprobante.SelloDigital = doc.ChildNodes[0].Attributes["selloCFD"].Value;
            comprobante.SelloDigitalPAC = doc.ChildNodes[0].Attributes["selloSAT"].Value;
            comprobante.CadenaOriginal = cadenaOriginal;
            comprobante.Mensaje = doc.ChildNodes[0].Attributes["noCertificadoSAT"].Value;
            comprobante.FolioSAT = uuid;
            comprobante.Xml = xmlComprobante;

            // TODO: Guardar el xml?
            //RepositorioComprobantes.Insert(comprobante);
            VentaService.Save(comprobante.Ventas.ToList(), emisorEmpresaID);
            RepositorioComprobantes.Insert(comprobante);
            await RepositorioComprobantes.SaveAsync();
            Log.Info($"Actualiza el comprobante");
            RepositorioComprobantes.Update(comprobante);
            await RepositorioComprobantes.SaveAsync();

            Log.Info($"Devolviendo resultado...");
            return new FacturarResult { ResultCode = FacturarResultCode.SUCCESS, FacturaID = comprobante.ComprobanteID, Factura = comprobante };
        }


        public async Task<List<Comprobante>> GetListadoFacturas(int clienteID)
        {
            var result = await RepositorioComprobantes.GetAsync(c => c.Cliente.ClienteID == clienteID);
            return result.ToList();
        }

        public async Task<MemoryStream> GetPDFFactura(int facturaID)
        {
            Log.Info($"Obteniendo la factura {facturaID}");
            var comprobante = RepositorioComprobantes.GetByID(facturaID);
            Log.Info($"Comprobante {comprobante}");
            Log.Info($"Usamos el metodo de {PDFService}");
            MemoryStream NewMemoryStream = PDFService.GetPDF(comprobante) as MemoryStream;
            return await Task.FromResult<MemoryStream>(PDFService.GetPDF(comprobante) as MemoryStream);
        }

        public Task<MemoryStream> GetXMLFactura(int facturaID)
        {
            var comprobante = RepositorioComprobantes.GetByID(facturaID);
            if (comprobante != null && comprobante.Xml != null && comprobante.Xml.XML != null)
                return Task.FromResult<MemoryStream>(new MemoryStream(Encoding.UTF8.GetBytes(comprobante.Xml.XML)));
            return null;
        }

        public Task<FacturarResult> Facturar(FacturaViewModel viewModel)
        {
            throw new NotImplementedException();
        }
    }
}
