﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HACSYS.Management.DAL.Models;
using HACSYS.Management.DAL.Repositories;
using HACSYS.BizMan.Facturacion.Web.Repositories;
using HACSYS.BizMan.Facturacion.Web.Results;
using HACSYS.BizMan.Facturacion.Web.Models;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using NLog;
using HACSYS.BizMan.Facturacion.Web.Controllers;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class ClientService : IClienteService
    {
        private IClienteRepository RepositorioClientes;
        private IDireccionRepository RespositorioLocalDireccion;
        private IUsuarioRepository RepositorioUsuarios;
        private IDatosFiscalesRepository RepositorioDatosfiscales;
        private Logger Log = LogManager.GetLogger("FacturacionHub");

        public ClientService(IClienteRepository repositorioClientes, IDireccionRepository repositoriolocaldireccion, IUsuarioRepository repositorioUsuarios, IDatosFiscalesRepository datosrepo)
        {
            RepositorioClientes = repositorioClientes;
            RespositorioLocalDireccion = repositoriolocaldireccion;
            RepositorioUsuarios = repositorioUsuarios;
            RepositorioDatosfiscales = datosrepo;
           
        }

        //public async Task<ResponseRegistration> Add(
        //    string rfc,
        //    string razonSocial,
        //    string mailNotifications,
        //    string calle,
        //    string numExterior,
        //    string numInterior,
        //    string colonia,
        //    string localidad,
        //    string referencia,
        //    string codigoPostal,
        //    string municipio)
        //{
        //    Direccion ClienteDireccion = new Direccion();
        //    Cliente cliente = new Cliente();

        //    if (String.IsNullOrEmpty(rfc) || string.IsNullOrEmpty(razonSocial) || String.IsNullOrEmpty(numInterior) || String.IsNullOrEmpty(colonia) || String.IsNullOrEmpty(localidad) || String.IsNullOrEmpty(codigoPostal) || String.IsNullOrEmpty(municipio))
        //        return new ResponseRegistration() { clienteStatus = ClienteStatusCode.DataMissMatch, cliente = null };
        //    else
        //    {
        //        // Direccion data
        //        ClienteDireccion.Calle = calle;
        //        ClienteDireccion.NumExterior = numExterior;
        //        ClienteDireccion.NumInterior = numInterior;
        //        ClienteDireccion.Colonia = colonia;
        //        ClienteDireccion.Localidad = localidad;
        //        ClienteDireccion.Referencia = referencia;
        //        ClienteDireccion.CodigoPostal = codigoPostal;
        //        ClienteDireccion.Municipio = municipio;

        //        //Client data
        //        cliente.RFC = rfc;
        //        cliente.RazonSocial = razonSocial;
        //        cliente.EmailNotificaciones = mailNotifications;
        //        cliente.Direcciones.Add(ClienteDireccion);
        //    }

        //    var resultado = await RepositorioLocal.AsyncGet(q => q.RFC.Equals(rfc));
        //    resultado = resultado.Where(r => r.RazonSocial.Equals(razonSocial));

        //    if (resultado != null)
        //    {
        //        // Aquí verificamos que aun que el objeto venga con algo, necesite tener datos para poder trabajar con él
        //        if (resultado.Count() >= 0)
        //        {
        //            // Si no hay ningun cliente registrado con esos datos
        //            RepositorioLocal.Insert(cliente);

        //            return new ResponseRegistration { clienteStatus = ClienteStatusCode.Success, cliente = cliente };
        //        }
        //        else
        //        {
        //            // Si todos los datos están bien y no hay ningun tipo de error le regresamos un Success y el despacho encontrado
        //            return new ResponseRegistration() { clienteStatus = ClienteStatusCode.ClientRegistered, cliente = resultado.FirstOrDefault() };
        //        }
        //    }

        //    return null;
        //}

        //public async Task<ResponseRegistration> Get(string rfc, string razonSocial)
        //{
        //    var resultado = await RepositorioLocal.AsyncGet(q => q.RFC.Equals(rfc));
        //    resultado = resultado.Where(r => r.RazonSocial.Equals(razonSocial));

        //    if (resultado != null)
        //    {
        //        // Aquí verificamos que aun que el objeto venga con algo, necesite tener datos para poder trabajar con él
        //        if (resultado.Count() == 0)
        //        {
        //            // Si no tiene información del cliente regresa el despacho nulo y el código señalando que no fueron
        //            // Encontrados resutlados con el query
        //            return new ResponseRegistration { clienteStatus = ClienteStatusCode.NotFound, cliente = null };
        //        }
        //        else
        //        {
        //            // En caso de que se haya encontrado PERO no coincidan las cantidades de monto en despacho y
        //            if (resultado.FirstOrDefault().RFC != rfc) { return new ResponseRegistration() { clienteStatus = ClienteStatusCode.DataMissMatch, cliente = resultado.FirstOrDefault() }; }
        //            else
        //            {
        //                // Si todos los datos están bien y no hay ningun tipo de error le regresamos un Success y el despacho encontrado
        //                return new ResponseRegistration() { clienteStatus = ClienteStatusCode.Success, cliente = resultado.FirstOrDefault() };
        //            }
        //        }
        //    }

        //    return null;
        //}

        public async Task<ResponseRegistration> Get(string rfc, string razonSocial)
        {
            throw new NotImplementedException();
            //// TODO: Probar si deja buscar el RFC dentro de Datos Fiscales o hay que hacer join antes
            //Cliente resultado = RepositorioLocal.Get(q => q.DatosFiscales.RFC.Equals(rfc)).FirstOrDefault();
            ////var resultado = await RepositorioLocal.AsyncGet(q => q.RFC.Equals(rfc));
            ////resultado = resultado.Where(r => r.RazonSocial.Equals(razonSocial));

            //if (resultado != null)
            //{
            //    // Aquí verificamos que aun que el objeto venga con algo, necesite tener datos para poder trabajar con él

            //        // En caso de que se haya encontrado PERO no coincidan las cantidades de monto en despacho y
            //        if (resultado.RFC != rfc) { return new ResponseRegistration() { clienteStatus = ClienteStatusCode.DataMissMatch, cliente = resultado }; }
            //        else
            //        {
            //            // Si todos los datos están bien y no hay ningun tipo de error le regresamos un Success y el despacho encontrado
            //            return new ResponseRegistration() { clienteStatus = ClienteStatusCode.Success, cliente = resultado };
            //        }

            //}else
            //{
            //    return new ResponseRegistration { clienteStatus = ClienteStatusCode.NotFound, cliente = null }; 
            //}

            //return null;
        }


        public Task<Cliente> GetCliente(int id)
        {
            Cliente c = (RepositorioClientes.GetByID(id)); 
            return Task.FromResult<Cliente>(RepositorioClientes.GetByID(id));
        }

        public Task<Cliente> GetClienteFromUserID(int id)
        {
            var user = RepositorioUsuarios.GetByID(id);
            if (user == null)
                return Task.FromResult<Cliente>(null);
            return Task.FromResult<Cliente>(user.Cliente);
        }

        public void Insert(Cliente cliente)
        {
            throw new NotImplementedException();
        }

        // Agrega dirección CAMBIAR POR DIRECCION + DATOS FISCALES
        public Direccion UpdateClientDetails(Direccion entity, int id)
        {
            //DatosFiscales DatosFiscalesActualizar = RepositorioDatosfiscales.GetByID(id);
            //Direccion DireccionConIDGenerado = saveDireccion(entity);
            //DatosFiscalesActualizar.Direccion = null;
            //DatosFiscalesActualizar.Direccion = DireccionConIDGenerado;
            //RepositorioDatosfiscales.Update(DatosFiscalesActualizar);
            //int i = RepositorioDatosfiscales.Save();

            entity.Estado.Direcciones = null;

            RespositorioLocalDireccion.Insert(entity);
            
            RespositorioLocalDireccion.Save();
            Direccion DireccionConIDGenerado = RespositorioLocalDireccion.Get(q => q.Calle.Equals(entity.Calle) && q.Colonia.Equals(entity.Colonia)).FirstOrDefault();
            return DireccionConIDGenerado;
        }


        public async Task<UserToken> UToken(string token)
        {
            //return RepositorioLocalToken.Get(z => z.Token.Equals(token)).FirstOrDefault();
            return null;
        }

        public Cliente changeDireccion(int uid, Direccion ndireccion, Direccion vdireccion)
        {
            try
            {
                //RespositorioLocalDireccion.removeDefault(uid, vdireccion, ndireccion);
                removeDefault(uid, vdireccion, ndireccion);
                return RepositorioClientes.GetByID(uid);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;

            }
        }

        private void removeDefault(int uid, Direccion vdireccion, Direccion ndireccion)
        {
            vdireccion.DireccionDefault = false;
            RespositorioLocalDireccion.Update(vdireccion);
            ndireccion.DireccionDefault = true;
            RespositorioLocalDireccion.Update(ndireccion);
        }

        public Direccion getDireccion(int direccionID)
        {

                return RespositorioLocalDireccion.GetByID(direccionID);

        }

        //TODO: REMOVER
        public Cliente changeDireccion(Cliente cliente)
        {
            //return RepositorioLocal.changeDireccion(cliente);
            throw new NotImplementedException();
        }

        public Cliente getByEmail(string email)
        {
            //return RepositorioLocal.getByMail(email);

            return RepositorioClientes.Get(q => q.DatosFiscales.Correo.Equals(email)).FirstOrDefault();
            throw new NotImplementedException();
        }

        public Task<ResponseRegistration> Add(string rfc, string razonSocial, string mailNotifications, Direccion direccion)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Direccion>> GetDirecciones(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<string> getCorreo(int id)
        {
            var idCliente = RepositorioClientes.GetByID(id);
            return RepositorioDatosfiscales.GetByID(idCliente.DatosFiscalesID).Correo;
        }

        public async Task addMail(int id, string mail,string umail)
        {

            Log.Info($"Obtenemos el primer correo registrado {umail}");
            Log.Info($"Agregando el mail {mail} al cliente {id}");
            var idCliente = RepositorioClientes.GetByID(id);
            Log.Info($"Obtenemos el cliente {idCliente.ClienteID} con los datos fiscales {idCliente.DatosFiscalesID}");
            var DatosFiscales = RepositorioDatosfiscales.GetByID(idCliente.DatosFiscalesID);
            Log.Info($"Agregando el mail {mail} al cliente {id}");
            if (DatosFiscales.Correo == null || DatosFiscales.Correo == "")
            {
                DatosFiscales.Correo = umail + "," +mail;
                RepositorioDatosfiscales.Update(DatosFiscales);
                RepositorioDatosfiscales.Save();
            }
            else
            {
                string aux = DatosFiscales.Correo;
                aux = aux + "," + mail;
                DatosFiscales.Correo = aux;
                RepositorioDatosfiscales.Update(DatosFiscales);
            }

        }
    }
}
