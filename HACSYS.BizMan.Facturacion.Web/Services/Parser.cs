﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using HACSYS.Tools.Security.Certificates;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class Password : Org.BouncyCastle.OpenSsl.IPasswordFinder
    {
        private readonly char[] password;

        public Password(string word)
        {
            this.password = (char[])word.ToCharArray().Clone();
        }

        public char[] GetPassword()
        {
            return (char[])password.Clone();
        }
    }
    public class Parser : IParser
    {
        private AsymmetricKeyParameter PrivateKey;
        private AsymmetricKeyParameter PublicKey;
        public Parser(ICertificateProvider<X509Certificate> provider)
        {

        }
        public string GeneraXML(cfdv32.Comprobante comprobante)
        {
            try
            {
                XmlSerializerNamespaces xmlNameSpace = new XmlSerializerNamespaces();
                xmlNameSpace.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                xmlNameSpace.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
                using (System.IO.MemoryStream stream = new MemoryStream())
                {
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(stream, Encoding.UTF8);
                    xmlTextWriter.Formatting = Formatting.Indented;
                    XmlSerializer xs = new XmlSerializer(typeof(cfdv32.Comprobante));

                    xs.Serialize(xmlTextWriter, comprobante, xmlNameSpace);
                    xmlTextWriter.Flush();

                    stream.Position = 0;
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("BizMan", "GeneraXML:" + ex.Message);
                return "";
            }

        }

        public string CadenaOriginalv3(string RutaArchivoXML)
        {
            if (System.IO.File.Exists(RutaArchivoXML + ".xml"))
            {
                Stream XSLT = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".cadenaoriginal_3_2.xslt");

                System.IO.StringWriter strWriter = new StringWriter();
                XmlReader Reader = new XmlTextReader(XSLT);

                System.Xml.Xsl.XslCompiledTransform transformer = new System.Xml.Xsl.XslCompiledTransform();

                transformer.Load(Reader);
                transformer.Transform(RutaArchivoXML + ".xml", null, strWriter);

                return Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(System.Text.RegularExpressions.Regex.Replace(strWriter.ToString(), @"(^\s+)|(\s+$)|(\s+(?=\s.))", "")));

            }
            return "";

        }

        public bool CargarLlavePrivada(string ArchivoLlavePrivada, string password)
        {
            //Leemos el DER
            if (File.Exists(ArchivoLlavePrivada))
            {
                try
                {
                    byte[] signed = File.ReadAllBytes(ArchivoLlavePrivada);
                    //MemoryStream ms = new MemoryStream(UTF8Encoding.UTF8.GetBytes(Convert.ToBase64String(signed)));
                    //byte[] decoded = Org.BouncyCastle.Utilities.Encoders.Base64.Decode(ms.ToArray());
                    Password pass = new Password(password);

                    Asn1Object sigASN = Asn1Object.FromByteArray(signed);
                    EncryptedPrivateKeyInfo pInfo = EncryptedPrivateKeyInfo.GetInstance(sigASN);
                    PrivateKeyInfo decrypted = PrivateKeyInfoFactory.CreatePrivateKeyInfo(pass.GetPassword(), pInfo);
                    PrivateKey = (RsaPrivateCrtKeyParameters)PrivateKeyFactory.CreateKey(decrypted);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                try
                {
                    byte[] signed = Convert.FromBase64String(ArchivoLlavePrivada);
                    //MemoryStream ms = new MemoryStream(UTF8Encoding.UTF8.GetBytes(Convert.ToBase64String(signed)));
                    //byte[] decoded = Org.BouncyCastle.Utilities.Encoders.Base64.Decode(ms.ToArray());
                    Password pass = new Password(password);

                    Asn1Object sigASN = Asn1Object.FromByteArray(signed);
                    EncryptedPrivateKeyInfo pInfo = EncryptedPrivateKeyInfo.GetInstance(sigASN);
                    PrivateKeyInfo decrypted = PrivateKeyInfoFactory.CreatePrivateKeyInfo(pass.GetPassword(), pInfo);
                    PrivateKey = (RsaPrivateCrtKeyParameters)PrivateKeyFactory.CreateKey(decrypted);
                    return true;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.EventLog.WriteEntry("PetroFact", "SAT Cargar LlavePrivada\n" + ex.Message);
                    return false;
                }
            }

        }

        //public string GenerarSello( string CadenaOriginal, X509Certificate2 certificate)
        //{
        //    var publicKey = // certificate.GetPublicKey();
        //    if (publicKey != null && PrivateKey != null)
        //    {
        //        AsymmetricCipherKeyPair key = new AsymmetricCipherKeyPair(publicKey, PrivateKey);
        //        //ISigner sig = SignerUtilities.GetSigner("MD5WithRSA");//MD5WITHRSA/ISO9796-2
        //        ISigner sig = SignerUtilities.GetSigner("SHA-1withRSA");//SHA1WITHRSA/ISO9796-2

        //        byte[] Mensaje = System.Text.UTF8Encoding.UTF8.GetBytes(CadenaOriginal);
        //        sig.Init(true, key.Private);
        //        sig.BlockUpdate(Mensaje, 0, Mensaje.Length);
        //        byte[] signature = sig.GenerateSignature();
        //        return Convert.ToBase64String(signature);
        //        //Convert.ToBase64String(Org.BouncyCastle.Utilities.Encoders.Base64.Encode(signature));
        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}

        public string GetCadenaOriginal(string XML)
        {
            if (XML.Length > 0)
            {
                Stream XSLT = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".Resources.cadenaoriginal_3_2.xslt");

                System.IO.StringWriter strWriter = new StringWriter();
                XmlReader Reader = new XmlTextReader(XSLT);

                System.Xml.Xsl.XslCompiledTransform transformer = new System.Xml.Xsl.XslCompiledTransform();

                transformer.Load(Reader);
                XmlReader xml = XmlReader.Create(new System.IO.StringReader(XML));
                transformer.Transform(xml, null, strWriter);

                return Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(System.Text.RegularExpressions.Regex.Replace(strWriter.ToString(), @"(^\s+)|(\s+$)|(\s+(?=\s.))", "")));

            }
            return "";

        }

        public string GenerarSello(string CadenaOriginal, X509Certificate2 certificate)
        {
            throw new NotImplementedException();
        }
    }
}
