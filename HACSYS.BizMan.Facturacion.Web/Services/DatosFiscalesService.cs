﻿using System;
using System.Linq;
using System.Threading.Tasks;
using HACSYS.Management.DAL.Models;
using HACSYS.Management.DAL.Repositories;
using NLog;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class DatosFiscalesService : IDatosFiscalesService
    {
        private Logger Log = LogManager.GetLogger("DatosFiscalesService");
        private IDireccionRepository RespositorioLocalDireccion;
        private IDatosFiscalesRepository RepositorioDatosfiscales;
        private IEstadoRepository RepositorioEstado;
        IClienteRepository Clienterepo;
        public DatosFiscalesService(IEstadoRepository repoestado, IClienteRepository clienterepo,IDireccionRepository repositoriolocaldireccion/*, IUserTokenRepository repositoriolocaltoken*/, IDatosFiscalesRepository datosrepo)
        {
            Clienterepo = clienterepo;
            RespositorioLocalDireccion = repositoriolocaldireccion;
            //RepositorioLocalToken = repositoriolocaltoken;
            RepositorioDatosfiscales = datosrepo;
            RepositorioEstado = repoestado;

        }

        public async Task<DatosFiscales> changeDatos(Direccion direccion, int id)
        {
            Log.Info($"Comienza obteniendo los datos del cliente a actualizar");
            Cliente c = Clienterepo.Get(q => q.ClienteID == id).FirstOrDefault();
            int idF = c.DatosFiscales.DatosFiscalesID;
            Log.Info($"Obtenemos lso datos fiscales del cliente");
            DatosFiscales DatosFiscalesActualizar = RepositorioDatosfiscales.GetByID(idF);

            Log.Info($"Asignamos los datos de la nueva dirección");
            ////DatosFiscalesActualizar.Direccion.Calle = direccion.Calle;
            ////DatosFiscalesActualizar.Direccion.CodigoPostal = direccion.CodigoPostal;
            ////DatosFiscalesActualizar.Direccion.Colonia = direccion.Colonia;
            ////DatosFiscalesActualizar.Direccion.Estado.EstadoID = direccion.Estado.EstadoID;
            Estado edo = RepositorioEstado.GetByID(direccion.Estado.EstadoID);
            if (edo != null)
            {
                direccion.Estado = edo;
                DatosFiscalesActualizar.Direccion = direccion;
            }
            else
                DatosFiscalesActualizar.Direccion = direccion;

            Log.Info($"Realizamos un update a los datos fiscales");
            RepositorioDatosfiscales.Update(DatosFiscalesActualizar);
            
            int i = RepositorioDatosfiscales.Save();
            Log.Info($"Despues del update se realizaron {i} operaciones con el db context");

            return RepositorioDatosfiscales.GetAsync(q => q.DatosFiscalesID == idF).Result.FirstOrDefault();
        }

        public async Task<DatosFiscales> AgregarCorreo(string correo, int id)
        {
            Log.Info($"Comienza obteniendo los datos del cliente a actualizar");
            Cliente c = Clienterepo.Get(q => q.ClienteID == id).FirstOrDefault();
            int idF = c.DatosFiscales.DatosFiscalesID;
            Log.Info($"Obtenemos lso datos fiscales del cliente");
            DatosFiscales DatosFiscalesActualizar = RepositorioDatosfiscales.GetByID(idF);

            Log.Info($"Revisamos si hay algún correo registrado");
            if (DatosFiscalesActualizar.Correo != null)
            {
                Log.Info($"Agregamos un correo a la lista");
                DatosFiscalesActualizar.Correo += ","+correo;
                Log.Info($"Realizamos un update a los datos fiscales");

                Log.Info($"Hacemos el update");
                RepositorioDatosfiscales.Update(DatosFiscalesActualizar);

                int i = RepositorioDatosfiscales.Save();
                Log.Info($"Despues del update se realizaron {i} operaciones con el db context");

                return RepositorioDatosfiscales.GetAsync(q => q.DatosFiscalesID == idF).Result.FirstOrDefault();
            }
            else
            {
                Log.Info($"Agregamos el correo {correo}");
                DatosFiscalesActualizar.Correo = correo;

                Log.Info($"Realizamos un update a los datos fiscales");
                RepositorioDatosfiscales.Update(DatosFiscalesActualizar);

                int i = RepositorioDatosfiscales.Save();
                Log.Info($"Despues del update se realizaron {i} operaciones con el db context");

                return RepositorioDatosfiscales.GetAsync(q => q.DatosFiscalesID == idF).Result.FirstOrDefault();
            }

            
        }

    }
}
