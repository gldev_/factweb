﻿using HACSYS.BizMan.Facturacion.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IConfigFacturacionService
    {
        ConfigFacturacion getConfig(int id);

    }
}
