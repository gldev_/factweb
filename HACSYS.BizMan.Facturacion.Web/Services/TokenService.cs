﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using System.Net.Mail;
using HACSYS.BizMan.Facturacion.Web.Repositories;
using HACSYS.BizMan.Facturacion.Web.Models;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class TokenService : ITokenService
    {
        private IUserTokenRepository RepositorioLocal;
        public TokenService(IUserTokenRepository repositorioLocal)
        {
            RepositorioLocal = repositorioLocal;
        }


        public Task<string> GenerateToken(string rfc, string emailTo)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(rfc));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("X2"));
                }

                var date = DateTime.Now;

                date = date.AddDays(7);

                UserToken ut = new UserToken() { Expira = date.ToString(), emailTo = emailTo, Token = sb.ToString() };

                RepositorioLocal.Insert(ut);

                



                return Task.FromResult<string>(sb.ToString());



            }

        }
    }
}
