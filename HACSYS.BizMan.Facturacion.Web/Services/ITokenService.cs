﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface ITokenService
    {
        Task<string> GenerateToken(string rfc, string emailTo);
    }
}
