﻿using HACSYS.BizMan.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IDatosPagoService
    {
        CondicionDePago GetCondicionDePago(string condicion);
        MetodoDePago GetMetodoDePago(string metodo);
        FormaDePago GetFormaDePago(string forma);
    }
}
