﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HACSYS.Management.DAL.Models;
using HACSYS.Management.DAL.Repositories;
using System.Web.Configuration;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class EmpresasService : IEmpresasService
    {
        private int CurrentEmpresaID;
        private IEmpresaRepository RepositorioEmpresas;
        private Empresa Current;
        public EmpresasService(IEmpresaRepository repositorioEmpresas)
        {
            RepositorioEmpresas = repositorioEmpresas;
            if(WebConfigurationManager.AppSettings["EmpresaID"] != null)
                CurrentEmpresaID = Int32.Parse(WebConfigurationManager.AppSettings["EmpresaID"]);
        }
        public async Task<Empresa> GetCurrent()
        {
            return RepositorioEmpresas.GetByID(CurrentEmpresaID);
        }
    }
}