﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IEmailService : IIdentityMessageService
    {
        Task SendAsync(string email, Stream pdf, Stream xml, string pdfName = null, string xmlName = null);
        Task SendAsync(int empresaid, string gpo);
        Task SendAsync(string email, Stream pdf, Stream xml, string correos, string pdfName = null, string xmlName = null);
    }
}