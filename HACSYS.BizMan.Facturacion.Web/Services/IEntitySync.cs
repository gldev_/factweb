﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IEntitySync<TEntity> where TEntity:class
    {
        // Obtiene los cambios efectuados en el servidor
        Task Pull();
        // Envia los cambios locales al servidor
        Task Push(IEnumerable<TEntity> items);
        // Se asegura de que las items esten bien sincronizadas (ambas esten en el cliente y en el servidor)
        Task Merge(IEnumerable<TEntity> items);
    }
}
