﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.DAL.Repositories;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    class DatosPagoService : IDatosPagoService
    {
        private ICondicionDePagoRepository RepositorioCondicionDePago;
        private IMetodoDePagoRepository RepositorioMetodoDePago;
        private IFormaDePagoRepository RepositorioFormaDePago;
        public DatosPagoService(ICondicionDePagoRepository repositorioCondicionDePago, IMetodoDePagoRepository repositorioMetodoDePago, IFormaDePagoRepository repositorioFormaDePago)
        {
            RepositorioCondicionDePago = repositorioCondicionDePago;
            RepositorioMetodoDePago = repositorioMetodoDePago;
            RepositorioFormaDePago = repositorioFormaDePago;
        }

        public CondicionDePago GetCondicionDePago(string condicion)
        {
            return RepositorioCondicionDePago.Get(c => c.Condicion == condicion).FirstOrDefault() ?? new CondicionDePago { Condicion = condicion };
        }

        public FormaDePago GetFormaDePago(string forma)
        {
            return RepositorioFormaDePago.Get(f => f.Forma == forma).FirstOrDefault() ?? new FormaDePago { Forma = forma };
        }

        public MetodoDePago GetMetodoDePago(string metodo)
        {
            return RepositorioMetodoDePago.Get(c => c.Metodo == metodo).FirstOrDefault() ?? new MetodoDePago { Metodo = metodo };
        }
    }
}
