﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.Facturacion.DAL.Models;
using HACSYS.BizMan.Facturacion.Web.QueryResponses;
using HACSYS.BizMan.Facturacion.Web.Results;
using HACSYS.BizMan.Facturacion.Web.ViewModels;
using HACSYS.Management.DAL.Models;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public interface IFacturacionService
    {
        Task<bool> VentaFacturada(Venta venta);
        Task<bool> VentaFacturada(int estacionID, string folio, double monto);
        Task<FacturarResult> Facturar(Empresa emisor, Cliente cliente, List<Venta> items);
        Task<FacturarResult> Facturar(Cliente cliente, List<Venta> items);
        Task<FacturarResult> Facturar(FacturaViewModel viewModel);
        Task<MemoryStream> GetPDFFactura(int facturaID);
        Task<MemoryStream> GetXMLFactura(int facturaID);
        Task<List<Comprobante>> GetListadoFacturas(int clienteID);

    }
}
