﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.Facturacion.Web.Services
{
    public class DateProvider : IDateProvider
    {
        public DateProvider()
        {

        }
        public DateTime GetDateWithDelta()
        {
            // TODO: Implementar Correctamente
            //var deltaString = ""; 
            //var delta = DateTime.ParseExact(deltaString, "HH:mm:ss", CultureInfo.InvariantCulture);
            //return GetDateWithDelta(delta.TimeOfDay);
            return DateTime.Now;
        }

        public DateTime GetDateWithDelta(TimeSpan delta)
        {
            return DateTime.Now.Add(delta);
        }
    }
}
