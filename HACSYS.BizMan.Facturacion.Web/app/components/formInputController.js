﻿angular.module('General', [])
.directive('formInput', function () {
    return {
        restrict: 'E',
        templateUrl: 'app/components/form-input.html',
        scope: {
            size: '@',
            errorText: '@',
            name: '@',
            id: '@',
            placeholder: '@',
            pattern: '@',
            type: '@',
            icon: '@',
            required: '=?',
            errors: '=?',
            model: '=',
            form: '=',
            compareTo: '=',
            validate: '='
        },
        link: function (scope, element, attrs, controllers) {
            scope.OnFocus = function (obj) {
                $(obj.target).closest(".textbox-wrap").addClass("focused");
            };
            scope.OnBlur = function (obj) {
                $(obj.target).closest(".textbox-wrap").removeClass("focused");
            }
            scope.Regx = new RegExp(scope.pattern);
            if (!scope.type)
                scope.type = 'text';

            if (!scope.icon)
                scope.icon = 'fa-font';

            if (scope.compareTo != undefined) {
                scope.form[scope.name].$validators.compareTo = function(modelValue)
                {
                    return modelValue == scope.compareTo;
                }

                scope.$watch("model", function () {
                    scope.form[scope.name].$validate();
                });
            }

            if(scope.validate != undefined)
            {
                scope.form[scope.name].$validators.inputValidation = scope.validate
            }

            if(scope.errors == undefined)
            {
                scope.errors = [scope.errorText];
            }

            if(scope.required == undefined)
            {
                scope.required = false;
            }
        }
    }
});