﻿angular.module('Facturar')
.controller('DatosFiscalesController', ['$scope', 'DireccionesService', 'DatosFiscalesService', '$rootScope', function ($scope, DireccionesService, DatosFiscalesService,$rootScope) {
    var self = this;
    self.Cliente = {};
    self.Direccion = {};
    self.Paises = [];
    self.PaisSeleccionado = {};
    self.DatosFiscalesForm = $scope.form;
    $scope.model.Cliente = self.Cliente;
    $scope.model.Direccion = self.Direccion;
    self.UserDatosFiscales = {};
    // Funciones para las formas


    self.OnFocus = function (obj) {
        $(obj.target).closest(".textbox-wrap").addClass("focused");
    }

    self.OnBlur = function (obj) {
        $(obj.target).closest(".textbox-wrap").removeClass("focused");
    }

    // Obtener la lista de paises
    DireccionesService.GetPaises().success(function (data) {
        $.each(data, function () {
            self.Paises.push(this);
        });
    });
}])
.directive('datosFiscales', function () {
    return {
        restrict: 'E',
        templateUrl: 'app/components/datos-fiscales.html',
        controller: 'DatosFiscalesController',
        controllerAs: 'DatosFiscalesCtrl',
        scope: {
            model: '=',
            form: '='
        }
    }
})
.directive('datosFiscalesV', function () {
    return {
        restrict: 'E',
        templateUrl: 'app/components/datos-fiscales-v.html',
        controller: 'DatosFiscalesController',
        controllerAs: 'DatosFiscalesCtrl',
        scope: {
            model: '=',
            form: '='
        }
    }
});