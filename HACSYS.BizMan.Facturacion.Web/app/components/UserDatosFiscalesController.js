﻿angular.module('Usuario')
.controller('UserDatosFiscalesController', ['$cookies','$rootScope', '$scope', 'DireccionesService', 'UserService', function ($cookies,$rootScope, $scope, DireccionesService, UserService) {
    var self = this;
    self.Cliente = {};
    self.Direccion = { };
    self.Paises = [];
    self.PaisSeleccionado = {};
    self.DatosFiscalesForm = $scope.form;
    $scope.model.Cliente = self.Cliente;
    $scope.model.Direccion = self.Direccion;

    // Funciones para las formas
    self.OnFocus = function (obj) {
        $(obj.target).closest(".textbox-wrap").addClass("focused");
    }

    self.OnBlur = function (obj) {
        $(obj.target).closest(".textbox-wrap").removeClass("focused");
    }

    self.uid = $cookies.getObject("Account");
    UserService.GetUDetails(self.uid.userID);
    $rootScope.alertSite = function (datosUsuario, datosDireccion, direccionArray) {
        $rootScope.Fiscales = datosUsuario;
        self.userData = datosUsuario;
        self.userData.Direccion = datosDireccion;

    };

    // Obtener la lista de paises
    DireccionesService.GetPaises().success(function (data) {
        $.each(data, function () {
            self.Paises.push(this);
        });

    });
}])
.directive('datosFiscalesUser', function () {
    return {
        restrict: 'E',
        templateUrl: 'app/components/direccion-edit.html',
        controller: 'UserDatosFiscalesController',
        controllerAs: 'UDatosFiscalesCtrl',
        scope: {
            model: '=',
            form: '='
        }
    }
})
.directive('userdatosFiscalesV', function () {
    return {
        restrict: 'E',
        templateUrl: 'app/components/datos-fiscales-v.html',
        controller: 'UserDatosFiscalesController',
        controllerAs: 'UDatosFiscalesCtrl',
        scope: {
            model: '=',
            form: '='
        }
    }
});