﻿(function () {
    angular
        .module('Account')
        .factory('AccountService', AccountService);

    AccountService.$inject = ['$http', 'AuthService','$cookies'];

    function AccountService($http, AuthService, $cookies) {
        var obj = {};

        obj.Register = function (data) {
            var func = $http.post('api/Account/Register', data);
            func.success(function (data) {
                if (data.StatusCode == 200) {
                    obj = data;
                }
            });
            return func;
        };

        obj.ForgotPassword = function (email) {
            return $http.post('api/Account/ForgotPassword', { Email: email });
        }

        obj.ResetPassword = function (data) {
            return $http.post('api/Account/ResetPassword', data);
        }

        obj.ChangePassword = function (data) {
            return $http.post('api/Account/ChangePassword', data);
        }

        obj.Login = function (data) {
            var func = $http.post('api/Account/Login', data);
            func.success(function (data) {
                if (data.StatusCode == 200) {
                    AuthService.Logged = true;
                    AuthService.GetUser();
                }
            });
            return func;
        };

        obj.Logoff = function () {
            return $http.get('api/Account/Logoff')
                .success(function () {
                    AuthService.User = {};
                    AuthService.Logged = false;
                    $cookies.remove("Account");
                });
        };
        return obj;
    }
})();