﻿(function () {
    angular.module('Registros', []).factory("RegisterService", ['$rootScope', '$http', function ($rootScope, $http) {
        var self = this;
      

        var registerClient = function (data) {
            var string = $.param(data);
            var func = $http.post('api/Registro/AddR', data);
            func.success(function (result) {
                console.log(result);
            });
            return func;
        };

        return {
            registerClient: registerClient
        };
    }]);
})();