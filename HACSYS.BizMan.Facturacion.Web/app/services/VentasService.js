﻿(function () {
    angular.module("Ventas",['SignalR']).factory("VentasService", ['$rootScope','Hub','$http',function ($rootScope,Hub,$http) {
        var service = this;
        var hub = new Hub('FacturacionHub', {
            listeners: {
                'OnVentaSearchResult': function (venta) {
                    $rootScope.$emit("VentaSearchResult", venta)
                    if (venta === null) {
                        console.log("Venta no encontrado");
                    } else {
                        console.log("Venta Recibido");
                    }
                }
            },
            methods: ['GetVentaServidor', 'RemoveDespachoOcupado', 'FacturaClear'],
            //rootPath: '/FacturacionWeb/signalr',
            errorHandler: function (error) {    
                console.error(error);
            }
        });

        var GuardaReferencia = function (data) {
            var func = $http.post('api/Ticket/GuardaReferencia', data);
            func.success(function (result) {
                console.log(result);
            });
            return func;
        };


        var GetVenta = function (empresaID, folio, monto) {
            console.log(empresaID);
            return hub.GetVentaServidor(empresaID, folio, monto).then(function (result) {
               

                if (result.ResultCode == 0)
                {
                    result.message = "El venta se encuentra en proceso de facturación.";
                    result.error = true;
                }
                else if (result.ResultCode == 1)
                {
                    result.message = "Datos Inválidos";
                    result.error = true;
                }
                else if (result.ResultCode == 2) {
                    result.message = "Buscando...";
                }
                else if (result.ResultCode == 3)
                    result.message = "Se recibió la información con éxito.";
                else if (result.ResultCode == 4)
                {
                    result.message = "El ticket no se encuentra en la estación seleccionada.";
                    result.error = true;
                }
                else if (result.ResultCode == 6) {
                    result.message = "El ticket expiró.";
                    result.error = true;
                }

                 return result;
            });
        };


        var RemoveVenta = function (ventas) {
            console.log(ventas);
            return hub.RemoveDespachoOcupado(ventas).then(function (result) {


                if (result.response == 200) {
                    result.message = "Se retiró el ticket con éxito.";
                    $rootScope.ventasRefresh(result.Ventas);
                }
                else if (result.response == 500) {
                    result.message = "No se pudo retirar el ticket, por favor presiona 'F5'";
                    result.error = true;
                }

                return result;
            });
        };

        var ClearFacturaCache = function () {
            return hub.FacturaClear().then(function () {
                
            });
        };
        
        return {
            GetVenta: GetVenta,
            GuardaReferencia: GuardaReferencia,
            RemoveVenta: RemoveVenta,
            ClearFacturaCache: ClearFacturaCache
        };
    }]);
})();