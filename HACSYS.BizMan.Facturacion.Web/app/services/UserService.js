﻿(function () {
    var userService = angular.module('UserService', []);
    userService.factory('UserService', ['$http','$rootScope', function ($http,$rootScope) {
        var obj = {};
        var result = {};

        obj.DataResult = function () {
            alert(result);
        }

        //TODO: PROBAR LA NUEVA ESTRUCTURA
        obj.GetUDetails = function (data) {
            var func = $http.post('api/Cliente/GetClientDetails', data);
            func.success(function (datos) {
                var direccion = {};

                $rootScope.alertSite(datos.DatosFiscales, datos.DatosFiscales.Direccion, datos.Direcciones);
                console.log(datos);
            }), func.error(function (datos) {
              console.log(datos);
            });

            return func;
        };

        obj.AddDireccion = function (data, data2, data3) {
            var func = $http.post('api/Cliente/updateDireccion', { direccion: data, userID: data2, pais: data3 });
            func.success(function (datos) {
                console.log(datos);
                if (datos === null)
                { alert("Hubo un problema al agregar la dirección"); }
                else {
                    alert("Direccion Agregada");
                    $location.path('/User/Datos');
                    $location.replace();
                }
            });
        };

        obj.AddMail = function (data) {
            var func = $http.post('api/Cliente/AddMail', data);
            func.success(function (datos) {
                alert("Correo agregado exitosamente")
                console.log(datos);
            }), func.error(function (datos) {
                console.log(datos);
            });

            return func;
        };


        // REMOVER ESTO
        obj.GetDirecciones = function (data) {
            var func = $http.post('api/Cliente/getClienteDirecciones', data);
            func.success(function (datos) {
                $rootScope.alertDirecciones(datos);
            }), func.error(function (datos) {
                console.log(datos);
            });

            return func;
        };
        
        // REMOVER ESTO TAMBIÉN
        obj.CambiarDireccion = function (id,data1, data2) {
            var func = $http.post('api/Cliente/changeDireccion', { ClienteID: id, direccionNueva:data1, direccion: data2 });
            func.success(function (datos) {
                obj.GetUDetails($rootScope.UserID);
            }), func.error(function (datos) {
                console.log(datos);
            });
        }


        return obj;
    }]);
})();