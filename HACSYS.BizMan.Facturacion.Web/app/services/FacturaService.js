﻿(function () {
    angular.module("Facturas", []).factory("FacturasService", ['$rootScope','$http', function ($rootScope,$http) {

        var self = this;
        
        var obj = {}


        obj.Facturar = function (data) {
            var func = $http.post('api/Factura/Facturar', data);
            func.success(function (result) {
                console.log(result);
            });
            return func;
        };

        obj.FacturarCliente = function (data) {
             return $http.post('api/Factura/FacturarCliente', data);
        };

        obj.Get = function (data) {
            return func = $http.get('api/Factura');
        };

        obj.GetFechas = function (start, end) {
            return func = $http.get('api/Factura/'+start+'/'+end);
        }
        return obj;
    }]);
})();