﻿(function(){
	'use strict';
	angular.module("Empresa", []).factory("EmpresasService", ["$http", function ($http) {
	    var obj = {};
	    obj.getAll = function(){
	        return $http.get('api/Empresas/').then(function(data)
	        {
	            return data.data;
	        })
	    };

	    obj.get = function (numEstacion) {
	        return $http.get('api/Empresas/' + numEstacion).then(function (data) {
	            return data.data;
	        });
	    };

	    return obj;
	}]);
})();