﻿(function () {
    var datosFiscalesService = angular.module('DatosFiscalesService', ['ngCookies','utils']);
    datosFiscalesService.factory('DatosFiscalesService', ['$http', '$cookies', 'PromiseFactory', function ($http, $cookies, PromiseFactory) {
        var obj = {};
        var result = {};

        obj.DataResult = function () {
            alert(result);
        }

        obj.DatosFiscales = function () {
            var usr = $cookies.getObject('Account');
            if (usr != undefined && usr.DatosFiscales != undefined)
            {
                var defer = PromiseFactory.defer();
                defer.resolve(usr.DatosFiscales);
                return defer.promise;
            }
            return $http.get('api/Cliente/DatosFiscales')
            .success(function (data) {
                if(usr != undefined)
                {
                    usr.DatosFiscales = data;
                    $cookies.putObject('Account', usr);
                }
            });
        };

        return obj;
    }]);
})();