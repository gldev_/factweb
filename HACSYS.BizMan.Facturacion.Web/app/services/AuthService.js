﻿(function () {
    var auth = angular.module('Auth', ['ngCookies', 'utils']);
    auth.factory('AuthService', ['$http', '$location', '$rootScope', '$cookies', 'PromiseFactory', function ($http, $location, $rootScope, $cookies, PromiseFactory) {
        var CookieName = 'Account'
        var obj = {};
        obj.User = {};
        obj.Logged = false;

        obj.eatCookie = function(name) {
            document.cookie = [name, '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/; domain=.', window.location.host.toString()].join('');
        }

        obj.GetUser = function () {
            var usr = $cookies.getObject(CookieName)
            if (usr != undefined)
            {
                obj.User = usr;
                obj.Logged = true;
                var defer = PromiseFactory.defer();
                defer.resolve(usr);
                return defer.promise;
            }

            var func = $http.get('api/Account/UserDetails')
            .success(function (data) {
                obj.User = data;
                if (obj.User.UserName != null) {
                    obj.Logged = true
                    $cookies.putObject(CookieName, data);

                }
                else {
                    obj.Logged = false;
                }
            });
            return func;
        };
        return obj;
    }]);

    auth.controller('hcAuthController', ['AuthService', '$scope', '$element', function (AuthService, $scope, $element) {
        var self = this;
        self.Auth = AuthService;
        self.AllowedRoles = $scope.hcAuth;
        if ($scope.not)
            self.neg = $scope.not;
        else
            self.neg = false;
        $scope.$watch(
            function () {
                return self.Auth;
            },
            function () {
                self.Check();
            },
            true
        );

        self.Check = function () {
            if (!self.AllowedRoles && self.Auth.Logged) {
                if (self.neg)
                    $element.addClass('ng-hide');
                else
                    $element.removeClass('ng-hide');
            }
            else if (self.Auth.Logged && _.intersection(self.AllowedRoles, self.Auth.User.Roles).length > 0) {
                if (self.neg)
                    $element.addClass('ng-hide');
                else
                    $element.removeClass('ng-hide');
            }
            else {
                if (self.neg)
                    $element.removeClass('ng-hide');
                else
                    $element.addClass('ng-hide');
            }
        };

        self.Check();

    }]);
    auth.directive('hcAuth', ['AuthService', function (AuthService) {
        return {
            restric: 'A',
            scope: {
                hcAuth: '=',
                not: '='
            },
            controller: 'hcAuthController'
        }
    }])
})();