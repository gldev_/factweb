﻿(function () {
    angular.module('Direcciones', ['ngCookies'])
        .factory('DireccionesService', ['$http','$cookies', function ($http,$cookies) {
        var obj = {};

        obj.GetPaises = function (data) {

            //var general = $cookies.getObject('General')
            //if (general != undefined && general.Paises != undefined) {
            //    var defer = PromiseFactory.defer();
            //    defer.resolve(general.Paises);
            //    return defer.promise;
            //}

            var func = $http.get('api/Direcciones/Paises', { cache: true });
            //.success(function (data) {
            //    if (general == undefined)
            //        general = {};
            //    general.Paises = data;
            //    $cookies.putObject('General', general);
            //});
            return func;
        };

        return obj;
    }]);
})();