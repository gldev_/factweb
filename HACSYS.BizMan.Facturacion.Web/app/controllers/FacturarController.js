﻿var ResponseCode = { BUSY: 0, INVALID_DATA: 1, SEARCHING: 2, SUCCESS: 3, NOT_FOUND: 4, VENTA_USED: 5, DATE_EXPIRED: 6, UNAVAILABLE:7 };
var FacturarResultCode = { SUCCESS: 0, ERROR: 1, INVALID_DATA: 2, SERVER_ERROR: 3, SAT_ERROR: 4, PAC_ERROR: 5 };
(function () {
    var home = angular.module('Facturar', ['DatosFiscalesService', 'Ventas', 'Empresa', 'Facturas', 'datatables', 'Direcciones', 'ui.bootstrap', 'ui.router', 'Auth', 'General']);

    home.controller('FacturarController', ['$scope', '$rootScope', '$state', '$modal', 'VentasService', 'EmpresasService', 'FacturasService', 'DireccionesService', 'DatosFiscalesService', '$timeout', 'AuthService', '$anchorScroll',
                                  function ($scope, $rootScope, $state, $modal, VentasService, EmpresasService, FacturasService, DireccionesService, DatosFiscalesService, $timeout, AuthService, $anchorScroll) {

        var self = this;
        self.data = {};
        self.ventas = [];
        self.empresas = [];
        self.Paises = [];
        self.PaisSeleccionado = {};
        self.Buscando = false;
        self.ErrorBusqueda = { Mostrar: false };
        self.ResultadoVentas = [];
        self.UserDatosFiscales = {};
        self.Correo;
        self.timer;
        self.FacturaErrorMessage;
        self.Auth = AuthService;
        self.PaisNombre;
        self.ObjetoCookie = "";
        self.tmpVenta = {};
        self.MetodoDePago = 0;
        self.ReferenciaDePago;
        self.ErrorFacturar = false;

        // Obtener la lista de paises
        DireccionesService.GetPaises().success(function (data) {
            $.each(data, function () {
                self.Paises.push(this);
                if(AuthService.Logged && self.Direccion.Estado != undefined){
                    $.each(self.Paises, function () {
                        if (this.PaisID == self.Direccion.Estado.PaisID) {
                            self.PaisNombre = this.Nombre;
                        }
                    });
                }
            });
        });
        self.FacturaID = 0;

        // Define included objects
        self.Direccion = {
            Calle: "",
            CodigoPostal: "",
            Colonia: "",
            Localidad: "",
            NumExterior: "",
            NumInterior: "",
            Referencia: ""
        };

        //$rootScope.DatosUsuario = self.read("DatosLocales");

        self.Cliente = {
            DatosFiscales: {
                RFC: "",
                RazonSocial: "",
                Direccion: self.Direccion
            }
        };
        
        if (AuthService.Logged)
        {
            DatosFiscalesService.DatosFiscales()
                .success(function (data) {
                    self.Cliente.DatosFiscales.RFC = data.RFC;
                    self.Cliente.DatosFiscales.RazonSocial = data.RazonSocial;
                    self.Direccion = data.Direccion;
                    var idPais = data.Direccion.Estado.PaisID;
                    $.each(self.Paises, function () {
                        if (this.PaisID == idPais) {
                            self.PaisNombre = this.Nombre;
                        }
                    });
                });
        }

        self.CloseAlert = function(index)
        {
            self.ResultadoVentas.splice(index, 1);
        }

        // Funciones para las formas
        self.OnFocus = function (obj) {
            $(obj.target).closest(".textbox-wrap").addClass("focused");
        }

        self.OnBlur = function (obj) {
            $(obj.target).closest(".textbox-wrap").removeClass("focused");
            var numInterior = $("#numInteriorInput").val();

            if(isNaN(numInterior))
            {
             console.log("NumInterior INVALIDA");   
            }
        
            var Monto = $("#montoInput").val();

            if(isNaN(Monto))
            {
             console.log("Monto INVALIDA");   
            }
        
        }


        self.RemoveVenta = function (index)
        {
            // TODO: Agregar el método para remover la venta de la lista de espera del hub
            VentasService.RemoveVenta(self.ventas[index]).then(function (data) {
                console.log(data);
            });
            self.ventas.splice(index, 1);
        }

        self.Factura = {
            Cliente: self.Cliente,
            Ventas: self.ventas,
            Correo: self.Correo
        };


        EmpresasService.getAll().then(function (data) {
            console.log(data);
            self.empresas = data;
        });

        self.Facturar = function () {
            self.ErrorFacturar = false;
            if (self.ventas[0].MetodoDePago.Metodo === "Tarjeta") {
                //TODO: Validación de los campos
                $('#ref_venta').modal('toggle');
                
            } else {
                var modalInstance = $modal.open({
                    templateUrl: 'ModalContent.html'
                });
                if (AuthService.Logged) {
                    VentasService.ClearFacturaCache();
                    FacturasService.FacturarCliente(self.ventas).success(function (result) {
                        if (result.ResultCode == FacturarResultCode.SUCCESS) {
                            modalInstance.close();
                            self.FacturaID = result.FacturaID;
                            self.ventas = [];
                            $state.go('resultado', { FacturaID: self.FacturaID });
                        }
                        else {
                            VentasService.ClearFacturaCache();
                            self.FacturaErrorMessage = result.Message;
                            modalInstance.close();
                            self.ErrorFacturar = true;
                        }
                    });
                } else {
                    VentasService.ClearFacturaCache();
                    FacturasService.Facturar(self.Factura).success(function (result) {
                        if (result.ResultCode == FacturarResultCode.SUCCESS) {
                            modalInstance.close();
                            self.ventas = [];
                            self.FacturaID = result.FacturaID;
                            $state.go('resultado', { FacturaID: self.FacturaID });
                        }
                        else {
                            self.FacturaErrorMessage = result.Message;
                            modalInstance.close();
                            self.ErrorFacturar = true;
                        }
                    });
                }
            }
        }
        self.CloseFacturaAlert = function(){
            $('#FacturaErrorAlert').hide();
        }


        self.Open = function (id) {
            $("#detalle" + id).toggle("blind");
        }

        // Se agregó Toggle por que no reaccionaba al cambio de ng-hide
        $rootScope.$on("VentaSearchResult", function (event, result) {
            $scope.$apply(function () {
                if (result.ResultCode != ResponseCode.SEARCHING) {
                    self.Buscando = false;
                    $timeout.cancel(self.timer);
                }
                CheckResult(result);
            });
        });

        function CheckResult(result) {
            var alerta = {};
            alerta.text = result.Message;
            // Si el resultado fue de error, mostrar el mensaje en rojo, de lo contrario mostrar mensaje de venta exitoso
            if (result.ResultCode == ResponseCode.NOT_FOUND)
            {
                alerta.text = "Venta no encontrado";
                alerta.Mostrar = true;
                self.ErrorBusqueda = alerta;
            }
            else if (result.ResultCode == ResponseCode.INVALID_DATA) {
                alerta.text = result.Message;
                alerta.Mostrar = true;
                self.ErrorBusqueda = alerta;
            }
            else if (result.ResultCode == ResponseCode.SEARCHING)
            {
                self.Buscando = true;
                self.ErrorBusqueda.Mostrar = false;
            }
            else if (result.ResultCode == ResponseCode.SUCCESS) {
                alerta.text = "Venta agregado con éxito";
                alerta.type = 'success';
                self.ErrorBusqueda.Mostrar = false;
                //TODO: Validar que la venta no esté en la lista de ventas local
                //if (self.ventas.length > 0) {
                //    for (var i = 0; i < self.ventas.length; i++) {
                //        for (var j = i; j <= self.ventas.length; j++) {
                //            if (self.ventas[i].Folio === self.ventas[j+1].Folio) {
                //                alert("El ticket se encuentra en la lista.");
                //            } else { self.ventas.push(result.Venta); }
                //        }
                //    }
                //} else { self.ventas.push(result.Venta); }
                self.ventas.push(result.Venta);
                self.ResultadoVentas.push(alerta);
                

            }
            else if (result.ResultCode == ResponseCode.VENTA_USED) {
                alerta.text = "Esta venta ya ha sido facturada";
                alerta.Mostrar = true;
                self.ErrorBusqueda = alerta;
            }
            else if (result.ResultCode == ResponseCode.BUSY) {
                alerta.text = result.Message;
                alerta.Mostrar = true;
                self.ErrorBusqueda = alerta;
            } else if (result.ResultCode == ResponseCode.DATE_EXPIRED) {
                alerta.text = "Ticket expirado";
                alerta.Mostrar = true;
                self.ErrorBusqueda = alerta;
            } else if (result.ResultCode == ResponseCode.UNAVAILABLE) {
                alerta.text = "Servicio no disponible, favor de intentar más tarde";
                alerta.Mostrar = true;
                self.ErrorBusqueda = alerta;
            }
        }

        $rootScope.Deactivate = function () {
            $scope.$apply(function () {
                self.Buscando = false;
            });
            
        }
                
        self.validaMetodoDePago = function ()
        {
          
                //TODO: guardar referencia con ajax y validar que no haya más ventas de otros tipos de pago
                
                obj = { Referencia: self.ReferenciaDePago, ReferenciaDePagoID: 0 };
                for(var i = 0; i < self.ventas.length; i++)
                {
                    self.ventas[i].ReferenciaDePago = obj;
                }
                $('#ref_venta').modal('toggle');
                var modalInstance = $modal.open({
                    templateUrl: 'ModalContent.html'
                });
                if (AuthService.Logged) {
                    VentasService.ClearFacturaCache();
                    FacturasService.FacturarCliente(self.ventas).success(function (result) {
                        if (result.ResultCode == FacturarResultCode.SUCCESS) {
                            modalInstance.close();
                            self.ventas = [];
                            self.FacturaID = result.FacturaID;
                            $state.go('resultado', { FacturaID: self.FacturaID });
                        }
                        else {
                            self.FacturaErrorMessage = result.message;
                            modalInstance.close();
                            $('#FacturaErrorAlert').alert();
                        }
                    });
                } else {
                    VentasService.ClearFacturaCache();
                    FacturasService.Facturar(self.Factura).success(function (result) {
                        if (result.ResultCode == FacturarResultCode.SUCCESS) {
                            modalInstance.close();
                            self.ventas = [];
                            self.FacturaID = result.FacturaID;
                            $state.go('resultado', { FacturaID: self.FacturaID });
                        }
                        else {
                            self.FacturaErrorMessage = result.message;
                            modalInstance.close();
                            $('#FacturaErrorAlert').alert();
                        }
                    });
                }
           
        }


        self.BuscarVenta = function () {
            self.ErrorBusqueda.Mostrar = false;
            self.Buscando = true;

            self.timer = $timeout(function () {
                var alerta = {};
                alerta.text = "Error de conexión, favor de intentar más tarde";
                alerta.Mostrar = true;
                self.ErrorBusqueda = alerta;
                    self.Buscando = false;
            }, 40000);
            VentasService.GetVenta(self.data.empresa.EmpresaID, self.data.folio, self.data.monto).then(function (result) {
                $scope.$apply(function () {
                    if (result.ResultCode != ResponseCode.SEARCHING)
                    {
                        self.Buscando = false;
                        $timeout.cancel(self.timer);
                    }
                    CheckResult(result);
                });
            });
        };
     
        
        
        $("#numInteriorInput").keyup(function(event){
            
            var numInterior = $("#numInteriorInput").val();

            if(isNaN(numInterior))
            {
             console.log("NumInterior INVALIDA");   
            }
            
        });
        
        $("#numExteriorInput").keyup(function(event){
            
            var numExterior = $("#numExteriorInput").val();

            if(isNaN(numExterior))
            {
             console.log("numExterior INVALIDA");   
            }
            
        });

        $("#codigoPostalInput").keyup(function(event){
            
            var codigoPostal = $("#codigoPostalInput").val();

            if(isNaN(codigoPostal))
            {
             console.log("codigoPostal INVALIDA");   
            }
            
        });
        
        $rootScope.ventasRefresh = function(ventas)        {
            self.ventas = ventas;
        }

        
    }]);
})();