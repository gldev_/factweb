﻿(function () {
    var app = angular.module('app');
    app.controller('NavbarController', ['AccountService', 'AuthService', '$state', function (AccountService, AuthService,  $state) {
        var self = this;
        self.AccountService = AccountService;
        self.Auth = AuthService;
        self.Logoff = function()
        {
            self.AccountService.Logoff().success(function () { $state.go('login'); });
            
        };
    }]);
})();