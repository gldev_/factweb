﻿(function () {
    var account = angular.module('Registro', ['Registros', 'Facturar', 'Account', 'General']);
    account.controller('RegisterController', ['$scope', 'AccountService', '$location','$rootScope', function ($scope, AccountService,$location,$rootScope) {
        var self = this;
        self.RegisterForm;
        self.AccountForm;
        self.Cliente = {};
        self.Direccion = {};
        self.Account = {};
        //self.Account.Email = "";
        //self.Account.Password = "";
        //self.Account.ConfirmPassword = "";
        self.Loading = false;
        self.PasswordErrors = {};
        self.Errors = [];

        self.PasswordValidation = function (password) {
            if (password == undefined || password == "")
                return;
            var hasUpperCase = /[A-Z]/.test(password);
            var hasLowerCase = /[a-z]/.test(password);
            var hasNumbers = /\d/.test(password);
            var hasNonalphas = /\W/.test(password);
            self.PasswordErrors.UpperCase = hasUpperCase ? "" : "El password debe de contener mayúsculas";
            self.PasswordErrors.LoweCase =  hasLowerCase ? "" : "El password debe de contener minúsculas";
            self.PasswordErrors.HasNumbers = hasNumbers ? "" : "El password debe de contener por lo menos un número";
            var arr = [];
            $.each(self.PasswordErrors, function (key,value) {
                if(value)
                    arr.push(value);
            });
            self.Errors = arr;
            return hasUpperCase && hasLowerCase && hasNumbers;
        }

        self.OnSubmit = function () {
            if (self.RegisterForm.$valid)
            {
                self.Loading = true;
                var vm = {
                    DatosFiscales: { RFC: self.Cliente.RFC.toUpperCase(), RazonSocial: self.Cliente.RazonSocial, Direccion: self.Direccion },
                    Account: self.Account
                    };
                AccountService.Register(vm)
                .success(function (result) {
                    self.Loading = false;
                    if(result.StatusCode == 200)
                    {
                        $location.path('/Verificar');
                    } else {
                        self.Errors = result.Errores;
                    }
                })
                .error(function (result) {
                    self.Loading = false;
                })
            }
        }

    }]);
})();