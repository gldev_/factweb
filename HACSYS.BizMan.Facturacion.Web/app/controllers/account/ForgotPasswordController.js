﻿(function () {
    angular.module('Account')
    .controller('ForgotPasswordController', [ 'AccountService', '$location', '$state', function (AccountService, $location, $state) {
        var self = this;
        self.Correo
        self.Error = false;
        self.ErrorText = "";
        self.RecoveryForm;
        self.Waiting = false;
        self.Enviar = function()
        {
            self.Waiting = true;
            AccountService.ForgotPassword(self.Correo)
            .success(function (result) {
                self.Waiting = false;
                if (result.StatusCode == 200) {
                    $state.go("forgotPasswordSuccess");
                }
                else {
                    self.Error = true;
                    self.ErrorText = "Error, no se pudo encontrar una cuenta con dicho correo."
                }
            })
            .error(function () {
                self.Waiting = false;
                self.Error = true;
                self.ErrorText = "Error cambiando correo";
            });
        }
    }]);
})();