﻿(function () {
    var account = angular.module('Account')
        .controller('ResetPasswordController', ['AccountService', '$location', '$stateParams', '$state', function (AccountService, $location, $stateParams, $state) {
        var self = this;
        self.Correo = "";
        self.Password = "";
        self.ConfirmPassword = "";
        self.Token = $stateParams.Token;
        self.Error = false;
        self.ErrorText = "";
        self.Waiting = false;
        self.Form;
        self.Errors = [];
        self.PasswordErrors = {};
        self.PasswordValidation = function (password) {
            if (password == undefined || password == "")
                return false;
            var hasUpperCase = /[A-Z]/.test(password);
            var hasLowerCase = /[a-z]/.test(password);
            var hasNumbers = /\d/.test(password);
            var hasNonalphas = /\W/.test(password);
            self.PasswordErrors.UpperCase = hasUpperCase ? "" : "El password debe de contener mayúsculas";
            self.PasswordErrors.LoweCase = hasLowerCase ? "" : "El password debe de contener minúsculas";
            self.PasswordErrors.HasNumbers = hasNumbers ? "" : "El password debe de contener por lo menos un número";
            var arr = [];
            $.each(self.PasswordErrors, function (key, value) {
                if (value)
                    arr.push(value);
            });
            self.Errors = arr;
            return hasUpperCase && hasLowerCase && hasNumbers;
        }

        self.Enviar = function () {
            var data = { Email: self.Correo, Password: self.Password, ConfirmPassword: self.ConfirmPassword, Code: self.Token };
            self.Waiting = true;
            AccountService.ResetPassword(data)
            .success(function (result) {
                self.Waiting = false;
                if (result.StatusCode == 200) {
                    $state.go('resetPasswordSuccess')
                } else {
                    self.Error = true;
                    self.ErrorText = "Error, No se pudo completar el cambio de contraseña";
                }
            })
            .error(function () {
                self.Waiting = false;
                self.Error = true;
                self.ErrorText = "Error, No se pudo completar el cambio de contraseña";
            });
        }
    }]);
})();