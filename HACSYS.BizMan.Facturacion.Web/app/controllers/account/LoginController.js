﻿(function () {
    var account = angular.module('Account');

    account.controller('LoginController', ['$cookies','AccountService', '$location', '$scope', '$state', function ($cookies,AccountService, $location, $scope, $state) {
        var self = this;
        self.Errors = [];
        self.Waiting = false;
        self.OnFocus = function(obj)
        {
            $(obj.target).closest(".textbox-wrap").addClass("focused");
        }

        self.OnBlur = function(obj)
        {
            $(obj.target).closest(".textbox-wrap").removeClass("focused");
        }

        self.loginCredentials = {
            RememberMe : false
        };

        self.Login = function () {
            self.Errors = [];
            self.Waiting = true;
            AccountService.Login(self.loginCredentials)
                .success(function (data) {
                    self.Waiting = false;
                    if(data.StatusCode == 200)
                    {
                        self.Errors = [];
                        $state.go("listadoFacturas");
                    }
                    if(data.StatusCode == 401)
                    {
                        self.Errors.push("Credenciales Invalidas");
                    }
                })
                .error(function (data) {
                    self.Waiting = false;
                    console.log(data);
                });
        }
    }]);
})();