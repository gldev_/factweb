﻿(function () {
    var account = angular.module('Account')
        .controller('ChangePasswordController', ['AccountService', '$location', '$stateParams', '$state', function (AccountService, $location, $stateParams, $state) {
            var self = this;
            self.OldPassword = "";
            self.NewPassword = "";
            self.ConfirmPassword = "";
            self.Error = false;
            self.ErrorText = "";
            self.Success = false;
            self.SuccessText = "";
            self.Waiting = false;
            self.Form;
            self.Errors = [];
            self.PasswordErrors = {};
            self.PasswordValidation = function (password) {
                if (password == undefined || password == "")
                    return false;
                var hasUpperCase = /[A-Z]/.test(password);
                var hasLowerCase = /[a-z]/.test(password);
                var hasNumbers = /\d/.test(password);
                var hasNonalphas = /\W/.test(password);
                self.PasswordErrors.UpperCase = hasUpperCase ? "" : "La contraseña debe de contener mayúsculas";
                self.PasswordErrors.LoweCase = hasLowerCase ? "" : "La contraseña debe de contener minúsculas";
                self.PasswordErrors.HasNumbers = hasNumbers ? "" : "La contraseña debe de contener por lo menos un número";
                var arr = [];
                $.each(self.PasswordErrors, function (key, value) {
                    if (value)
                        arr.push(value);
                });
                self.Errors = arr;
                return hasUpperCase && hasLowerCase && hasNumbers;
            }

            self.Enviar = function () {
                var data = { OldPassword: self.OldPassword, NewPassword: self.NewPassword, ConfirmPassword: self.ConfirmPassword};
                self.Waiting = true;
                AccountService.ChangePassword(data)
                .success(function (result) {
                    self.Waiting = false;
                    if (result.StatusCode == 200) {
                        self.Success = true;
                        self.SuccessText = "El cambio de contraseña fue realizado con éxito";
                        self.OldPassword = self.ConfirmPassword = self.NewPassword = "";
                    } else {
                        self.Error = true;
                        self.ErrorText = "Error, No se pudo completar el cambio de contraseña";
                    }
                })
                .error(function () {
                    self.Waiting = false;
                    self.Error = true;
                    self.ErrorText = "Error, No se pudo completar el cambio de contraseña";
                });
            }
        }]);
})();