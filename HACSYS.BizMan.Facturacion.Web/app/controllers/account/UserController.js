﻿(function () {
    var user = angular.module('Usuario', ['UserService']);
    user.controller('UserController', ['$cookies', '$rootScope', 'UserService', '$scope', 'DireccionesService', function ($cookies, $rootScope, UserService, $scope, DireccionesService) {
        var self = this;
        self.Direccion = {
        };
        self.newMail = "";
        self.Direcciones = [];
        self.flag = 0;
        self.userData = {};
        self.UserName = $rootScope.UserName;
        self.uid = $cookies.getObject("Account");
        UserService.GetUDetails(self.uid.userID);
        self.Paises = [];
        DireccionesService.GetPaises().success(function (data) {
            $.each(data, function () {
                self.Paises.push(this);
            });

        });
        
        self.AddMail = function () {
            data = { ClientID: self.uid.userID, Mail: self.newMail, UMail: self.uid.UserName };
            UserService.AddMail(data);
        }

        $rootScope.alertSite = function (datosUsuario, datosDireccion, direccionArray) {
            $rootScope.Fiscales = datosUsuario;
            self.userData = datosUsuario;
            self.userData.Direccion = datosDireccion;
            self.Direccion = self.userData.Direccion;
            //self.Direcciones = direccionArray;
            //$state.reload();

            //console.log(self.userData.Direcciones.Calle);
        };

        self.OnSubmit = function (data) {

            if (self.RegisterForm.$valid === true) {
                self.Direccion.Pais;

                UserService.AddDireccion(self.Direccion, self.uid.userID, self.Direccion.Pais);
            } else {
                alert("El formulario contiene datos inválidos")
            }

        }

        self.cambiaDireccion = function () {
            var tmp = $( "#DireccionActual option:selected" ).text();
            var newArray = {};
            $.each(self.Direcciones, function () {
                if (this.Calle == tmp) {
                    newArray = this;
                }
            });

            UserService.CambiarDireccion($rootScope.UserID, self.userData.Direcciones, newArray);
        }

        $("#menu1").click(function(){


            $("#DatosFacturacion").show("Blind");
            $("#FacturasPendientes").hide("Blind");

        });

        

        
        $("#menu2").click(function(){

            $("#DatosFacturacion").hide("Blind");
            $("#FacturasPendientes").show("Blind");
            
        });

    }]);

})();