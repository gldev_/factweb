﻿(function () {
    var account = angular.module('Account', ['Auth','ui.router','ngCookies','utils']);
    account.controller('AccountController', ['$scope', 'AccountService', function ($scope, AccountService) {
//TODO: Agregar un mensaje que indique al usuario que el password debe contener lo siguiente:
//      Una letra mayúscula, una letra minúscula, números y un dígito que no sea letra (|,+,],etc...)
        self = this;
        self.usr = {};

        self.register = function (usr) {
            console.log(usr);
            var resultado = self.checkPassword(usr);
            if (resultado) {
                AccountService.Register(usr);
            }
        };

        self.checkPassword = function(usr)
        {

            //TODO: Cambiar la función a como comparaba antes los registros
            if (usr.RePassword != usr.Password) {
                console.log("Los passwords no coinciden");
                return false;
            } else
                return true;
        };

    }]);

    account.directive('hcCheckbox', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    increaseArea: '20%' // optional
                });
            }
        }
    });
})();