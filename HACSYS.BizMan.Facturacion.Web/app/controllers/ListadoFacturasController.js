﻿(function(){
    angular.module('Facturar').controller('ListadoFacturasController', ['$q', 'DTOptionsBuilder', 'FacturasService', function ($q, DTOptionsBuilder, FacturasService) {
        var self = this;
        self.Facturas = [];

        FacturasService.Get()
        .success(function(data)
        {
            self.Facturas = data;
        });
		self.dtOptions = DTOptionsBuilder.newOptions().withLanguage(
            {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        ).withOption("responsive", true);
	    
		var optionsMoment = {
		    startDate: moment().startOf('month'),
		    endDate: moment(),
		    minDate: '01/01/2013',
		    maxDate: Date.now().toString,
		    dateLimit: { days: 60 },
		    showDropdowns: true,
		    showWeekNumbers: false,
		    timePicker: false,
		    timePickerIncrement: 1,
		    timePicker12Hour: true,
		    ranges: {
		        'Ayer': [moment().subtract('days', 1), moment().subtract('days', 1)],
		        'Semana Actual': [moment().startOf('week'), moment().endOf('week')],
		        'Semana Anterior': [moment().subtract('week', 1).startOf('week'), moment().subtract('week', 1).endOf('week')],
		        'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
		        'Mes Anterior': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		    },
		    opens: 'right',
		    buttonClasses: ['btn btn-default'],
		    applyClass: 'btn-sm btn-success',
		    cancelClass: 'btn-sm pull-right',
		    format: 'DD/MM/YYYY',
		    separator: ' a ',
		    locale: {
		        applyLabel: 'Aceptar',
		        cancelLabel: 'Cancelar',
		        fromLabel: 'Desde',
		        toLabel: 'a',
		        customRangeLabel: 'Rango de Fechas',
		        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		        firstDay: 1
		    }
		};
		$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
		var cb = function (start, end, label) {
		    FacturasService.GetFechas(start.format('YYYY-MMM-DD'), end.format('YYYY-MMM-DD'))
            .success(function (data) {
                self.Facturas = data;
            })
            .error(function (error) {
            });
		    $('#reportrange span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
		}

		$('#reportrange').daterangepicker(optionsMoment, cb);
		$('#reportrange').on('apply.daterangepicker', function (ev, picker) {

		});
	}]);
})();