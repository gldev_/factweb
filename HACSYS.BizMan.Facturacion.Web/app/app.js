﻿(function () {

    var app = angular.module('app', ['Auth', 'Home', 'Account', 'ngAnimate', 'Facturar', 'Empresa', 'Registro', 'Usuario','DatosFiscalesService','ui.router']);

    app.directive('hcNavbar', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/views/shared/hc-navbar.html'
        };
    })

    app.config(['$stateProvider','$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/Login");

        $stateProvider
        .state('login', {
            url: '/Login',
            controller: 'LoginController',
            controllerAs: 'LoginCtrl',
            templateUrl: 'app/views/LoginView.html'
         })
        .state('registro', {
            url: '/Registro',
            controller: 'RegisterController',
            controllerAs: 'RegisterCtrl',
            templateUrl: 'app/views/RegisterView.html'
        })
        .state('verificar', {
            url: '/Verificar',
            templateUrl: 'app/views/Account/VerificarCorreo.html'
        })
        .state('confirmacion', {
            url: '/Confirmacion',
            templateUrl: 'app/views/Account/ConfirmacionCuenta.html'
        })
        .state('facturar', {
            url: '/Facturar',
            controller: 'FacturarController',
            controllerAs: 'FacturarCtrl',
            templateUrl: 'app/views/FacturarView.html'
        })
        .state('resultado', {
            url: '/Resultado/:FacturaID',
            templateUrl: 'app/views/facturar/facturar-resultado.html',
            controller: function ($scope, $stateParams) {
                $scope.id = $stateParams.FacturaID;
            }
        })
        .state('listadoFacturas', {
            url: '/ListadoFacturas',
            controller: 'ListadoFacturasController',
            controllerAs: 'ListadoFacturasCtrl',
            templateUrl: 'app/views/ListadoFacturas.html'
        })
        .state('user', {
            url: '/User',
            controller: 'UserController',
            controllerAs: 'UserCtrl',
            templateUrl: 'app/views/UserPage.html'
        })
        .state('user.datos', {
            url: '/Datos',
            controller: 'UserController',
            controllerAs: 'UserCtrl',
            templateUrl: 'app/views/Nested/User/nested-datos.html'
        })
        .state('user.password', {
            url: '/Password',
            controller: 'ChangePasswordController',
            controllerAs: 'ChangePasswordCtrl',
            templateUrl: 'app/views/Nested/User/nested-password.html'
        })
        .state('user.facturas', {
            url: '/Facturas',
            controller: 'UserController',
            controllerAs: 'UserCtrl',
            templateUrl: 'app/views/Nested/User/nested-facturas.html'
        })
        .state('user.direccion', {
            url: '/Direccion',
            controller: 'UserController',
            controllerAs: 'UserCtrl',
            templateUrl: 'app/views/Nested/User/nested-addDirection.html'
        })
        .state('forgotPassword', {
            url: '/ForgotPassword',
            controller: 'ForgotPasswordController',
            controllerAs: 'ForgotPasswordCtrl',
            templateUrl: 'app/views/Account/ForgotPassword.html'
        })
        .state('forgotPasswordSuccess', {
            url: '/ForgotPasswordSuccess',
            templateUrl: 'app/views/Account/ForgotPasswordSuccess.html'
        })
        .state('resetPassword', {
            url: '/ResetPassword/:Id/{Token:.*}',
            controller: 'ResetPasswordController',
            controllerAs: 'ResetPasswordCtrl',
            templateUrl: 'app/views/Account/ResetPassword.html'
        })
        .state('resetPasswordSuccess', {
            url: '/RestPasswordSuccess',
            templateUrl: 'app/views/Account/ResetPasswordSuccess.html'
        })
        .state('PassChange', {
            url: '/PasswordChange',
            controller: 'UserController',
            controllerAs: 'UserCtrl',
            templateUrl: 'app/views/Account/PasswordChange.html'
        })
        .state('user.Mail', {
            url: '/AddMail',
            controller: 'UserController',
            controllerAs: 'UserCtrl',
            templateUrl: 'app/views/Nested/User/nested-correos.html'
        })
    }]);

    app.controller('MainController', ['AuthService', function (AuthService) {
        AuthService.GetUser();
    }]);

    app.run(function ($rootScope, $location, $state, AuthService) {


        $rootScope.$on('$stateChangeStart', function (e, toState, toParams
                                                       , fromState, fromParams) {

            var isLogin = toState.name === "login";
            if (isLogin && AuthService.Logged) {
                e.preventDefault();
                $state.go('listadoFacturas')
                return;
            } else if (isLogin)
            {
                return;
            }

            //// now, redirect only not authenticated

            //if (AuthService.Logged === false) {
            //    e.preventDefault(); // stop current execution
            //    $state.go('login'); // go to login
            //}
        });
    });
})();