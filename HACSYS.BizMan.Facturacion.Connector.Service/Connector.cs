﻿
using HACSYS.BizMan.FacturacionWeb.Connector.ARES;
using HACSYS.BizMan.FacturacionWeb.Connector.Core;
using HACSYS.BizMan.FacturacionWeb.Connector.CORE;
using HACSYS.SignalR;
using NLog;
using System;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using HACSYS.BizMan.DAL.EF6.Repositories;
using System.Configuration;

namespace HACSYS.BizMan.Facturacion.Connector.Service
{
    public partial class Connector : ServiceBase
    {
        private Logger Log = LogManager.GetLogger("ConnectorService");
        private Thread worker;
        private FacturacionClient cliente;
        public Connector()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            Log.Info("Iniciando Servicio");
            worker = new Thread(StartConnection);
            worker.Start();
        }

        private async void StartConnection()
        {
            try
            {
                var factory = new HubProxyFactory();
                var db = new VentasDbContext("VentasDB");
                var exists = db.Database.Exists();
                InitialData.InitializeDatabaseAsync(db).Wait();
#if DEBUG
                if (!exists)
                    TicketsSampleData.InitializeDatabaseAsync(db).Wait();
#endif
                //DB CONFIG
                var ventas = db.Venta.ToList();
                var repository = new VentaRepository(db);
                var despachos = new AresDbContext("ARESDB");
                var service = new AresTicketService(despachos, db);
                //var service = new VentasService(repository);
                // SERVER CONFIG
                var server = ConfigurationManager.AppSettings["Server"];
                var empresaID = ConfigurationManager.AppSettings["EmpresaID"];
                // MAIL CONFIG
                var sender = ConfigurationManager.AppSettings["Sender"];
                var key = ConfigurationManager.AppSettings["Key"];
                var MailConfig = ConfigurationManager.AppSettings["MailConfig"];
                int MailTime = 1;
                try {
                    MailTime = int.Parse(MailConfig);
                }catch(Exception ex)
                {
                    Log.Info($"No se pudo parsear {MailConfig}");
                }

                Log.Info($"Iniciando conexión a servidor: {server}");
                cliente = new FacturacionClient(Int32.Parse(empresaID), factory, service,sender,key, MailTime, server);
            }
            catch (Exception e)
            {
                Log.Error("Error al iniciar el servicio");
                Log.Error(e.Message);
                Log.Error(e.InnerException.Message);
            }
            //try
            //{
            //    var factory = new HubProxyFactory();
            //    var db = new VentasDbContext(@"server=VINCI\SQLEXPRESS;Database=VentasDB; User Id=sa;Password=hacsys;");
            //    TicketsSampleData.InitializeDatabaseAsync(db).Wait();
            //    var ventas = db.Venta.ToList();
            //    var repository = new VentaRepository(db);
            //    var despachos = new AresDbContext("server=CVARES; database=ARES;User Id=sa;Password=gilbadmin;");
            //    var service = new AresTicketService(despachos, db);
            //    var test = despachos.Despachos.Count();
            //    //var service = new VentasService(repository);
            //    cliente = new FacturacionClient(factory, service, "http://VINCI/FacturacionWeb");
            //}catch(Exception e)
            //{
            //    Log.Error("Error al iniciar el servicio");
            //    Log.Error(e.Message);
            //    Log.Error(e.InnerException.Message);
            //}
        }

        protected override void OnStop()
        {

        }
    }
}
