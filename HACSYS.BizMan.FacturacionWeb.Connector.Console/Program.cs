﻿using HACSYS.BizMan.DAL.EF6.Repositories;
using HACSYS.BizMan.FacturacionWeb.Connector.ARES;
using HACSYS.BizMan.FacturacionWeb.Connector.Core;
using HACSYS.BizMan.FacturacionWeb.Connector.CORE;
using HACSYS.SignalR;
using Microsoft.Framework.ConfigurationModel;
using System;
using System.Linq;

namespace HACSYS.BizMan.FacturacionWeb.Connector.Console
{
    public class Program
    {
        public void Main(string[] args)
        {
            try
            {
                var factory = new HubProxyFactory(Microsoft.AspNet.SignalR.Client.TraceLevels.All);
                var db = new VentasDbContext(@"Server=CVARES;Database=VentasDB; User Id=sa;Password=gilbadmin;");
                var exists = db.Database.Exists();
                InitialData.InitializeDatabaseAsync(db).Wait();
    #if DEBUG
                if(!exists)
                    TicketsSampleData.InitializeDatabaseAsync(db).Wait();
    #endif
                var ventas = db.Venta.ToList();
                var repository = new VentaRepository(db);
                var despachos = new AresDbContext("server=CVARES; database=ARES; User Id=sa;Password=gilbadmin;");
                var service = new AresTicketService(despachos, db);
                //var service = new VentasService(repository);
                var client = new FacturacionClient(1,factory, service, "http://localhost");
                //var client = new FacturacionClient(1, factory, service, "http://localhost:62722");
                System.Console.WriteLine("Press Enter to exit");
                System.Console.ReadLine();
            }catch(Exception e)
            {

            }

        }
    }
}
