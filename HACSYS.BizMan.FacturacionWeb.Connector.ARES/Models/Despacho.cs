﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.FacturacionWeb.Connector.ARES.Models
{
    [Table("POS.Despacho")]
    public class Despacho
    {
        public int DespachoID { get; set; }
        public string Producto { get; set; }
        public string ProductoClave { get; set; }
        public DateTime Fecha { get; set; }

        [Column(name:"Volumen")]
        public decimal VolumenDecimal { get; set; }
        [NotMapped]
        public double Volumen { get { return Convert.ToDouble(VolumenDecimal); } }

        [Column(name: "Importe")]
        public decimal ImporteDecimal { get; set; }
        [NotMapped]
        public double Importe { get { return Convert.ToDouble(ImporteDecimal); } }

        [Column(name: "Precio")]
        public decimal PrecioDecimal { get; set; }
        [NotMapped]
        public double Precio { get { return Convert.ToDouble(PrecioDecimal); } }
        public string TipoPago { get; set; }
        public int TipoPagoID { get; set; }
    }
    /*
          ,[Surtidor]
      ,[MangueraID]
      ,[Manguera]
      ,[ProductoID]
      ,[Producto]
      ,[ProductoClave]
      ,[Fecha]
      ,[Volumen]
      ,[Importe]
      ,[Precio]
      ,[TipoPagoID]
      ,[TipoPago]
    */
}
