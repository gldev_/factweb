﻿using HACSYS.BizMan.DAL.Models;
using HACSYS.BizMan.FacturacionWeb.Connector.Core;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACSYS.BizMan.FacturacionWeb.Connector.ARES
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class AresTicketService : IVentasService
    {
        private AresDbContext ARESContext;
        private VentasDbContext VentasContext;
        private Logger Log = LogManager.GetLogger("AresVentasService");
        public AresTicketService(AresDbContext despachosContext, VentasDbContext ventasContext)
        {
            ARESContext = despachosContext;
            VentasContext = ventasContext;
            ventasContext.Configuration.ProxyCreationEnabled = false;
            //ventasContext.Configuration.LazyLoadingEnabled = false;
        }

        public List<ImpuestoAplicado> CalcularImpuestos(Venta venta)
        {
            throw new NotImplementedException();
        }

        public Task<Venta> GenerarVentaAsync(string folio, double monto)
        {
            var folioID = Int32.Parse(folio);
            var montoDecimal = Convert.ToDecimal(monto);
            Log.Info($"Buscando venta({folio},{monto})");
            var despacho = ARESContext.Despachos.FirstOrDefault(d => d.DespachoID == folioID && d.ImporteDecimal == montoDecimal);
            
            if (despacho == null)
            {
                Log.Info($"Venta con folio:{folio} y monto:{monto} no encontrada");
                return Task.FromResult<Venta>(null);
            }

            // Obtener el producto
            var producto = VentasContext.Productos.Include("Impuestos").FirstOrDefault(d => d.Clave == despacho.ProductoClave);
            if(producto == null)
            {
                Log.Info($"Producto con clave:{despacho.ProductoClave} no encontrado");
                return Task.FromResult<Venta>(null);
            }
            
            // Checar que el tipo de Tipo de pago sea solo Efectivo o Tarjeta
            if(despacho.TipoPagoID > 2)
            {
                return Task.FromResult<Venta>(new Venta());
            }
            
            // Obtener impuesto
            var iva = producto.Impuestos.FirstOrDefault(i => i.NombreSAT == "IVA" && i.Activo);
            //var ieps = producto.Impuestos.FirstOrDefault(i => i.NombreSAT == "IEPS" && i.Activo);
            var ieps = producto.Impuestos.FirstOrDefault(i => i.Productos.FirstOrDefault().ProductoID == producto.ProductoID && i.NombreSAT.Equals("IEPS") && i.Activo);


            Log.Info("Calculando Impuestos");
            //Calcular los importes de cada impuesto
            var importeIEPS = despacho.Volumen * (ieps.Tasa/10000); //despacho.Importe - (((producto.ValorUnitario - (ieps.Tasa/100)) * despacho.Volumen) * (iva.Tasa/100));;
            var importeConIva  = (double)despacho.ImporteDecimal - importeIEPS; 
            //double tasaIVA = (iva.Tasa + 100) / 100;
            //var importeSinIva = importeConIva / tasaIVA;
            var importeSinIva = importeConIva / ((iva.Tasa+100)/100);
            var importeIVA = importeConIva - importeSinIva;

            var iepsAplicado = new ImpuestoAplicado { Impuesto = ieps, Valor = importeIEPS };
            var ivaAplicado = new ImpuestoAplicado { Impuesto = iva, Valor = importeIVA };

            var empresa = VentasContext.Empresas.FirstOrDefault();

            var detalle = new DetalleVenta
            {
                Cantidad = despacho.Volumen,
                Unidad = producto.Unidad,
                Total = despacho.Importe,
                Producto = producto,
                Activo = true,
                Codigo = producto.Codigo,
                Clave = despacho.ProductoClave,
                Concepto = producto.Concepto,
                ValorUnitario = producto.ValorUnitario,
                Subtotal = despacho.Importe - (importeIVA),
                ImpuestosAplicados = new List<ImpuestoAplicado> { ivaAplicado, iepsAplicado }
            };
            double result = detalle.Subtotal + importeIVA;
            var metodo = VentasContext.MetodosDePago.FirstOrDefault(m => m.Metodo == despacho.TipoPago);
            if (metodo == null)
                metodo = new MetodoDePago() { Metodo = despacho.TipoPago };
            // Forma de pago = Pago en una sola exhibición
            var forma = VentasContext.FormasDePago.Find(1);
            // Condicion de pago = No Aplica
            var condicion = VentasContext.CondicionesDePago.Find(1);

            var venta = new Venta
            {
                EmpresaID = empresa.EmpresaID,
                Activar = true,
                Fecha = despacho.Fecha,
                Folio = despacho.DespachoID.ToString(),
                Monto = detalle.Total,
                Subtotal = detalle.Subtotal,
                Detalles = new List<DetalleVenta> { detalle },
                FormaDePago = forma,
                CondicionDePago = condicion,
                MetodoDePago = metodo

            };
            Log.Info($"Busqueda terminada empresa:{empresa.EmpresaID}, regresando venta encontrada");
            //JsonConvert.JsonSerializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            // Quitar los datos innecesarios
            var test = JsonConvert.SerializeObject(venta,Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                            PreserveReferencesHandling = PreserveReferencesHandling.Objects
                        });
            return Task.FromResult<Venta>(venta);
        }
    }
}
