﻿using HACSYS.BizMan.FacturacionWeb.Connector.ARES.Models;
using System.Data.Entity;

namespace HACSYS.BizMan.FacturacionWeb.Connector.ARES
{
    public class AresDbContext : DbContext
    {
        public AresDbContext(string conn) : base(conn)
        {

        }
        public DbSet<Despacho> Despachos { get; set; }
    }
}