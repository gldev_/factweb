﻿using HACSYS.SignalR;
namespace HACSYS.BizMan.Connector
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new HubProxyFactory(Microsoft.AspNet.SignalR.Client.TraceLevels.All);
            var db = new TicketsDbContext("server=.; database=TicketsDB; Application Name=ARES EVP Service; Trusted_Connection=True;User Id=sa;Password=gilbadmin;");
            TicketsSampleData.InitializeDatabaseAsync(db).Wait();
            var repository = new TicketRepository(db);
            var despachos = new AresDbContext("server=.; database=ARES; Application Name=ARES EVP Service; Trusted_Connection=True;User Id=sa;Password=gilbadmin;");
            var service = new AresTicketService(despachos, db);
            var client = new FacturacionClient(factory, service, "http://vinci:3737/");
            System.Console.WriteLine("Press Enter to exit");
            System.Console.ReadLine();
        }
    }
}
